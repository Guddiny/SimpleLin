# Emulator

This program emulate firmware request/respose described in [Protocol](https://docs.google.com/document/d/1k_0_KEaK4MgFOL2ShaHvgRaSgq0yzDBagRkQHupUjd0/edit?usp=sharing).

For use the emulator you need:
- Python > 3.6
- Null model emulator (like [com0com](http://com0com.sourceforge.net/))
    
Before run you must edit port name in main.py file (by default it's "COM23")

```sh
 port_name = "COM23"  # Port name of your "fake device"
 ```
