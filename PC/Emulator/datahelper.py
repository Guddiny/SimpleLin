# Get uint16_t value from byte array
def get_u16(arr, start_ptr):
    u16 = 0x0000
    u16 = arr[start_ptr] << 8 | arr[start_ptr + 1]
    return u16


# Put uint16_t value to byte array
def put_u16(arr, start_ptr, u16_value):
    arr[start_ptr] = (u16_value & 0xFF00) >> 8
    arr[start_ptr + 1] = u16_value & 0xFF