class WPacket:

    def send_wpkg(self, serial_port, cmd, data):
        data_ptr = 1
        crc = 0x00
        tx_data = [0x00] * 20
        # TODO add data length checking

        tx_data[0] = cmd
        crc = crc ^ cmd
        for i in range(0, len(data)):
            tx_data[data_ptr] = data[i]
            crc = crc ^ data[i]
            data_ptr += 1
        tx_data[19] = crc
        print(tx_data)
        serial_port.write(tx_data)

    def get_crc(self, wpkg):
        crc = 0x00
        for i in range(0,18):
            crc = crc ^ wpkg[i]
        return crc

    def check_crc(self, wpkg):
        act_crc = self.get_crc(wpkg)
        if act_crc == wpkg[19]:
            return True
        return False