import random
from time import sleep

import serial
import wpackage
import datahelper


class DeviceEmulator:

    emulation_type = 4  # Emulate only sending pkg

    port_name = "COM10"  # Port name of your device
    temp_data = [0x00, 0x00, 0x00, 0x00]

    # Connect to device
    sp1 = serial.Serial(port_name, 38400)

    # Helper for package sending
    wp = wpackage.WPacket()

    # Definitions from protocol.
    # See: https://docs.google.com/document/d/1k_0_KEaK4MgFOL2ShaHvgRaSgq0yzDBagRkQHupUjd0/edit#
    GET_DEVICE_NAME = 0x1a
    GET_DEVICE_VERSION = 0x2a
    GET_LIN_BAUDRATE = 0x1b
    SET_LIN_BAUDRATE = 0x2b
    GET_LIN_MASTER_TIMEOUT = 0x1C
    SET_LIN_MASTER_TIMEOUT = 0x2C
    GET_LIN_FRAME = 0x1D
    SET_LIN_FRAME = 0x2D

    W_PKG_LENGTH = 20

    def run(self):
        while True:
            if self.emulation_type == 1:
                # TODO add sending message emulation
                print('Unsupported type!')
                break

            elif self.emulation_type == 2:
                if self.emulation_type == 2:
                    data = self.sp1.read(20)

                    if self.wp.check_crc(data):
                        if data[0] == self.GET_DEVICE_NAME:
                            self.wp.send_wpkg(self.sp1, self.GET_DEVICE_NAME, [0x53, 0x69, 0x6D, 0x70, 0x6C, 0x65, 0x4C, 0x69, 0x6E, 0x5C, 0x6E, 0x0A])  #SimpleLin

                        elif data[0] == self.GET_DEVICE_VERSION:
                            datahelper.put_u16(self.temp_data, 0, 0x0001)  # V 0.01
                            self.wp.send_wpkg(self.sp1, self.GET_DEVICE_VERSION, self.temp_data)

                        elif data[0] == self.GET_LIN_BAUDRATE:
                            datahelper.put_u16(self.temp_data, 0, 0x2710)  # 10000 kb/s
                            self.wp.send_wpkg(self.sp1, self.GET_LIN_BAUDRATE, self.temp_data)

                        elif data[0] == self.GET_LIN_MASTER_TIMEOUT:
                            self.temp_data[0] = 0x14  # 20 ms
                            self.wp.send_wpkg(self.sp1, self.GET_LIN_MASTER_TIMEOUT, self.temp_data)

                    else:
                        print('Bad CRC')

            elif self.emulation_type == 3:
                print('Sending frame...')
                self.wp.send_wpkg(self.sp1, self.SET_LIN_FRAME, [0x6, 0x3C, 0x01, 0x00, 0x04, 0x1, 0x08])  # SimpleLin
                print('Done.')
                break

            elif self.emulation_type == 4:
                print('Sending messages..')
                data1 = [0x7, 0x55, 0x3C, 0x01, 0x00, 0x04, 0x1, 0x08]
                data2 = [0x5, 0x55, 0x85, 0x01, 0x00, 0x04, 0x1, 0x08]
                data3 = [0x6, 0x55, 0x03, 0x01, 0x00, 0x04, 0x1, 0x08]
                while True:
                    self.wp.send_wpkg(self.sp1, self.GET_LIN_FRAME,
                                      data1)  # SimpleLin
                    data1[4] = random.randint(16,21)
                    data1[5] = random.randint(0,255)
                    data1[6] = random.randint(0,255)
                    sleep(0.01)

                    self.wp.send_wpkg(self.sp1, self.GET_LIN_FRAME,
                                      data2)  # SimpleLin
                    data2[4] = random.randint(16, 21)
                    data2[5] = random.randint(0, 255)
                    data2[random.randint(3, 5)] = random.randint(0, 255)
                    sleep(0.01)

                    self.wp.send_wpkg(self.sp1, self.GET_LIN_FRAME,
                                      data3)  # SimpleLin
                    data2[1] = random.randint(16, 21)
                    data3[5] = random.randint(0, 255)
                    data3[6] = random.randint(0, 255)
                    sleep(0.01)
                break


# Application entry point
if __name__ == "__main__":
        de = DeviceEmulator()
        de.run()
