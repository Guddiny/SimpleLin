﻿using LinGuiWP.BL.ConnectionManagment;
using LinGuiWP.BL.WProtocol;
using System;
using System.Diagnostics;

namespace LinGuiWP.Helpers
{
    public static class WPackage
    {
        // Send data by serial port formatted as WPckage
        public static void SendWpkg(this ISerialPortManager portManager, byte cmd, byte[] data)
        {
            byte[] txData = MakeWpkg(cmd, data);
            portManager.WriteData(txData);
#if DEBUG
            Debug.WriteLine(BitConverter.ToString(txData));
#endif
        }

        public static byte[] MakeWpkg(byte cmd, byte[] data)
        {
            byte[] txData = new byte[(int)Definitions.TxRxArratySize];
            int dataPtr = (int)Definitions.DataPtr;
            byte crc = 0x00;

            txData[(int)Definitions.CmdPtr] = cmd;
            crc ^= cmd;
            for (int i = 0; i < data.Length; i++)
            {
                txData[dataPtr] = data[i];
                crc ^= data[i];
                dataPtr++;
            }
            txData[(int)Definitions.CrcPtr] = crc;
            return txData;
        }
    }
}
