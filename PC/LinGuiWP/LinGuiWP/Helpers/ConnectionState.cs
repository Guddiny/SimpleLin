﻿namespace LinGuiWP.Helpers
{
    public static class ConnectionState
    {
        public const string Online = "ONLINE";
        public const string Offline = "OFFLINE";
    }
}