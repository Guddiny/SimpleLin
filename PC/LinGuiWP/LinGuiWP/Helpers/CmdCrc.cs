﻿using LinGuiWP.BL.WProtocol;

namespace LinGuiWP.Helpers
{
    public static class CmdCrc
    {
        public static byte GetCmdCrc(this byte[] data)
        {
            byte crc = 0;

            for (int i = 0; i < data.Length - 1; i++)
            {
                crc = (byte)(crc ^ data[i]);
            }

            return crc;
        }

        public static bool IsValidWpkgCrc(this byte[] data)
        {
            if (data.GetCmdCrc() == data[(int)Definitions.CrcPtr])
                return true;

            return false;
        }
    }
}