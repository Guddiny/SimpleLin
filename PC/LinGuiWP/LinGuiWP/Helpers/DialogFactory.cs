﻿using Microsoft.Win32;

namespace LinGuiWP.Helpers
{
    public static class DialogFactory
    {
        public static OpenFileDialog GetOpenFileDialog(string filter = "")
        {
            var openDialog = new OpenFileDialog();
            openDialog.CheckFileExists = true;
            openDialog.CheckPathExists = true;
            openDialog.RestoreDirectory = true;
            openDialog.ReadOnlyChecked = true;
            openDialog.ShowReadOnly = true;
            openDialog.FileName = "";
            openDialog.Filter = filter;

            return openDialog;
        }

        public static SaveFileDialog GetSaveFileDialog(string filter = "")
        {
            var saveDialog = new SaveFileDialog();
            saveDialog.Filter = filter;

            return saveDialog;
        }
    }
}
