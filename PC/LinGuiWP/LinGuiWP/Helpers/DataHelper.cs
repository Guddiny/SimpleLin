﻿using System;

namespace LinGuiWP.Helpers
{
    public static class DataHelper
    {
        // Get Uint16 value from array
        public static UInt16 GetU16(this byte[] arr, int startPtr)
        {
            UInt16 result = 0x0000;
            result = (UInt16)((arr[startPtr] << 8) | arr[startPtr + 1]);
            return result;
        }

        // Put Uint16 value to array
        public static void PutU16(this byte[] arr, int startPtr, UInt16 value)
        {
            arr[startPtr] = (byte)((value & 0xFF00) >> 8);
            arr[startPtr + 1] = (byte)(value & 0xFF);
        }
    }
}
