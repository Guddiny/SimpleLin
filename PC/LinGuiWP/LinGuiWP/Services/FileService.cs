﻿using System.Collections.Generic;
using LinGuiWP.Models.Lin;
using LinGuiWP.Services.Abstract;
using Newtonsoft.Json;
using System.IO;
using LinGuiWP.Helpers;
using LinGuiWP.Models;
using System.Xml.Serialization;

namespace LinGuiWP.Services
{
    public class FileService : IFileService
    {
        public FramesBuffer OpenFile(string filePath)
        {
            var result = new FramesBuffer();
            using (StreamReader file = File.OpenText(filePath))
            {
                var serializer = new XmlSerializer(typeof(FramesBuffer));
                result = (FramesBuffer)serializer.Deserialize(file);
            }

            return result;
        }

        public ObservablePairCollection<int, FrameSenderEntity> OpenCmdFile(string filePath)
        {
            var result = new ObservablePairCollection<int, FrameSenderEntity>();
            using (StreamReader file = File.OpenText(filePath))
            {
                var serializer = new XmlSerializer(typeof(ObservablePairCollection<int, FrameSenderEntity>));
                result = (ObservablePairCollection<int, FrameSenderEntity>)serializer.Deserialize(file);
            }

            return result;
        }

        public List<LinFrame> OpenFile(string filePath, object dataSource)
        {
            throw new System.NotImplementedException();
        }

        public void SaveFile(string filePath, object dataSource)
        {
            using (StreamWriter file = File.CreateText(filePath))
            {
                var serializer = new JsonSerializer
                {
                    Formatting = Formatting.Indented
                };
                serializer.Serialize(file, dataSource);
            }
        }

        public void SaveFile<T>(string filePath, T dataSource)
        {
            using (StreamWriter file = File.CreateText(filePath))
            {
                var serialiser = new XmlSerializer(typeof(T));
                serialiser.Serialize(file, dataSource);
            }
        }
    }
}