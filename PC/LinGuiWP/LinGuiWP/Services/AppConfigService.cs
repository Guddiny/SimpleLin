﻿using System;
using System.IO;
using System.Reflection;
using System.Xml.Serialization;

namespace LinGuiWP.Services
{
    public class AppConfigService
    {
        // Public
        public string ConfigPath { get; set; }
        public string FileName { get; set; }
        public readonly string DefaultFilepath;

        // Private
        private readonly string _defaultFileName;
        private readonly string _defaultFileDirectory;

        // Constructor
        public AppConfigService()
        {
            _defaultFileDirectory = AppDomain.CurrentDomain.BaseDirectory;
            _defaultFileName = $"linguicfg_v{Assembly.GetExecutingAssembly().GetName().Version}";
            DefaultFilepath = $"{_defaultFileDirectory}{_defaultFileName}.ulcfg";
        }

        /// <summary>
        /// Read app (user) config
        /// </summary>
        /// <returns></returns>
        public AppConfig ReadConfig()
        {
            AppConfig cfg = new AppConfig();

            if (File.Exists(DefaultFilepath))
            {
                using (StreamReader file = File.OpenText(DefaultFilepath))
                {
                    var serialiser = new XmlSerializer(typeof(AppConfig));
                    cfg = (AppConfig)serialiser.Deserialize(file);
                }
            }
            else
            {
                cfg = new AppConfig();
            }

            return cfg;
        }

        /// <summary>
        /// Save app (user) config
        /// </summary>
        /// <param name="appConfig"></param>
        public void SaveConfig(AppConfig appConfig)
        {
            using (StreamWriter file = File.CreateText(DefaultFilepath))
            {
                var serialiser = new XmlSerializer(typeof(AppConfig));
                serialiser.Serialize(file, appConfig);
            }
        }
    }
}