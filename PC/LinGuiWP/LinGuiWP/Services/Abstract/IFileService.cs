﻿using LinGuiWP.Models.Lin;
using System.Collections.Generic;
using LinGuiWP.Helpers;
using LinGuiWP.Models;

namespace LinGuiWP.Services.Abstract
{
    public interface IFileService
    {
        FramesBuffer OpenFile(string filePath);
        List<LinFrame> OpenFile(string filePath, object dataSource);
        ObservablePairCollection<int, FrameSenderEntity> OpenCmdFile(string filePath);
        void SaveFile(string filePath, object dataSource);
        void SaveFile<T>(string filePath, T dataSource);
    }
}