﻿namespace LinGuiWP.Services.Abstract
{
    public interface IMessageService
    {
        void ShowError(string message);
        void ShowInfo(string message);
        void ShowExclamatiom(string message);
    }
}