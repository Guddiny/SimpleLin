﻿using System.Windows;

namespace LinGuiWP.Services
{
    public class AppConfig
    {
        // Connection config
        public string LastSerialPort { get; set; } = "COM1";
        public int LinBaudrateSelectedIndex { get; set; } = 0;

        public int SelectedBaud { get; set; } = 9600;

        // Window config
        public int Top { get; set; } = 150;
        public int Left { get; set; } = 150;
        public int Height { get; set; } = 550;
        public int Width { get; set; } = 760;
        public WindowState WindowState { get; set; } = WindowState.Normal;
        public Visibility FramesGridVisibility { get; set; } = Visibility.Collapsed;
        public Visibility BitViewVisibility { get; set; } = Visibility.Collapsed;
    }
}