﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace LinGuiWP.Converters
{
    public class CountToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && parameter != null)
            {
                int x = (int)value;
                int y = System.Convert.ToInt32(parameter);

                if (x > y)
                {
                    return Visibility.Visible;
                }
            }

            return Visibility.Hidden;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
