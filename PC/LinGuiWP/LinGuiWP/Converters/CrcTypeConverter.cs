﻿using LinGuiWP.Models.Lin;
using System;
using System.Globalization;
using System.Windows.Data;

namespace LinGuiWP.Converters
{
    public class CrcTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (CrcType)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (CrcType)value;
        }
    }
}
