﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace LinGuiWP.Converters
{
    public class IntToStringConverter:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int? digit = value as int?;

            var result = digit.ToString();

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var digitString = value as string;

            int? digit = System.Convert.ToInt32(digitString);

            return digit;
        }
    }
}