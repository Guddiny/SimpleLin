﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace LinGuiWP.Converters
{
    public class DataItemEnableDisabelConverter:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && parameter != null)
            {
                var itemValue = (int) value;
                var itemParam = System.Convert.ToInt32(parameter);
                if (itemValue >= itemParam)
                {
                    return true;
                }
            }
            
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}