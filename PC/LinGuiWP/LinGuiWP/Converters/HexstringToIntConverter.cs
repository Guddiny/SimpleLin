﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace LinGuiWP.Converters
{
    public class HexstringToIntConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            byte? intValue = value as byte?;

            var result = intValue?.ToString("X");

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var hexString = value as string;

            byte? result = System.Convert.ToByte(hexString, 16);
            
            return result;
        }
    }
}