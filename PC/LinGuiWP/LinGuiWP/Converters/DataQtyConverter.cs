﻿using LinGuiWP.Models;
using System;
using System.Globalization;
using System.Windows.Data;

namespace LinGuiWP.Converters
{
    public class DataQtyConverter : IValueConverter
    {
        // Conver data form data йен combobox for send kin frame tab -> becouse data qty it's PID + data + CRC (PID and CRC = 2 bytes) but for
        // user beter show data qty as only data bytes. See protocol https://docs.google.com/document/d/1k_0_KEaK4MgFOL2ShaHvgRaSgq0yzDBagRkQHupUjd0/edit
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (int)value - FrameSenderEntity.DclCorrection; 
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (int)value + FrameSenderEntity.DclCorrection;
        }
    }
}
