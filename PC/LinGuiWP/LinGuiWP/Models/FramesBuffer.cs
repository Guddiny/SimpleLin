﻿using LinGuiWP.Models.Lin;
using System.Collections.ObjectModel;
using System.Linq;
using GalaSoft.MvvmLight.Threading;
using GalaSoft.MvvmLight;
using LinGuiWP.Helpers;
using LinGuiWP.Mappers;

namespace LinGuiWP.Models
{
    public class FramesBuffer: ObservableObject
    {
        public ObservableCollection<LinFrame> LinFrames { get; set; }
        public ObservablePairCollection<int, ObservableLinFrame> ObservableLinFrames { get; set; }

        private BitByte _selectedByte;
        public BitByte SelectedByte
        {
            get { return _selectedByte; }
            set
            {
                Set(() => SelectedByte, ref _selectedByte, value);
            }
        }
        
        private decimal _errors = 0;
        public decimal Errors
        {
            get { return _errors; }
            set
            {
                Set(() => Errors, ref _errors, value);
            }
        }

        private decimal _errorsPercent = 0;
        public decimal ErrorsPercent
        {
            get { return _errorsPercent; }
            set
            {
                Set(() => ErrorsPercent, ref _errorsPercent, value);
            }
        }

        public FramesBuffer()
        {
            LinFrames = new ObservableCollection<LinFrame>();
            ObservableLinFrames = new ObservablePairCollection<int, ObservableLinFrame>();  
            SelectedByte = new BitByte();
        }

        public void InsertFrame(LinFrame frame)
        {
            DispatcherHelper.CheckBeginInvokeOnUI(() =>
            {
                LinFrames.Add(frame);
                if (frame.State == FrameState.Classic || frame.State == FrameState.Enhanced || frame.State == FrameState.OK)
                {
                    InsertToOservableList(frame);
                }
                else
                {
                    Errors++;
                    ErrorsPercent = (Errors * 100)/LinFrames.Count ;
                }
            });
        }

        private void InsertToOservableList(LinFrame frame)
        {
            var obsFrame = frame.MapToObservableLinFrame();

            if (ObservableLinFrames.All(p => p.Key != obsFrame.PID))
            {
                ObservableLinFrames.Add(obsFrame.PID, obsFrame);
            }
            else
            {
                var tempFrame = ObservableLinFrames.First(p => p.Key == obsFrame.PID).Value;

                if (!tempFrame.IsWathing) // If frame is not watching - do not update values
                {
                    return;
                }
                
                tempFrame.Period = obsFrame.TimeMarker.TotalMilliseconds - tempFrame.TimeMarker.TotalMilliseconds;
                tempFrame.TimeMarker = obsFrame.TimeMarker;

                if (tempFrame.DataQty != obsFrame.DataQty)
                {
                    tempFrame.DataQty = obsFrame.DataQty;
                }
                if (tempFrame.PID != obsFrame.PID)
                {
                    tempFrame.PID = obsFrame.PID;
                }
                if (tempFrame.ID != obsFrame.ID)
                {
                    tempFrame.ID = obsFrame.ID;
                }
                if (tempFrame.CRC != obsFrame.CRC)
                {
                    tempFrame.CRC = obsFrame.CRC;
                }
                for (int i = 0; i < obsFrame.Data.Length; i++)
                {
                    if (tempFrame.Data[i].Item != obsFrame.Data[i].Item)
                    {
                        tempFrame.Data[i].Item = obsFrame.Data[i].Item;
                    }

                    if (tempFrame.SelectedByteIndex == i)
                    {
                        var item = tempFrame.Data[i].Item;
                        if (item != null) 
                            SelectedByte.SetBitByte(item.Value);
                    }
                }

                tempFrame.Count++;
            }
        }
    }
}