﻿namespace LinGuiWP.Models
{
    public enum EntityMode
    {
        SingleShot,
        ExactAmount,
        Infinitely
    }
}