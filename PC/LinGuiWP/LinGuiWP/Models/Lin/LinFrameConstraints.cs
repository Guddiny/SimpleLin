﻿namespace LinGuiWP.Models.Lin
{
    public static class LinFrameConstraints
    {
        // Because real id is first 6 bit in protected Id (PID)
        private const byte _realIdBitMask = 0x3f;
        // Because P0 is 7 bit in PID
        private const byte _p0ParityBitMask = 0x40;
        // Because P1 is 8 bit in PID
        private const byte _p1ParityBitMask = 0x80;
        // By lin 2.2 specification
        private const byte _minValidId = 0x00;
        // By lin 2.2 specification
        private const byte _maxValidId = 0x3f;

        private const byte _b0BitMask = 0x01;
        private const byte _b1BitMask = 0x02;
        private const byte _b2BitMask = 0x04;
        private const byte _b3BitMask = 0x08;
        private const byte _b4BitMask = 0x10;
        private const byte _b5BitMask = 0x20;

        // Return real ID from PID (real id is first 6 bit in protected Id (PID) )
        public static byte GetRealId(byte pid)
        {
            byte id = (byte)(pid & _realIdBitMask);
            return id;
        }

        // PID byte structure [P1 P0 5 4 3 2 1 0]
        // Get P0 parity bit.
        private static byte GetP0ParityBit(byte pid)
        {
            return (byte)((pid & _p0ParityBitMask) >> 6);
        }


        // PID byte structure [P1 P0 5 4 3 2 1 0]
        // Get P1 parity bit.
        private static byte GetP1ParityBit(byte pid)
        {
            return (byte)((pid & _p1ParityBitMask) >> 7);
        }


        // Calculate parity bit 1 by Lin specification.
        private static byte CalculateParityP0(byte realId)
        {
            var b0 = realId & _b0BitMask;
            var b1 = (realId & _b1BitMask) >> 1;
            var b2 = (realId & _b2BitMask) >> 2;
            var b4 = (realId & _b4BitMask) >> 4;

            byte parityP0 = (byte)(b0 ^ b1 ^ b2 ^ b4);

            return parityP0;
        }

        // Calculate parity bit 1 by Lin specification. 
        private static byte CalculateParityP1(byte realId)
        {
            var b1 = (realId & _b1BitMask) >> 1;
            var b3 = (realId & _b3BitMask) >> 3;
            var b4 = (realId & _b4BitMask) >> 4;
            var b5 = (realId & _b5BitMask) >> 5;

            byte parityP1 = (byte)((b1 ^ b3 ^ b4 ^ b5) ^ 1);

            return parityP1;
        }

        // Check that real id located in actual range.
        // TODO Add this function
        //public static bool ValidateId(LinFrame linFrame)
        //{
        //    byte id = GetRealId((byte)linFrame.PID);

        //    bool result =  id <= _maxValidId;

        //    if (!result)
        //    {
        //        linFrame.State = FrameState.InvalidId;
        //    }

        //    return result;
        //}

        // CHeck parity 
        public static bool ValidateParity(LinFrame linFrame)
        {
            byte id = GetRealId((byte)linFrame.PID);
            byte factP0 = GetP0ParityBit((byte)linFrame.PID);
            byte factP1 = GetP1ParityBit((byte)linFrame.PID);

            byte calcP0 = CalculateParityP0(id);
            byte calcP1 = CalculateParityP1(id);

            bool result = (factP0 == calcP0) && (factP1 == calcP1);

            if (!result)
            {
                linFrame.State = FrameState.InvalidParity;
            }

            return result;
        }

        // Calculate CRC of lin protocol version 1.3
        public static byte CrcV13(LinFrame linFrame)
        {
            ushort crc = 0;

            for (int i = 0; i < linFrame.DataQty; i++)
            {
                crc += (ushort)linFrame.Data[i];
                if (crc >= 256)
                {
                    crc -= 0xFF;
                }
            }

            crc = (ushort)(0xFF - crc);

            return (byte)crc;
        }

        // Calculate CRC of lin protocol version 2.*
        public static byte CrcV22(LinFrame linFrame)
        {
            ushort crc = 0;

            crc += linFrame.PID;

            for (int i = 0; i < linFrame.DataQty; i++)
            {
                crc += (ushort)linFrame.Data[i];
                if (crc >= 256)
                {
                    crc -= 0xFF;
                }
            }

            crc = (ushort)(0xFF - crc);

            return (byte)crc;
        }

        // Check crc and assign frame state
        public static bool ValidateCrc(LinFrame linFrame)
        {
            byte calcCrc13 = CrcV13(linFrame);
            if (calcCrc13 == linFrame.CRC)
            {
                linFrame.State = FrameState.Classic;
                return true;
            }

            byte calcCrc22 = CrcV22(linFrame);
            if (calcCrc22 == linFrame.CRC)
            {
                linFrame.State = FrameState.Enhanced;
                return true;
            }

            linFrame.State = FrameState.InvalidCRC;
            return false;
        }
    }
}
