﻿using System;
using GalaSoft.MvvmLight;

namespace LinGuiWP.Models.Lin
{
    public class ObservableLinFrame : ObservableObject
    {
        private const int DefaultCountValue = 1;
        private const int DefaultByteIndex = 10;

        public ObservableLinFrame()
        {
            SelectedByteIndex = DefaultByteIndex;
        }

        private bool _isWathing = true;

        public bool IsWathing
        {
            get { return _isWathing; }
            set { Set<bool>(() => IsWathing, ref _isWathing, value); }
        }

        private TimeSpan _timeMarker;

        public TimeSpan TimeMarker
        {
            get { return _timeMarker; }
            set { Set<TimeSpan>(() => TimeMarker, ref _timeMarker, value); }
        }

        private double _period;

        public double Period
        {
            get { return _period; }
            set { Set<Double>(() => Period, ref _period, value); }
        }

        private byte _pid;

        public byte PID
        {
            get { return _pid; }
            set { Set<byte>(() => PID, ref _pid, value); }
        }

        private byte _id;

        public byte ID
        {
            get { return _id; }
            set { Set<byte>(() => ID, ref _id, value); }
        }

        private int _dataQty;

        public int DataQty
        {
            get { return _dataQty; }
            set { Set<int>(() => DataQty, ref _dataQty, value); }
        }

        private byte? _crc;

        public byte? CRC
        {
            get { return _crc; }
            set { Set<byte?>(() => CRC, ref _crc, value); }
        }

        private int _count = DefaultCountValue;

        public int Count
        {
            get { return _count; }
            set { Set<int>(() => Count, ref _count, value); }
        }

        public ObservableItem[] Data { get; set; }

        public uint SelectedByteIndex { get; set; }

        public void ResetIndex()
        {
            SelectedByteIndex = DefaultByteIndex;
        }

        private string _note;

        public string Note
        {
            get => _note;
            set => Set<string>(() => Note, ref _note, value);
        }
    }
}