﻿using GalaSoft.MvvmLight;

namespace LinGuiWP.Models.Lin
{
    public class ObservableItem : ObservableObject
    {
        private byte? _item;
        public byte? Item
        {
            get { return _item; }
            set
            {
                Set<byte?>(() => Item, ref _item, value);
            }
        }
    }
}
