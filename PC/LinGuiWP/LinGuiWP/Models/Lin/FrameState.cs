﻿namespace LinGuiWP.Models.Lin
{
    public enum FrameState
    {
        OK,
        BadDataLength,
        InvalidCRC,
        Enhanced,
        Classic,
        InvalidParity,
        InvalidId
    }
}