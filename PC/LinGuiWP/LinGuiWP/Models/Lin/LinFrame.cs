﻿using System;

namespace LinGuiWP.Models.Lin
{
    public class LinFrame
    {
        public TimeSpan TtimeMarker { get; set; }
        public byte PID { get; set; }
        public byte ID { get; set; }
        public int DataQty { get; set; }
        public byte?[] Data { get; set; }
        public byte? CRC { get; set; }
        public CrcType CrcType { get; set; }
        public FrameState State { get; set; }
    }
}
