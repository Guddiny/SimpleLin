﻿using GalaSoft.MvvmLight;
using LinGuiWP.Models.Lin;
using System;

namespace LinGuiWP.Models
{
    public class FrameSenderEntity : ObservableObject, ICloneable
    {
        private LinFrame _frame;

        public const int DclCorrection = 2;

        public LinFrame Frame
        {
            get { return _frame; }
            set { Set(() => Frame, ref _frame, value); }
        }

        private bool _isMaster = false;

        public bool IsMaster
        {
            get { return _isMaster; }
            set
            {
                Set(() => IsMaster, ref _isMaster, value);
                EvaluateCanTimeout();
            }
        }

        private bool _isEveryTime = false;

        public bool IsEveryTime
        {
            get { return _isEveryTime; }
            set
            {
                Set(() => IsEveryTime, ref _isEveryTime, value);
                EvaluateCanTimeout();
            }
        }

        private bool _canTimeout = false;

        public bool CanTimeout
        {
            get { return _canTimeout; }
            set { Set(() => CanTimeout, ref _canTimeout, value); }
        }

        private void EvaluateCanTimeout()
        {
            if (IsMaster && IsEveryTime)
            {
                CanTimeout = true;
            }
            else
            {
                CanTimeout = false;
            }
        }

        private bool _isEnabled = false;

        public bool IsEnabled
        {
            get { return _isEnabled; }
            set { Set(() => IsEnabled, ref _isEnabled, value); }
        }

        private bool _isStarted = false;

        public bool IsStarted
        {
            get { return _isStarted; }
            set { Set(() => IsStarted, ref _isStarted, value); }
        }


        private EntityMode _entityMode;

        public EntityMode EntityMode
        {
            get { return _entityMode; }
            set { Set(() => EntityMode, ref _entityMode, value); }
        }


        private int _repetitionsNumber;

        public int RepetitionsNumber
        {
            get { return _repetitionsNumber; }
            set { Set(() => RepetitionsNumber, ref _repetitionsNumber, value); }
        }

        private int _msgDelay;

        public int MsgDelay
        {
            get { return _msgDelay; }
            set { Set(() => MsgDelay, ref _msgDelay, value); }
        }

        private bool _isUseIncrement;

        public bool IsUseIncrement
        {
            get { return _isUseIncrement; }
            set { Set(() => IsUseIncrement, ref _isUseIncrement, value); }
        }

        private byte?[] _incrementValues;

        public byte?[] IncrementValues
        {
            get { return _incrementValues; }
            set { Set(() => IncrementValues, ref _incrementValues, value); }
        }

        private string _entityName = "Noname";

        public string EntityName
        {
            get { return _entityName; }
            set { Set(() => EntityName, ref _entityName, value); }
        }
        
        
        private int _timeout = 1000; // Value in microseconds

        public int Timeout
        {
            get { return _timeout; }
            set { Set(() => Timeout, ref _timeout, value); }
        }
        
        private int _currentTimeStamp = 0; // Value in microseconds

        public int CurrentTimeStamp
        {
            get { return _currentTimeStamp; }
            set { Set(() => CurrentTimeStamp, ref _currentTimeStamp, value); }
        }

        
        private int _sendCount = 0; // Value in microseconds

        public int SendCount
        {
            get { return _sendCount; }
            set { Set(() => SendCount, ref _sendCount, value); }
        }

        private bool _canChange = true;
        
        public bool CanChange
        {
            get { return _canChange; }
            set { Set(() => CanChange, ref _canChange, value); }
        }


        private CrcType _crcType;

        public CrcType CrcType
        {
            get { return _crcType;} 
            set { Set(() => CrcType, ref (_crcType), value); }
        }
        
        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}