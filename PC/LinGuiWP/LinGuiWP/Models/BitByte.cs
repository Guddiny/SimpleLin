﻿using GalaSoft.MvvmLight;

namespace LinGuiWP.Models
{
    public class BitByte:ObservableObject
    {
        private decimal _bit0 = 0;
        public decimal Bit0
        {
            get { return _bit0; }
            set
            {
                Set(() => Bit0, ref _bit0, value);
            }
        }
        
        private decimal _bit1 = 0;
        public decimal Bit1
        {
            get { return _bit1; }
            set
            {
                Set(() => Bit1, ref _bit1, value);
            }
        }
        private decimal _bit2 = 0;
        public decimal Bit2
        {
            get { return _bit2; }
            set
            {
                Set(() => Bit2, ref _bit2, value);
            }
        }
        private decimal _bit3 = 0;
        public decimal Bit3
        {
            get { return _bit3; }
            set
            {
                Set(() => Bit3, ref _bit3, value);
            }
        }
        private decimal _bit4 = 0;
        public decimal Bit4
        {
            get { return _bit4; }
            set
            {
                Set(() => Bit4, ref _bit4, value);
            }
        }
        private decimal _bit5 = 0;
        public decimal Bit5
        {
            get { return _bit5; }
            set
            {
                Set(() => Bit5, ref _bit5, value);
            }
        }
        private decimal _bit6 = 0;
        public decimal Bit6
        {
            get { return _bit6; }
            set
            {
                Set(() => Bit6, ref _bit6, value);
            }
        }
        private decimal _bit7 = 0;
        public decimal Bit7
        {
            get { return _bit7; }
            set
            {
                Set(() => Bit7, ref _bit7, value);
            }
        }

        public void SetBitByte(byte value)
        {
            Bit0 = (value & (1 << 0)) >> 0;
            Bit1 = (value & (1 << 1)) >> 1;
            Bit2 = (value & (1 << 2)) >> 2;
            Bit3 = (value & (1 << 3)) >> 3;
            Bit4 = (value & (1 << 4)) >> 4;
            Bit5 = (value & (1 << 5)) >> 5;
            Bit6 = (value & (1 << 6)) >> 6;
            Bit7 = (value & (1 << 7)) >> 7;
            // TODO add byte to bit mapping
        }
    }
}