/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:LinGuiWP"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using GalaSoft.MvvmLight.Ioc;
using LinGuiWP.BL.ConnectionManagment;
using LinGuiWP.Services;
using LinGuiWP.Services.Abstract;
using LinGuiWP.ViewModel;
using Microsoft.Practices.ServiceLocation;

namespace LinGuiWP
{
    public class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            ////if (ViewModelBase.IsInDesignModeStatic)
            ////{
            ////    // Create design time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DesignDataService>();
            ////}
            ////else
            ////{
            ////    // Create run time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DataService>();
            ////}

            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<IdStoryViewModel>();
            SimpleIoc.Default.Register<ChartViewModel>(()=>new ChartViewModel());
            SimpleIoc.Default.Register<SlaveSenderViewModel>();
            SimpleIoc.Default.Register<MasterSenderViewModel>();
            SimpleIoc.Default.Register<ISerialPortManager, AsyncSerialPortManager>();
            SimpleIoc.Default.Register<IMessageService, MessageService>();
            SimpleIoc.Default.Register<IFileService, FileService>();
        }

        public MainViewModel Main
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainViewModel>();
            }
        }

        public ChartViewModel Chart
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ChartViewModel>();
            }
        }

        public IdStoryViewModel IdStory
        {
            get
            {
                return ServiceLocator.Current.GetInstance<IdStoryViewModel>();
            }
        }

        public SlaveSenderViewModel SlaveSender
        {
            get
            {
                return ServiceLocator.Current.GetInstance<SlaveSenderViewModel>();
            }
        }

        public MasterSenderViewModel MasterSender
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MasterSenderViewModel>();
            }
        }

        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}