﻿using System.Windows;

namespace LinGuiWP.ViewModel
{
    /// <summary>
    /// Interaction logic for ChartView.xaml
    /// </summary>
    public partial class ChartView : Window
    {
        public ChartView()
        {
            InitializeComponent();
        }
    }
}
