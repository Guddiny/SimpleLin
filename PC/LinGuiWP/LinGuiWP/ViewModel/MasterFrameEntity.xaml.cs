﻿using System.Globalization;
using System.Windows.Controls;
using System.Windows.Input;

namespace LinGuiWP.ViewModel
{
    /// <summary>
    /// Interaction logic for MasterFrameEntity.xaml
    /// </summary>
    public partial class MasterFrameEntity : UserControl
    {
        public MasterFrameEntity()
        {
            InitializeComponent();
        }

        private void OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int hexNumber;
            e.Handled = !int.TryParse(e.Text, NumberStyles.HexNumber, CultureInfo.CurrentCulture, out hexNumber);
        }
    }
}
