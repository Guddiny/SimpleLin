using System;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using LinGuiWP.BL.ConnectionManagment;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using LinGuiWP.BL.WProtocol;
using LinGuiWP.Helpers;
using LinGuiWP.Mappers;
using LinGuiWP.Services.Abstract;
using GalaSoft.MvvmLight.Threading;
using LinGuiWP.Models;
using LinGuiWP.Models.Lin;
using System.Threading.Tasks;
using System.Windows;
using NLog;
using System.Threading;
using System.Reflection;
using System.Windows.Controls;
using LinGuiWP.Services;

namespace LinGuiWP.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        // Build version
        private string _version = Assembly.GetExecutingAssembly().GetName().Version.ToString();

        private Logger _log = LogManager.GetCurrentClassLogger();

        public const string LatestFwVersion = "ver. 1.2";

        // Fields
        private readonly ISerialPortManager _portManager;
        private readonly IMessageService _messageService;
        private readonly IFileService _fileService;
        private readonly ChartViewModel _chatViewModel;
        private readonly SlaveSenderViewModel _slaveSenderViewModel;
        private readonly MasterSenderViewModel _masterSenderViewModel;
        public ConnectioView _connectioView;
        public SlaveSenderView _slaveSenderView;
        public MasterSenderView _masterSenderView;
        private AppConfigService _appConfigService;
        private AppConfig _appConfig;

        private Stopwatch _sw;
        public FramesBuffer FramesBuffer { get; set; }
        private bool _isMessageHandling = false;
        private const int DefaultGridSize = 300;

        // Property 1
        public ObservableCollection<string> PortList { get; set; }
        public ObservableCollection<int> BaudrateList { get; set; }
        public ObservableCollection<int> TimeoutList { get; set; }

        // Property 2
        public string Title
        {
            get { return $"Simple Lin sniffer - v{Assembly.GetExecutingAssembly().GetName().Version}  |   {LastSerialPort}"; }
        }

        private string _currentConnectionState = "OFFLINE";
        public string CurrentConnectionState
        {
            get { return _currentConnectionState; }
            set
            {
                Set(() => CurrentConnectionState, ref _currentConnectionState, value);
            }
        }

        private int _top;
        public int Top
        {
            get { return _top; }
            set { Set(() => Top, ref _top, value); }
        }

        private int _left;
        public int Left
        {
            get { return _left; }
            set { Set(() => Left, ref _left, value); }
        }

        private int _height;
        public int Height
        {
            get { return _height; }
            set { Set(() => Height, ref _height, value); }
        }

        private int _width;
        public int Width
        {
            get { return _width; }
            set { Set(() => Width, ref _width, value); }
        }

        private WindowState _windowState;
        public WindowState WindowState
        {
            get { return _windowState; }
            set { Set(() => WindowState, ref _windowState, value); }
        }

        private string _lastSerialPort;
        public string LastSerialPort
        {
            get { return _lastSerialPort; }
            set { _lastSerialPort = value; }
        }

        private int _selectedBaud;
        public int SelectedBaud
        {
            get { return _selectedBaud; }
            set { _selectedBaud = value; }
        }

        private string _deviceName = string.Empty;
        public string DeviceName
        {
            get { return _deviceName; }
            set
            {
                Set(() => DeviceName, ref _deviceName, value);
            }
        }

        private string _deviceVersion = string.Empty;
        public string DeviceVersion
        {
            get { return _deviceVersion; }
            set
            {
                Set(() => DeviceVersion, ref _deviceVersion, value);
            }
        }

        private int _masterTimeout = 0;
        public int MasterTimeout
        {
            get { return _masterTimeout; }
            set
            {
                Set(() => MasterTimeout, ref _masterTimeout, value);
            }
        }

        private int _linBaudrate = 0;
        public int LinBaudrate
        {
            get { return _linBaudrate; }
            set
            {
                Set(() => LinBaudrate, ref _linBaudrate, value);
            }
        }

        private int _linBaudrateSelectedIndex = 0;
        public int LinBaudrateSelectedIndex
        {
            get { return _linBaudrateSelectedIndex; }
            set
            {
                Set(() => LinBaudrateSelectedIndex, ref _linBaudrateSelectedIndex, value);
            }
        }

        private bool _canChangePort = true;
        public bool CanChangePort
        {
            get { return _canChangePort; }
            set
            {
                Set(() => CanChangePort, ref _canChangePort, value);
            }
        }

        private bool _canStart = false;
        public bool CanStart
        {
            get { return !_canStart; }
            set
            {
                Set(() => CanStart, ref _canStart, value);
                CanStop = _canStart;
            }
        }

        private bool _canStop = false;
        public bool CanStop
        {
            get { return _canStop; }
            set
            {
                Set(() => CanStop, ref _canStop, value);
            }
        }

        private Visibility _progressVisibility = Visibility.Hidden;
        public Visibility ProgressVisibility
        {
            get { return _progressVisibility; }
            set
            {
                Set(() => ProgressVisibility, ref _progressVisibility, value);
            }
        }

        private Visibility _debugModeVisibility = Visibility.Hidden;
        public Visibility DebugModeVisibility
        {
            get
            {
#if DEBUG
                return Visibility.Visible;
#elif !DEBUG
                return Visibility.Hidden;
#endif
            }
        }

        private Visibility _gridVisibility;
        public Visibility GridVisibility
        {
            get { return _gridVisibility; }
            set
            {
                Set(() => GridVisibility, ref _gridVisibility, value);
            }
        }

        private Visibility _bitViewVisibility;
        public Visibility BitViewVisibility
        {
            get { return _bitViewVisibility; }
            set
            {
                Set(() => BitViewVisibility, ref _bitViewVisibility, value);
            }
        }

        private Pair<int, ObservableLinFrame> _selectedItem;
        public Pair<int, ObservableLinFrame> SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                Set(() => SelectedItem, ref _selectedItem, value);
            }
        }

        private DataGridCellInfo _cellInfo;

        public DataGridCellInfo CellInfo
        {
            get { return _cellInfo; }
            set
            {
                Set(() => CellInfo, ref _cellInfo, value);
                foreach (var f in FramesBuffer.ObservableLinFrames)
                {
                    f.Value.ResetIndex();
                }
            }
        }

        // Commands

        public RelayCommand ShowConnectionSetupCmd { get; set; }
        private void ShowConnectionSetup()
        {
            if (_connectioView == null)
            {
                _connectioView = new ConnectioView();
                _connectioView.Owner = Application.Current.MainWindow;
                _connectioView.Show();
            }

            _connectioView.Visibility = Visibility.Visible;
        }

        public RelayCommand ShowSlaveSendingViewCmd { get; set; }
        private void ShowSlaveSendingView()
        {
            if (_slaveSenderView == null)
            {
                _slaveSenderView = new SlaveSenderView();
                _slaveSenderView.Owner = Application.Current.MainWindow;
                _slaveSenderView.Show();
            }
            if (_masterSenderView != null)
            {
                _masterSenderViewModel.StopAll();
                _masterSenderView.Visibility = Visibility.Hidden;
            }

            _slaveSenderView.Visibility = Visibility.Visible;
        }

        public RelayCommand ShowMasterSendingViewCmd { get; set; }
        private void ShowMasterSendingView()
        {
            if (_masterSenderView == null)
            {
                _masterSenderView = new MasterSenderView();
                _masterSenderView.Owner = Application.Current.MainWindow;
                _masterSenderView.Show();
            }
            if (_slaveSenderView != null)
            {
                _slaveSenderViewModel.StopAll();
                _slaveSenderView.Visibility = Visibility.Hidden;
            }

            _masterSenderView.Visibility = Visibility.Visible;
        }

        public RelayCommand RefreshPortListCmd { get; set; }
        private void RefreshPortList()
        {
            PortList.Clear();
            foreach (var p in _portManager.GetPortList())
            {
                PortList.Add(p);
            }
            _log.Info($"Refreshed port list");
        }

        public RelayCommand ClearCmd { get; set; }
        private void Clear()
        {
            FramesBuffer.LinFrames.Clear();
            FramesBuffer.ObservableLinFrames.Clear();
            FramesBuffer.Errors = 0;
            FramesBuffer.ErrorsPercent = 0;
            _log.Info("Clear incoming buffers");
        }

        public RelayCommand<string> ChangeConnetionStateCmd { get; private set; }
        private void ChangeConnetionState(string param)
        {
            try
            {
                if (!_portManager.PortState)
                {
                    _portManager.OpenPort(param);
                    CurrentConnectionState = ConnectionState.Online;
                    CanChangePort = false;
                    _log.Info($"Open port with name: {param}");
                    _sw.Start();

                    _portManager.SendWpkg((byte)Commands.GetDeviceName, new byte[] { 0x12, 0x12 }); // Request from device - Name
                    Thread.Sleep(200);
                    _portManager.SendWpkg((byte)Commands.GetDeviceVersion, new byte[] { 0x12, 0x12 }); // Request from device - Version
                    Thread.Sleep(200);
                    SetBaudrate(SelectedBaud);
                    Thread.Sleep(200);
                    _portManager.SendWpkg((byte)Commands.GetLinBaudrate, new byte[] { 0x12, 0x12 }); // Request from device - Baudrate
                    Thread.Sleep(200);
                    _portManager.SendWpkg((byte)Commands.GetMasterTimeout, new byte[] { 0x12, 0x12 }); // Request from device - Timeout
                }
                else
                {
                    _sw.Stop();
                    _portManager.ClosePort();
                    CurrentConnectionState = ConnectionState.Offline;
                    ResetLabels();
                    CanChangePort = true;
                    _log.Info($"Close port with name: {_portManager.CurrentPortName}");
                }
            }
            catch (SerialportManagerException e)
            {
                _log.Error(e);
                _messageService.ShowError(e.Message);
            }
        }

        public RelayCommand SaveCmd { get; private set; }
        private void Save()
        {
            var saveFileDialog = DialogFactory.GetSaveFileDialog("Text file (*.linbuf)|*.linbuf");
            saveFileDialog.FileName = GetDefaultBufferFileName();

            if (saveFileDialog.ShowDialog() == true)
            {
                try
                {
                    _fileService.SaveFile<FramesBuffer>(saveFileDialog.FileName, FramesBuffer);
                    _log.Info($"Saved buffer file: '{saveFileDialog.FileName}', with frame count: {FramesBuffer.LinFrames.Count}");
                }
                catch (Exception e)
                {
                    _log.Error(e);
                    _messageService.ShowError(e.Message);
                }
            }
        }

        public RelayCommand OpenCmd { get; private set; }
        private async void Open()
        {
            var openFileDialog = DialogFactory.GetOpenFileDialog("Text file (*.linbuf)|*.linbuf");
            openFileDialog.FileName = GetDefaultBufferFileName();

            if (openFileDialog.ShowDialog() == true)
            {
                ProgressVisibility = Visibility.Visible;
                try
                {
                    Clear();
                    await Task.Run(() =>
                    {
                        FramesBuffer buffer = _fileService.OpenFile(openFileDialog.FileName);
                        _log.Info($"Open buffer file: '{openFileDialog.FileName}', with frame count: {buffer.LinFrames.Count}");

                        DispatcherHelper.CheckBeginInvokeOnUI(() =>
                        {
                            foreach (var f in buffer.LinFrames)
                            {
                                FramesBuffer.LinFrames.Add(f);
                            }

                            foreach (var of in buffer.ObservableLinFrames)
                            {
                                FramesBuffer.ObservableLinFrames.Add(of);
                            }
                        });
                    });
                    _log.Info($"All frames loaded in view");
                }
                catch (Exception e)
                {
                    _log.Error(e.Message);
                    _messageService.ShowError("Invalid file.");
                }
                ProgressVisibility = Visibility.Hidden;
            }
        }

        public RelayCommand CloseMainWindowCmd { get; private set; }
        private void CloseMainWindow()
        {
            try
            {
                if (_portManager.PortState)
                {
                    _log.Info($"Port {_portManager.CurrentPortName} was closed before app is shutdown");
                    _portManager.ClosePort();
                }
            }
            catch (SerialportManagerException e)
            {
                _log.Error(e);
                _messageService.ShowError(e.Message);
            }
            finally
            {
                _appConfig.LastSerialPort = _portManager.CurrentVendorPortName;
                _appConfig.Top = Top;
                _appConfig.Left = Left;
                _appConfig.Height = Height;
                _appConfig.Width = Width;
                _appConfig.WindowState = WindowState;
                _appConfig.FramesGridVisibility = GridVisibility;
                _appConfig.BitViewVisibility = BitViewVisibility;
                _appConfig.LinBaudrateSelectedIndex = LinBaudrateSelectedIndex;
                _appConfig.SelectedBaud = SelectedBaud;
                _appConfig.LastSerialPort = LastSerialPort;

                try
                {
                    _appConfigService.SaveConfig(_appConfig);
                }
                catch (Exception e)
                {
                    _messageService.ShowError(e.Message);
                }

                _log.Info("------------------------------- APP SHUTDOWND---------------------");
            }
        }

        public RelayCommand<object> ShowChartCmd { get; private set; }
        private void ShowChart(object frame)
        {
            var a = SelectedItem?.Value?.PID;
            if (a != null)
            {
                var filtredFrames = FramesBuffer.LinFrames.Where(f => f.PID == a);
                ChartViewModel chartViewModel = new ChartViewModel();
                chartViewModel.MakeChart(filtredFrames);
                var chartView = new ChartView();
                chartView.DataContext = chartViewModel;
                chartViewModel.Title = $"PID: {a:X2}";
                chartView.Show();

                _log.Info($"Show chart by PID {a:X2}");
            }
        }

        public RelayCommand<object> ShowIdStoryCmd { get; private set; }
        private void ShowIdStory(object frame)
        {
            var a = SelectedItem?.Value?.PID;
            if (a != null)
            {
                var filtredFrames = FramesBuffer.LinFrames.Where(f => f.PID == a);
                IdStoryViewModel idStoryViewModel = new IdStoryViewModel();
                idStoryViewModel.StoryFrames = new ObservableCollection<LinFrame>(filtredFrames);
                var isStoryView = new IdStoryView();
                isStoryView.DataContext = idStoryViewModel;
                isStoryView.Title = $"PID: {a:X2}";
                isStoryView.Show();

                _log.Info($"Show chart by PID {a:X2}");
            }
        }

        public RelayCommand<object> CheckAllCmd { get; private set; }
        private void CheckAll(object frame)
        {
            if (FramesBuffer.ObservableLinFrames.Count > 0)
            {
                foreach (var f in FramesBuffer.ObservableLinFrames)
                {
                    f.Value.IsWathing = true;
                }
            }
        }

        public RelayCommand<object> UnCheckAllCmd { get; private set; }
        private void UnCheckAll(object frame)
        {
            if (FramesBuffer.ObservableLinFrames.Count > 0)
            {
                foreach (var f in FramesBuffer.ObservableLinFrames)
                {
                    f.Value.IsWathing = false;
                }
            }
        }

        public RelayCommand EnableMessageHandlingCmd { get; private set; }
        private void EnableMessageHandling()
        {
            _isMessageHandling = true;
            CanStart = _isMessageHandling;

            _log.Info("Enable messages handling");
        }

        public RelayCommand DisableMessageHandlingCmd { get; private set; }
        private void DisableMessageHandling()
        {
            _isMessageHandling = false;
            CanStart = _isMessageHandling;

            _log.Info("Disable messages handling");
        }

        public RelayCommand StartStopCmd { get; private set; }
        private void StartStop()
        {
            if (!_isMessageHandling)
            {
                EnableMessageHandling();
            }
            else
            {
                DisableMessageHandling();
            }
        }

        public RelayCommand<int> SetBaudrateCmd { get; set; }
        private void SetBaudrate(int baudrate)
        {
            byte[] txBuff = new byte[4];
            txBuff.PutU16(0, (ushort)baudrate);

            _portManager.SendWpkg((byte)Commands.SetLinBaudrate, txBuff);
            Thread.Sleep(100);
            _portManager.SendWpkg((byte)Commands.GetLinBaudrate, txBuff); // REques new value from device

            _log.Info($"Set baudrate: {baudrate} kbits/s");
        }

        public RelayCommand<int> SetMasterTimeoutCmd { get; set; }
        private void SetMasterTimeout(int timeout)
        {
            byte[] txBuff = new byte[4];
            txBuff[0] = (byte)timeout;

            _portManager.SendWpkg((byte)Commands.SetMasterTimeout, txBuff);
            Thread.Sleep(100);
            _portManager.SendWpkg((byte)Commands.GetMasterTimeout, txBuff); // REques new value from device

            _log.Info($"Set master timeout: {timeout} ms");
        }

        public RelayCommand ShowAboutViewCmd { get; set; }
        private void ShowAboutView()
        {
            AboutView aboutView = new AboutView();
            aboutView.Owner = Application.Current.MainWindow;
            aboutView.Show();
        }

        public RelayCommand ShowHideDataGridCmd { get; set; }
        private void ShowHideDataGrid()
        {
            if (GridVisibility == Visibility.Visible)
            {
                GridVisibility = Visibility.Collapsed;
            }
            else
            {
                GridVisibility = Visibility.Visible;
            }
        }

        public RelayCommand ShowHideBitViewCmd{ get; set; }
        private void ShowHideBitView()
        {
            if (BitViewVisibility == Visibility.Visible)
            {
                BitViewVisibility = Visibility.Collapsed;
            }
            else
            {
                BitViewVisibility = Visibility.Visible;
            }
        }

        public RelayCommand UpdateSelectionCmd { get; set; }

        private void UpdateSelection()
        {
            if (SelectedItem?.Value != null)
            {
                SelectedItem.Value.SelectedByteIndex = (uint)CellInfo.Column.DisplayIndex - 3;
                if (SelectedItem.Value.SelectedByteIndex <= SelectedItem.Value.Data.Length)
                {
                    var item = SelectedItem.Value.Data[SelectedItem.Value.SelectedByteIndex];
                    if (item?.Item != null)
                    {
                        FramesBuffer.SelectedByte.SetBitByte((byte)item.Item);
                    }
                }
            }
        }

        private string GetDefaultBufferFileName()
        {
            return $"buffer_{DateTime.Now:_MMddyyyy_HHmmss}.linbuf";
        }

        // Init all command for view
        private void CmdInit()
        {
            ShowConnectionSetupCmd = new RelayCommand(ShowConnectionSetup);
            ShowSlaveSendingViewCmd = new RelayCommand(ShowSlaveSendingView);
            RefreshPortListCmd = new RelayCommand(RefreshPortList);
            ChangeConnetionStateCmd = new RelayCommand<string>(ChangeConnetionState);
            ClearCmd = new RelayCommand(Clear);
            SaveCmd = new RelayCommand(Save);
            OpenCmd = new RelayCommand(Open);
            CloseMainWindowCmd = new RelayCommand(CloseMainWindow);
            ShowChartCmd = new RelayCommand<object>(ShowChart);
            ShowIdStoryCmd = new RelayCommand<object>(ShowIdStory);
            EnableMessageHandlingCmd = new RelayCommand(EnableMessageHandling);
            DisableMessageHandlingCmd = new RelayCommand(DisableMessageHandling);
            StartStopCmd = new RelayCommand(StartStop);
            SetBaudrateCmd = new RelayCommand<int>(SetBaudrate);
            SetMasterTimeoutCmd = new RelayCommand<int>(SetMasterTimeout);
            ShowAboutViewCmd = new RelayCommand(ShowAboutView);
            ShowHideDataGridCmd = new RelayCommand(ShowHideDataGrid);
            ShowHideBitViewCmd = new RelayCommand(ShowHideBitView);
            UpdateSelectionCmd = new RelayCommand(UpdateSelection);
            CheckAllCmd = new RelayCommand<object>(CheckAll);
            UnCheckAllCmd = new RelayCommand<object>(UnCheckAll);
            ShowMasterSendingViewCmd = new RelayCommand(ShowMasterSendingView);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="portManager"></param>
        public MainViewModel
            (
            ISerialPortManager portManager,
            IMessageService messageService,
            IFileService fileService,
            ChartViewModel chartViewModel,
            SlaveSenderViewModel slaveSenderViewModel,
            MasterSenderViewModel masterSenderViewModel
            )
        {
            _log.Info("------------------------------- APP STARTED---------------------");

            _portManager = portManager;
            _messageService = messageService;
            _fileService = fileService;
            _chatViewModel = chartViewModel;
            //            _slaveSenderViewModel = new SlaveSenderViewModel(_portManager, null, null);
            //            _masterSenderViewModel = new MasterSenderViewModel(_portManager, null, null);
            _slaveSenderViewModel = slaveSenderViewModel;
            _masterSenderViewModel = masterSenderViewModel;

            FramesBuffer = new FramesBuffer();
            _sw = new Stopwatch();

            PortList = new ObservableCollection<string>(portManager.GetPortList());
            TimeoutList = new ObservableCollection<int>() { 3, 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 150, 200, 250 }; // Value in [ms]
            BaudrateList = new ObservableCollection<int>() { 2400, 9600, 10000, 10500, 19200 }; // Value in [Kbits/s]
            _portManager.OnReceiving += _portManager_OnReceiving;

            CmdInit();

            AppConfigInit();

            DispatcherHelper.Initialize();
        }

        private void AppConfigInit()
        {
            _appConfigService = new AppConfigService();
            _appConfig = _appConfigService.ReadConfig();

            Top = _appConfig.Top;
            Left = _appConfig.Left;
            Height = _appConfig.Height;
            Width = _appConfig.Width;
            WindowState = _appConfig.WindowState;
            GridVisibility = _appConfig.FramesGridVisibility;
            BitViewVisibility = _appConfig.BitViewVisibility;
            LinBaudrateSelectedIndex = _appConfig.LinBaudrateSelectedIndex;
            LastSerialPort = _appConfig.LastSerialPort;
            SelectedBaud = _appConfig.SelectedBaud;
            PortList.Add(_appConfig.LastSerialPort);
            ChangeConnetionState(_appConfig.LastSerialPort);
        }

        private void ResetLabels()
        {
            DeviceName = string.Empty;
            DeviceVersion = string.Empty;
            LinBaudrate = 0;
            MasterTimeout = 0;
        }

        private void CheckFirmwareVersion(string factFersion)
        {
            if (factFersion != LatestFwVersion)
            {
                _messageService.ShowInfo($"Update device to firmware version: {LatestFwVersion}\n \nGot to: https://can-bus-automotive.gitlab.io/Page/index.html");
            }
        }

        private void _portManager_OnReceiving(object sender, byte[] e)
        {
            if (e.IsValidWpkgCrc())
            {
                switch (e[(int)Definitions.CmdPtr])
                {
                    case (byte)(Commands.GetDeviceName):
                        DeviceName = e.MapToString();
                        _log.Info($"Device name: {DeviceName}");
                        break;

                    case (byte)(Commands.GetDeviceVersion):
                        DeviceVersion = e.MapToVersion();
#if !DEBUG
                        CheckFirmwareVersion(DeviceVersion);
#endif
                        _log.Info($"Device version: {DeviceVersion}");
                        break;

                    case (byte)(Commands.GetMasterTimeout):
                        MasterTimeout = e.MapToTimeout();
                        break;

                    case (byte)(Commands.GetLinBaudrate):
                        LinBaudrate = e.MapToBaudrate();
                        break;

                    case (byte)(Commands.IncomingLinFrame):
                        if (_isMessageHandling)
                        {
                            var ts = _sw.Elapsed;
                            var frame = e.MapToLinFrame();
                            frame.TtimeMarker = ts;
                            FramesBuffer.InsertFrame(frame);
                        }
                        break;

                    case (byte)(Commands.ResetPidFilter):
                        _slaveSenderViewModel.RxFrameHandler(e[1]);
                        break;
                }
            }
        }
    }
}