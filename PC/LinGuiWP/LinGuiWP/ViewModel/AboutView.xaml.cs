﻿using System.Diagnostics;
using System.Reflection;
using System.Windows;

namespace LinGuiWP.ViewModel
{
    /// <summary>
    /// Interaction logic for AboutView.xaml
    /// </summary>
    public partial class AboutView : Window
    {
        public AboutView()
        {
            InitializeComponent();

            VersionLabel.Content = Version;
            FullVersion.Text = FileVersionInfo.GetVersionInfo
                (Assembly.GetExecutingAssembly().Location).ToString();
        }

        public string Version
        {
            get
            {
                return $"Version: Stable {Assembly.GetExecutingAssembly().GetName().Version.ToString()}";
            }
        }

        private void Hyperlink_RequestNavigate(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
        {
            Process.Start(e.Uri.AbsoluteUri);
            e.Handled = true;
        }
    }
}
