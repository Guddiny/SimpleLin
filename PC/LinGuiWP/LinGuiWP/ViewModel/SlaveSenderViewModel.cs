﻿using LinGuiWP.BL.ConnectionManagment;
using LinGuiWP.BL.WProtocol;
using LinGuiWP.Helpers;
using LinGuiWP.Mappers;
using LinGuiWP.Models;
using System.Linq;
using LinGuiWP.Services.Abstract;
using System.Collections.Generic;
using System.Threading;
using System;
using GalaSoft.MvvmLight.Command;

namespace LinGuiWP.ViewModel
{
    public class SlaveSenderViewModel : SenderViemodelBase
    {
        public override void StartOne(int index)
        {
            var frameSenderEntity = FrameEntities.FirstOrDefault(f => f.Key == index);

            if (frameSenderEntity != null)
            {
                frameSenderEntity.Value.IsStarted = true;
                frameSenderEntity.Value.CanChange = false;
                FrameEntitiesActiv.Add(frameSenderEntity.Key, frameSenderEntity.Value);

                PortManager.SendWpkg((byte)Commands.SetPidFilter, GetPids()); // Update PID list in MCU
            }
        }

        public byte[] GetPids()
        {
            byte[] pids = new byte[18];

            for (int i = 0; i < FrameEntities.Count; i++)
            {
                if (!pids.Contains(FrameEntities[i].Value.Frame.PID))
                {
                    pids[i] = FrameEntities[i].Value.Frame.PID;
                }
            }

            return pids;
        }

        public RelayCommand SetPidFilterCmd { get; set; }

        /// <summary>
        /// Set PIDs for response
        /// </summary>
        public void SetPidFilter()
        {
            ResetPidFilter();
            Thread.Sleep(100);
            foreach (var f in FrameEntities)
            {
                if (f.Value.IsEnabled && f.Value.Frame.PID != 0x00 && f.Value.Frame.DataQty != 0)
                {
                    PortManager.SendWpkg((byte)Commands.SetPidFilter, f.Value.Frame.MapToByteArray());
                    Thread.Sleep(70);
                }
            }
        }


        public RelayCommand ResetPidFilterCmd { get; set; }

        /// <summary>
        /// Seret all PIDs for response
        /// </summary>
        public void ResetPidFilter()
        {
            PortManager.SendWpkg((byte)Commands.ResetPidFilter, new byte[] { 0x00 });
        }

        /// <summary>
        ///  Stop one frame
        /// </summary>
        public override void StopOne(int index)
        {
            var frameSenderEntity = FrameEntities.FirstOrDefault(f => f.Key == index);

            if (frameSenderEntity != null)
            {
                frameSenderEntity.Value.IsStarted = false;
                frameSenderEntity.Value.CanChange = true;
                FrameEntitiesActiv.Remove(frameSenderEntity.Key);

                PortManager.SendWpkg((byte)Commands.SetPidFilter, GetPids()); // Update PID list in MCU
            }
        }

        /// <summary>
        /// Stop all active frames
        /// </summary>
        public override void StopAll()
        {
            foreach (var f in FrameEntities)
            {
                if (f.Value.IsStarted)
                {
                    StopOne(f.Key);
                }
            }
        }

        /// <summary>
        /// Add one frame to frame sending list
        /// </summary>
        protected override void AddOne()
        {
            if (FrameEntities.Count < MaxFramesForSend)
            {
                var key = GetKey();
                var entity = new FrameSenderEntity
                {
                    Frame = new Models.Lin.LinFrame
                    {
                        DataQty = 0,
                        PID = 0,
                        Data = new byte?[8] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 },
                        State = Models.Lin.FrameState.OK,
                        CRC = 0
                    },
                    EntityMode = EntityMode.SingleShot,
                    IsEnabled = true,
                    IsStarted = false,
                    IsMaster = false,
                    IsUseIncrement = true,
                    IncrementValues = new byte?[8] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 },
                    MsgDelay = 500,
                    RepetitionsNumber = 10
                };

                FrameEntities.Add(key, entity);
            }
        }

        public override void RxFrameHandler(byte pid)
        {
            var frameEntity = FrameEntitiesActiv.FirstOrDefault(f => f.Value.Frame.PID == pid);
            if (!frameEntity.Equals(default(KeyValuePair<int, FrameSenderEntity>)))
            {
                UpdateIncrement(frameEntity.Value);
                PortManager.SendWpkg((byte)Commands.SetSlaveFrame, frameEntity.Value.Frame.MapToByteArray());
                frameEntity.Value.IsStarted = false;
                frameEntity.Value.SendCount++;

                if (!frameEntity.Value.IsEveryTime)
                {
                    FrameEntitiesActiv.Remove(frameEntity.Key);

                    // TODO Maybe need "SetPidFilter cmd" again... 
                }
            }
        }

        public SlaveSenderViewModel(ISerialPortManager portManager, IFileService fileService,
            IMessageService messageService) : base(portManager, fileService, messageService)
        {
            CmdInit();
        }

        private void CmdInit()
        {
            SetPidFilterCmd = new RelayCommand(SetPidFilter);
            ResetPidFilterCmd = new RelayCommand(ResetPidFilter);
        }
    }
}