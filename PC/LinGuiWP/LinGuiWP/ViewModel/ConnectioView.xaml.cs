﻿using System.Windows;

namespace LinGuiWP.ViewModel
{
    /// <summary>
    /// Interaction logic for ConnectioView.xaml
    /// </summary>
    public partial class ConnectioView : Window
    {
        public ConnectioView()
        {
            InitializeComponent();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            this.Visibility = Visibility.Hidden;
        }
    }
}
