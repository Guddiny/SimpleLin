﻿using GalaSoft.MvvmLight.Command;
using LinGuiWP.BL.ConnectionManagment;
using LinGuiWP.Helpers;
using LinGuiWP.Models;
using LinGuiWP.Services.Abstract;
using Microsoft.Win32;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using GalaSoft.MvvmLight;

namespace LinGuiWP.ViewModel
{
    public abstract class SenderViemodelBase : ViewModelBase
    {
        private readonly Logger _log = LogManager.GetCurrentClassLogger();
        protected readonly ISerialPortManager PortManager;
        private readonly IFileService _fileService;
        private readonly IMessageService _messageService;
        protected const int MaxFramesForSend = 18;

        private const string DefaultSendFrameFileName = "send_frames";

        public ObservablePairCollection<int, FrameSenderEntity> FrameEntities { get; set; }
        public Dictionary<int, FrameSenderEntity> FrameEntitiesActiv { get; set; }
        public ObservableCollection<int> PosibleDataQty { get; set; }
        public ObservableCollection<byte> ValidPids { get; set; }

        public int GetMaxFramesForSend
        {
            get { return MaxFramesForSend; }
        }

        private string GetDefaultSendFrameFileName()
        {
            return $"send_frames_{DateTime.Now:_MMddyyyy_HHmmss}.frm";
        }

        // Constructor
        public SenderViemodelBase(ISerialPortManager portManager, IFileService fileService,
            IMessageService messageService)
        {
            PortManager = portManager;
            _fileService = fileService;
            _messageService = messageService;

            FrameEntitiesActiv = new Dictionary<int, FrameSenderEntity>();
            FrameEntities = new ObservablePairCollection<int, FrameSenderEntity>();

            PosibleDataQty = new ObservableCollection<int> { 0, 1, 2, 3, 4, 5, 6, 7, 8 };
            ValidPids = new ObservableCollection<byte>
            {
                0x80,
                0xC1,
                0x42,
                0x03,
                0xc4,
                0x85,
                0x06,
                0x47,
                0x08,
                0x49,
                0xCA,
                0x8B,
                0x4C,
                0x0D,
                0x8E,
                0xCF,
                0x50,
                0x11,
                0x92,
                0xD3,
                0x14,
                0x55,
                0xD6,
                0x97,
                0xD8,
                0x99,
                0x1A,
                0x5B,
                0x9C,
                0xDD,
                0x5E,
                0x1F,
                0x20,
                0x61,
                0xE2,
                0xA3,
                0x64,
                0x25,
                0xA6,
                0xE7,
                0xA8,
                0xE9,
                0x6A,
                0x2B,
                0xEC,
                0xAD,
                0x2E,
                0x6F,
                0xF0,
                0xB1,
                0x32,
                0x73,
                0xB4,
                0xF5,
                0x76,
                0x37,
                0x78,
                0x39,
                0xBA,
                0xFB,
                0x3C,
                0x7D,
                0xFE,
                0xBF
            };

            AvailablePids = new ObservableCollection<byte>(ValidPids);

            CmdInit();
        }

        /// <summary>
        /// Update data byte in frame by increment value
        /// </summary>
        /// <param name="frameSenderEntity"></param>
        public void UpdateIncrement(FrameSenderEntity frameSenderEntity)
        {
            if (frameSenderEntity.IsUseIncrement)
            {
                for (int i = 0; i < frameSenderEntity.Frame.DataQty - FrameSenderEntity.DclCorrection; i++)
                {
                    frameSenderEntity.Frame.Data[i] += frameSenderEntity.IncrementValues[i];
                }
            }
        }

        /// <summary>
        /// Get netx unique key(index) for frame sending list
        /// </summary>
        /// <returns>Key</returns>
        public int GetKey()
        {
            if (FrameEntities.Count > 0)
            {
                var tmpKey = FrameEntities.Last().Key;
                var a = tmpKey + 1;
                return a;
            }

            return 0;
        }

        // Commands

        /// <summary>
        ///  Start or stop message handling
        /// </summary>
        public RelayCommand<int> StartStopCmd { get; private set; }

        private void StartStop(int index)
        {
            var frameSenderEntity = FrameEntities.FirstOrDefault(f => f.Key == index);
            if (frameSenderEntity?.Value != null && frameSenderEntity.Value.IsEnabled)
            {
                if (frameSenderEntity.Value.IsStarted)
                {
                    StopOne(frameSenderEntity.Key);
                }
                else
                {
                    StartOne(frameSenderEntity.Key);
                }
            }
            else
            {
                _log.Error($"Null value in {frameSenderEntity}");
            }
        }

        public ObservableCollection<byte> AvailablePids { get; set; }

        public virtual RelayCommand UpdateAvailablePidsCmd { get; private set; }

        private void UpdateAvailablePids()
        {
            //var usedIdList = new ObservableCollection<byte>();
            //foreach (var p in FrameEntities)
            //{
            //    usedIdList.Add(p.Value.Frame.PID);
            //}
            //var tmpArr = ValidPids.Except(usedIdList);
            //foreach (var p in usedIdList)
            //{
            //    AvailablePids.Remove(p);
            //}
        }

        /// <summary>
        /// Start frame sending
        /// </summary>
        public RelayCommand<int> StartOneCmd { get; private set; }

        public abstract void StartOne(int index);


        /// <summary>
        /// Stop one frame
        /// </summary>
        public RelayCommand<int> StopOneCmd { get; private set; }

        public abstract void StopOne(int index);

        /// <summary>
        /// Stop all active frames
        /// </summary>
        public abstract void StopAll();

        /// <summary>
        /// Add one frame to frame sending list
        /// </summary>
        public RelayCommand AddOneCmd { get; private set; }

        protected abstract void AddOne();

        /// <summary>
        /// Delete one frame from sending list
        /// </summary>
        public virtual RelayCommand<int> DeleteOneCmd { get; private set; }

        public void DeleteOne(int index)
        {
            var first = FrameEntities.FirstOrDefault(f => f.Key == index);

            if (first != null)
            {
                FrameEntities.Remove(first);
                FrameEntitiesActiv.Remove(first.Key);
            }
            else
            {
                _log.Warn("Element not found in collection FrameEntities");
            }
        }

        /// <summary>
        /// Delete all frames from sending list
        /// </summary>
        public RelayCommand DeleteAllCmd { get; private set; }

        private void DeleteAll()
        {
            FrameEntities.Clear();
            FrameEntitiesActiv.Clear();
        }

        /// <summary>
        /// Save full farme sending list to file
        /// </summary>
        public RelayCommand SaveSendFramesCmd { get; private set; }

        private void SaveSendFrames()
        {
            var saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text file (*.frm)|*.frm";
            saveFileDialog.FileName = GetDefaultSendFrameFileName();

            if (saveFileDialog.ShowDialog() == true)
            {
                try
                {
                    _fileService.SaveFile<ObservablePairCollection<int, FrameSenderEntity>>(saveFileDialog.FileName, FrameEntities);
                    _log.Info(
                        $"Saved buffer file: '{saveFileDialog.FileName}', with frame count: {FrameEntities.Count}");
                }
                catch (Exception e)
                {
                    _log.Error(e.Message);
                    _messageService.ShowError(e.Message);
                }
            }
        }

        /// <summary>
        /// Read frame sending list from file
        /// </summary>
        public RelayCommand OpenSendFramesCmd { get; private set; }

        public void OpenSendFrames()
        {
            var openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text file (*.frm)|*.frm";
            openFileDialog.FileName = "send_frames.frm";

            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    FrameEntities.Clear();
                    var frameEntities = _fileService.OpenCmdFile(openFileDialog.FileName);
                    foreach (var entity in frameEntities)
                    {
                        FrameEntities.Add(entity);
                    }

                    _log.Info(
                        $"Send frames was loaded from file: {openFileDialog.FileName}, qty: {frameEntities.Count}");
                }
                catch (Exception e)
                {
                    _log.Error(e.Message);
                    _messageService.ShowError(e.Message);
                }
            }
        }

        public abstract void RxFrameHandler(byte pid);

        private void CmdInit()
        {
            StartOneCmd = new RelayCommand<int>(StartOne);
            StopOneCmd = new RelayCommand<int>(StopOne);
            AddOneCmd = new RelayCommand(AddOne);
            DeleteAllCmd = new RelayCommand(DeleteAll);
            DeleteOneCmd = new RelayCommand<int>(DeleteOne);
            SaveSendFramesCmd = new RelayCommand(SaveSendFrames);
            OpenSendFramesCmd = new RelayCommand(OpenSendFrames);
            StartStopCmd = new RelayCommand<int>(StartStop);
            UpdateAvailablePidsCmd = new RelayCommand(UpdateAvailablePids);
        }
    }
}