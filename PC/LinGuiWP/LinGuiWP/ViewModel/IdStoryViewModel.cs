﻿using GalaSoft.MvvmLight;
using LinGuiWP.Models.Lin;
using System.Collections.ObjectModel;

namespace LinGuiWP.ViewModel
{
    public class IdStoryViewModel:ViewModelBase
    {
        public ObservableCollection<LinFrame> StoryFrames { get; set; }
    }
}
