﻿using System.ComponentModel;
using System.Windows;

namespace LinGuiWP.ViewModel
{
    /// <summary>
    /// Interaction logic for MasterSenderView.xaml
    /// </summary>
    public partial class MasterSenderView : Window
    {
        public MasterSenderView()
        {
            InitializeComponent();
        }

        public new void Close()
        {
            this.Visibility = Visibility.Hidden;
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
            Close();
        }
    }
}
