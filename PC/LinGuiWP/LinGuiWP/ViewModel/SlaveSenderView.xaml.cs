﻿using System.ComponentModel;
using System.Windows;

namespace LinGuiWP.ViewModel
{
    public partial class SlaveSenderView : Window
    {
        public SlaveSenderView()
        {
            InitializeComponent();
        }

        public new void Close()
        {
            this.Visibility = Visibility.Hidden; 
        }

        private void FrameSenderView_OnClosing(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
            Close();
        }
    }
}
