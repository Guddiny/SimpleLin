﻿using System.Globalization;
using System.Windows.Controls;
using System.Windows.Input;

namespace LinGuiWP.ViewModel
{
    public partial class SlaveFrameEntity : UserControl
    {
        public SlaveFrameEntity()
        {
            InitializeComponent();
        }

        private void OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int hexNumber;
            e.Handled = !int.TryParse(e.Text, NumberStyles.HexNumber, CultureInfo.CurrentCulture, out hexNumber);
        }
    }
}
