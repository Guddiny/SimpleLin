﻿using System;
using LinGuiWP.BL.ConnectionManagment;
using LinGuiWP.BL.WProtocol;
using LinGuiWP.Helpers;
using LinGuiWP.Mappers;
using LinGuiWP.Models;
using LinGuiWP.Services.Abstract;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;

namespace LinGuiWP.ViewModel
{
    public class MasterSenderViewModel : SenderViemodelBase
    {
        /// <summary>
        /// Start frame sending
        /// </summary>
        public override void StartOne(int index)
        {
            var frameSenderEntity = FrameEntities.FirstOrDefault(f => f.Key == index);

            if (frameSenderEntity != null)
            {
                frameSenderEntity.Value.IsStarted = true;
                frameSenderEntity.Value.CanChange = false;
            }
        }

        /// <summary>
        /// Stop one frame
        /// </summary>
        public override void StopOne(int index)
        {
            var frameSenderEntity = FrameEntities.FirstOrDefault(f => f.Key == index);

            if (frameSenderEntity != null)
            {
                frameSenderEntity.Value.IsStarted = false;
                frameSenderEntity.Value.CanChange = true;
            }
        }

        /// <summary>
        /// Stop all active frames
        /// </summary>
        public override void StopAll()
        {
            foreach (var f in FrameEntities)
            {
                if (f.Value.IsStarted)
                {
                    StopOne(f.Key);
                }
            }
        }

        /// <summary>
        /// Add one frame to frame sending list
        /// </summary>
        protected override void AddOne()
        {
            if (FrameEntities.Count < MaxFramesForSend)
            {
                var key = GetKey();
                var entity = new FrameSenderEntity
                {
                    Frame = new Models.Lin.LinFrame
                    {
                        DataQty = 0,
                        PID = 0x80,
                        Data = new byte?[8] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 },
                        State = Models.Lin.FrameState.OK,
                        CRC = 0
                    },
                    EntityMode = EntityMode.SingleShot,
                    IsEnabled = true,
                    IsStarted = false,
                    IsMaster = true,
                    IsUseIncrement = true,
                    IncrementValues = new byte?[8] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 },
                    MsgDelay = 500,
                    RepetitionsNumber = 10
                };

                FrameEntities.Add(key, entity);
            }
        }

        public override void RxFrameHandler(byte pid)
        {
            throw new InvalidOperationException("Master can't handle received slave frame");
        }

        public void SendHandler()
        {
            Task.Run(() =>
            {
                while (true)
                {
                    Thread.Sleep(TimeSpan.FromMilliseconds(1));
                    foreach (var frameEntity in FrameEntities)
                    {
                        if (frameEntity.Value.IsEveryTime && frameEntity.Value.IsStarted)
                        {
                            if (frameEntity.Value.Timeout / 2 <= frameEntity.Value.CurrentTimeStamp)
                            {
                                if (frameEntity.Value.Frame.DataQty == 0)
                                {
                                    PortManager.SendWpkg((byte)Commands.SetMasterHeader, new byte[] { frameEntity.Value.Frame.PID });
                                }
                                else
                                {
                                    PortManager.SendWpkg((byte)Commands.SetMasterFrame,
                                                                      frameEntity.Value.Frame.MapToByteArray());
                                    UpdateIncrement(frameEntity.Value);
                                }
                                frameEntity.Value.CurrentTimeStamp = 0;
                            }
                            else
                            {
                                frameEntity.Value.CurrentTimeStamp++;
                            }
                        }
                        else if (!frameEntity.Value.IsEveryTime && frameEntity.Value.IsStarted)
                        {
                            if (frameEntity.Value.Frame.DataQty == 0)
                            {
                                PortManager.SendWpkg((byte)Commands.SetMasterHeader, new byte[] { frameEntity.Value.Frame.PID });
                            }
                            else
                            {
                                PortManager.SendWpkg((byte)Commands.SetMasterFrame,
                                       frameEntity.Value.Frame.MapToByteArray());
                                UpdateIncrement(frameEntity.Value);
                            }
                            StopOne(frameEntity.Key);
                        }
                    }
                }
            });
        }


        public MasterSenderViewModel(ISerialPortManager portManager, IFileService fileService,
            IMessageService messageService) : base(portManager, fileService, messageService)
        {
            SendHandler();
        }
    }
}