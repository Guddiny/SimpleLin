﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight;
using LinGuiWP.Models.Lin;
using OxyPlot;
using OxyPlot.Series;
using GalaSoft.MvvmLight.Command;
using System;
using OxyPlot.Axes;

namespace LinGuiWP.ViewModel
{
    public class ChartViewModel : ViewModelBase
    {
        public ObservableCollection<DataPoint> Points { get; private set; }
        public ObservableCollection<DataPoint>[] ChartPoints { get; private set; }
        public PlotModel ChartModel { get; set; }
        List<OxyColor> SetColors { get; set; }

        private string _title = "";
        public string Title
        {
            get { return _title; }
            set
            {
                Set(() => Title, ref _title, value);
            }
        }

        // Commands
        public RelayCommand<string> ShowHideSeriesCmd { get; private set; }
        private void ShowHideSeries(string number)
        {
            int tempNumber = Int32.Parse(number);

            if (ChartModel.Series.Count > tempNumber)
            {
                if (ChartModel.Series[tempNumber].IsVisible)
                {
                    ChartModel.Series[tempNumber].IsVisible = false;
                }
                else
                {
                    ChartModel.Series[tempNumber].IsVisible = true;
                }
                ChartModel.InvalidatePlot(true);
            }
        }

        public RelayCommand HideAllSeriesCmd { get; private set; }
        private void HideAllSeries()
        {
            foreach (var s in ChartModel.Series)
            {
                s.IsVisible = false;
            }

            ChartModel.InvalidatePlot(true);
        }

        public RelayCommand ShowAllSeriesCmd { get; private set; }
        private void ShowAllSeries()
        {
            foreach (var s in ChartModel.Series)
            {
                s.IsVisible = true;
            }

            ChartModel.InvalidatePlot(true);
        }

        private void CmdInit()
        {
            ShowHideSeriesCmd = new RelayCommand<string>(ShowHideSeries);
            HideAllSeriesCmd = new RelayCommand(HideAllSeries);
            ShowAllSeriesCmd = new RelayCommand(ShowAllSeries);
            UpdateChartCmd = new RelayCommand(UpdateChart);
        }

        private void ViewInit()
        {
            ChartModel.LegendTitle = "Legend";
            ChartModel.LegendOrientation = LegendOrientation.Vertical;
            ChartModel.LegendPlacement = LegendPlacement.Inside;
            ChartModel.LegendPosition = LegendPosition.TopRight;
            ChartModel.LegendBackground = OxyColor.FromAColor(200, OxyColors.White);
            ChartModel.LegendBorder = OxyColors.Black;

            ChartModel.PlotAreaBorderColor = OxyColors.LightGray;

            var xAxis = new LinearAxis()
            {
                ExtraGridlineColor = OxyColors.LightGray,
                TextColor = OxyColors.LightGray,
                MajorGridlineColor = OxyColors.DarkSlateGray,
                MinorGridlineColor = OxyColors.DarkSlateGray,
                MajorGridlineStyle = LineStyle.Dot,
                MajorGridlineThickness = 1,
                Title = "Value",
                TitleColor = OxyColors.LightGray,
                Position = AxisPosition.Left,
                LabelFormatter = (d) => $"0x{Convert.ToInt32(d):X}"
            };
            var yAxis = new LinearAxis()
            {
                Title = "Time",
                TitleColor = OxyColors.LightGray,
                ExtraGridlineColor = OxyColors.LightGray,
                TextColor = OxyColors.LightGray,
                MajorGridlineColor = OxyColors.DarkSlateGray,
                MinorGridlineColor = OxyColors.DarkSlateGray,
                MajorGridlineStyle = LineStyle.Dot,
                MajorGridlineThickness = 1,
                Position = AxisPosition.Bottom
            };
            ChartModel.Axes.Add(xAxis);
            ChartModel.Axes.Add(yAxis);
        }

        public ChartViewModel()
        {
            ChartPoints = new ObservableCollection<DataPoint>[10];
            Points = new ObservableCollection<DataPoint>();
            ChartModel = new PlotModel();

            SetColors = new List<OxyColor>
            {
                OxyColors.Red,
                OxyColors.Orange,
                OxyColors.Yellow,
                OxyColors.Green,
                OxyColors.Blue,
                OxyColors.Indigo,
                OxyColors.Violet,
                OxyColors.Azure
            };

            ViewInit();
            CmdInit();
        }

        public RelayCommand UpdateChartCmd { get; private set; }
        public void UpdateChart()
        {
            ChartModel.InvalidatePlot(true);
        }

        public void HideAllCharts()
        {
            foreach (var series in ChartModel.Series)
            {
                series.IsVisible = false;
            }

            ChartModel.InvalidatePlot(true);
        }

        public void ShowAllCharts()
        {
            foreach (var series in ChartModel.Series)
            {
                series.IsVisible = true;
            }

            ChartModel.InvalidatePlot(true);
        }

        public void MakeChart(IEnumerable<LinFrame> frames)
        {
            int timeLine = 0;
            foreach (var f in frames)
            {
                if (f.State == FrameState.Classic || f.State == FrameState.Enhanced || f.State == FrameState.OK)
                {
                    for (int i = 0; i < f.DataQty; i++)
                    {
                        if (ChartModel.Series.Count < (i + 1)) // Add new chart to chart series
                        {
                            ChartModel.Series.Add(new LineSeries() { Title = $"B{i}" });
                        }

                        LineSeries a = (LineSeries)ChartModel.Series[i];
                        a.Color = SetColors[i];
                        a.Points.Add(new DataPoint(timeLine, Convert.ToDouble(f.Data[i])));
                        timeLine++;
                    }
                }
            }
        }
    }
}
