﻿using System;
using System.Globalization;
using System.Windows.Controls;

namespace LinGuiWP.Validators
{
    public class HexStringValidator:ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            try
            {
                var hexString = value as string;
                byte? result = Convert.ToByte(hexString, 16);
                return new ValidationResult(true, null);
            }
            catch (Exception )
            {
                return new ValidationResult(false, "Only HEX value");
            }
        }
    }
}