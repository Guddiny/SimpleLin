﻿using NLog;
using System.Runtime;
using System.Windows;
using System.Windows.Threading;

namespace LinGuiWP
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        private Logger _log = LogManager.GetCurrentClassLogger();

        private void App_OnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            _log.Error(e.Exception);
            e.Handled = false;
#if DEBUG
            throw e.Exception;
#endif
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            // NOTE Prety danger change
            GCSettings.LatencyMode = GCLatencyMode.SustainedLowLatency;

            ShutdownMode = ShutdownMode.OnMainWindowClose;
        }
    }
}
