﻿namespace LinGuiWP.Mappers
{
    public static class VersionMapper
    {
        public static string MapToVersion(this byte[] arr)
        {
            var result = $"ver. {(int) arr[1]}.{(int) arr[2]}";

            return result;
        }
    }
}
