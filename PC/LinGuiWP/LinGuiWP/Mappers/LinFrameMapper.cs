﻿using LinGuiWP.Models.Lin;

namespace LinGuiWP.Mappers
{
    public static class LinFrameMapper
    {
        public static LinFrame MapToLinFrame(this byte[] arr)
        {
            LinFrame frame = new LinFrame();

            frame.PID = arr[3]; // See protocol https://docs.google.com/document/d/1k_0_KEaK4MgFOL2ShaHvgRaSgq0yzDBagRkQHupUjd0/edit#
            frame.Data = new byte?[20];
            frame.DataQty = arr[1] - 3; // See protocol https://docs.google.com/document/d/1k_0_KEaK4MgFOL2ShaHvgRaSgq0yzDBagRkQHupUjd0/edit#
           
            int j = 4; // See protocol https://docs.google.com/document/d/1k_0_KEaK4MgFOL2ShaHvgRaSgq0yzDBagRkQHupUjd0/edit#
            for (int i = 0; i < frame.DataQty; i++)
            {
                frame.Data[i] = arr[j];
                j++;
            }

            frame.CRC = arr[j];

            LinFrameConstraints.ValidateCrc(frame);
            LinFrameConstraints.ValidateParity(frame);

            return frame;
        }


        public static ObservableLinFrame MapToObservableLinFrame(this LinFrame frame)
        {
            ObservableLinFrame obsFrame = new ObservableLinFrame();
            obsFrame.PID = frame.PID;

            ObservableItem[] items = new ObservableItem[frame.Data.Length];
            for (int i = 0; i < frame.Data.Length; i++)
            {
                var dataItem = new ObservableItem { Item = frame.Data[i] };
                items[i] = dataItem;
            }
            obsFrame.Data = items;

            obsFrame.TimeMarker = frame.TtimeMarker;
            obsFrame.ID = frame.ID;
            obsFrame.CRC = frame.CRC;
            obsFrame.DataQty = frame.DataQty;

            return obsFrame;
        }
    }
}
