﻿using System;

namespace LinGuiWP.Mappers
{
    public static class StringMapper
    {
        public static string MapToString(this byte[] arr)
        {
            var result = String.Empty;
            for (int i = 1; i < arr.Length - 2; i++)
            {
                if (arr[i] == '\n')
                {
                    return result;
                }
                result += (char) (arr[i]);
            }

            return result;
        }
    }
}