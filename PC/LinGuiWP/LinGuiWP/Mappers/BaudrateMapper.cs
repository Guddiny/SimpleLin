﻿using System;
using LinGuiWP.Helpers;

namespace LinGuiWP.Mappers
{
    public static class BaudrateMapper
    {
        public static UInt16 MapToBaudrate(this byte[] arr)
        {
            UInt16 result = 0x0000;
            result = arr.GetU16(1);

            return result;
        }
    }
}