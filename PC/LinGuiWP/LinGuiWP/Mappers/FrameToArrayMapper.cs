﻿using LinGuiWP.Models.Lin;
using System;

namespace LinGuiWP.Mappers
{
    public static class FrameToArrayMapper
    {
        public static byte[] MapToByteArray(this LinFrame linFrame)
        {
            byte[] txData = new byte[13];
            txData[0] = (byte)linFrame.DataQty;
            txData[1] = linFrame.PID;
            int j = 2;
            for (int i = 0; i < linFrame.DataQty-2; i++)
            {
                txData[j] = Convert.ToByte(linFrame.Data[i]);
                j++;
            }
            txData[j] = Convert.ToByte(linFrame.CRC);
            txData[12] = (byte)linFrame.CrcType;

            return txData;
        }
    }
}
