﻿using LinGuiWP.BL.WProtocol;
using NUnit.Framework;
using LinGuiWP.Helpers;

namespace LinGuiWP.Tests
{
    [TestFixture]
    class CmdCrcTests
    {
        byte[] data1 = new byte[(int)Definitions.TxRxArratySize]
        {
            0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16,
            0x17, 0x18, 0x19
        };

        [Test]
        public void CmdCrcExtensionTest_ValidCRC()
        {
            byte expCrc = 0x19;
            byte actCrc = data1.GetCmdCrc();

            Assert.That(actCrc, Is.EqualTo(expCrc));
        }

        [Test]
        public void CmdCrcExtensionTest_ValidResult()
        {
            bool expResult = true;
            bool actResult = data1.IsValidWpkgCrc();

            Assert.That(actResult, Is.EqualTo(expResult));
        }
    }
}
