﻿using LinGuiWP.Models;

namespace LinGuiWP.Tests
{
    public static class TestHelpers
    {
        public static FrameSenderEntity GetFrameSenderEntity()
        {
           var slaveFrameEntity = new FrameSenderEntity
            {
                Frame = new Models.Lin.LinFrame
                {
                    DataQty = 8,
                    PID = 0,
                    Data = new byte?[8] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 },
                    State = Models.Lin.FrameState.OK,
                    CRC = 0
                },
                EntityMode = EntityMode.SingleShot,
                IsEnabled = true,
                IsStarted = false,
                IsMaster = false,
                IsUseIncrement = true,
                IncrementValues = new byte?[8] { 0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7 },
                MsgDelay = 500,
                RepetitionsNumber = 10
            };

            return slaveFrameEntity;
        }
    }
}
