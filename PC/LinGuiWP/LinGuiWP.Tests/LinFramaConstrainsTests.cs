﻿using LinGuiWP.Models.Lin;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinGuiWP.Tests
{
    [TestFixture]
    public class LinFramaConstrainsTests
    {
        private LinFrame _validEnhancedFrame;
        private LinFrame _validClassicFrame;
        private LinFrame _invalidParityFrame;
        private LinFrame _invalidCrcFrame;
        private LinFrame _invalidPidFrame;

        [SetUp]
        public void SetUp()
        {
            _validClassicFrame = new LinFrame
            {
                PID = 0x3C,
                DataQty = 2,
                Data = new byte?[2] { 0xAA, 0xBB },
                CRC = 0x99
            };

            _validEnhancedFrame = new LinFrame
            {
                PID = 0x3C,
                DataQty = 2,
                Data = new byte?[2] { 0xAA, 0xBB },
                CRC = 0x5D
            };

            _invalidParityFrame = new LinFrame
            {
                PID = 0x40,
                DataQty = 2,
                Data = new byte?[2] { 0xAA, 0xBB },
                CRC = 0x99
            };

            _invalidCrcFrame = new LinFrame
            {
                PID = 0x3E,
                DataQty = 2,
                Data = new byte?[2] { 0xAA, 0xBB },
                CRC = 0xDD
            };

            _invalidPidFrame = new LinFrame
            {
                PID = 0x40,
                DataQty = 2,
                Data = new byte?[2] { 0xAA, 0xBB },
                CRC = 0x99
            };
        }

        [Test]
        public void ValidateParityTest_invalidParity()
        {
            // Act
            bool actResult = LinFrameConstraints.ValidateParity(_invalidParityFrame);

            // Assert
            Assert.That(actResult, Is.False);
            Assert.That(_invalidParityFrame.State, Is.EqualTo(FrameState.InvalidParity));
        }

        [Test]
        public void ValidateParityTest_validParity()
        {
            // Act
            bool actResult = LinFrameConstraints.ValidateParity(_validEnhancedFrame);

            // Assert
            Assert.That(actResult, Is.True);
            Assert.That(_validEnhancedFrame.State, Is.EqualTo(FrameState.OK));
        }

        [Test]
        public void ValidateCrc_validClasscCrc()
        {
            // Act
            bool actResult = LinFrameConstraints.ValidateCrc(_validClassicFrame);

            // Assert
            Assert.That(actResult, Is.True);
            Assert.That(_validClassicFrame.State, Is.EqualTo(FrameState.Classic));
        }

        [Test]
        public void ValidateCrc_validEnhancedCrc()
        {
            // Act
            bool actResult = LinFrameConstraints.ValidateCrc(_validEnhancedFrame);

            // Assert
            Assert.That(actResult, Is.True);
            Assert.That(_validEnhancedFrame.State, Is.EqualTo(FrameState.Enhanced));
        }

        [Test]
        public void ValidateCrc_invalidCrc()
        {
            // Act
            bool actResult = LinFrameConstraints.ValidateCrc(_invalidCrcFrame);

            // Assert
            Assert.That(actResult, Is.False);
            Assert.That(_invalidCrcFrame.State, Is.EqualTo(FrameState.InvalidCRC));
        }
    }
}
