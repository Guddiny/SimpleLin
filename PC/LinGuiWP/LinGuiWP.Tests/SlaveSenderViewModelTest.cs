﻿using LinGuiWP.BL.ConnectionManagment;
using LinGuiWP.Models;
using LinGuiWP.Services.Abstract;
using LinGuiWP.ViewModel;
using Moq;
using NUnit.Framework;
using NUnit.Framework.Internal;
using System.Linq;


namespace LinGuiWP.Tests
{
    [TestFixture]
    public class SlaveSenderViewModelTest
    {
        SlaveSenderViewModel _slaveSenderViemodel;
        FrameSenderEntity _slaveFrameEntity;

        [SetUp]
        public void SetUp()
        {
            // Arr
            var portMock = new Mock<ISerialPortManager>();
            var fileMock = new Mock<IFileService>();
            var messageMock = new Mock<IMessageService>();
            _slaveSenderViemodel = new SlaveSenderViewModel(portMock.Object, fileMock.Object, messageMock.Object);

            _slaveFrameEntity = TestHelpers.GetFrameSenderEntity();
        }

        [Test]
        public void StartOneTest_ValidStateForSenderEntiyAndNewItemInActionList()
        {
            // Arr
            for (int i = 0; i < 10; i++) // Start from 0
            {
                _slaveSenderViemodel.FrameEntities.Add(i, _slaveFrameEntity);
            }

            // Act
            _slaveSenderViemodel.StartOne(1);

            // Assert
            Assert.That(_slaveSenderViemodel.FrameEntities.First(f => f.Key == 1).Value.IsStarted, Is.True);
            Assert.That(_slaveSenderViemodel.FrameEntitiesActiv.First(f => f.Key == 1), Is.Not.Null);
            Assert.That(_slaveSenderViemodel.FrameEntitiesActiv.Count == 1);
        }

        [Test]
        public void StopOneTest_ValidStateForSenderEntiyAndNoneItemsInActionList()
        {
            // Arr
            for (int i = 0; i < 10; i++) // Start from 0
            {
                _slaveSenderViemodel.FrameEntities.Add(i, _slaveFrameEntity);
            }

            // Act
            _slaveSenderViemodel.StartOne(1);
            _slaveSenderViemodel.StopOne(1);

            // Assert
            Assert.That(_slaveSenderViemodel.FrameEntities.First(f => f.Key == 1).Value.IsStarted, Is.False);
            Assert.That(_slaveSenderViemodel.FrameEntitiesActiv.Count == 0);
        }

        [Test]
        public void RxFrameHandlerTest_ValidBehavirForSingleMessage_If_IsNotEveryTime()
        {
            // Arr
            for (int i = 0; i < 3; i++) // Start from 0
            {
                var slaveFrameEntity = TestHelpers.GetFrameSenderEntity();
                slaveFrameEntity.IsEveryTime = false;
                slaveFrameEntity.IsStarted = true;
                slaveFrameEntity.Frame.PID = (byte)(0x3C + i);

                _slaveSenderViemodel.FrameEntities.Add(i, slaveFrameEntity);
                _slaveSenderViemodel.FrameEntitiesActiv.Add(i, slaveFrameEntity);
            }

            // Act
            _slaveSenderViemodel.RxFrameHandler(0x3C);

            // Assert
            Assert.IsNull(_slaveSenderViemodel.FrameEntitiesActiv.Values.FirstOrDefault(f => f.Frame.PID == 0x3C));
        }

        [Test]
        public void RxFrameHandlerTest_ValidBehavirForSingleMessage_If_IsEveryTime()
        {
            // Arr
            for (int i = 0; i < 3; i++) // Start from 0
            {
                var slaveFrameEntity = TestHelpers.GetFrameSenderEntity();
                slaveFrameEntity.IsEveryTime = true;
                slaveFrameEntity.IsStarted = true;
                slaveFrameEntity.Frame.PID = (byte)(0x3C + i);

                _slaveSenderViemodel.FrameEntities.Add(i, slaveFrameEntity);
                _slaveSenderViemodel.FrameEntitiesActiv.Add(i, slaveFrameEntity);
            }

            // Act
            _slaveSenderViemodel.RxFrameHandler(0x3C);

            // Assert
            Assert.IsNotNull(_slaveSenderViemodel.FrameEntitiesActiv.Values.FirstOrDefault(f => f.Frame.PID == 0x3C));
        }

        [Test]
        public void GetPidsTest_ReturnAllPidsFromFrameEntityList()
        {
            // Arr
            var frame1 = new FrameSenderEntity
            {
                Frame = new Models.Lin.LinFrame
                {
                    DataQty = 8,
                    PID = 0,
                    Data = new byte?[8] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 },
                    State = Models.Lin.FrameState.OK,
                    CRC = 0
                }
            };

            var frame2 = new FrameSenderEntity
            {
                Frame = new Models.Lin.LinFrame
                {
                    DataQty = 8,
                    PID = 0,
                    Data = new byte?[8] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 },
                    State = Models.Lin.FrameState.OK,
                    CRC = 0
                }
            };
            var frame3 = new FrameSenderEntity
            {
                Frame = new Models.Lin.LinFrame
                {
                    DataQty = 8,
                    PID = 0,
                    Data = new byte?[8] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 },
                    State = Models.Lin.FrameState.OK,
                    CRC = 0
                }
            };

            frame1.Frame.PID = 0x11;
            frame2.Frame.PID = 0x12;
            frame3.Frame.PID = 0x13;

            _slaveSenderViemodel.FrameEntities.Add(1, frame1);
            _slaveSenderViemodel.FrameEntities.Add(2, frame2);
            _slaveSenderViemodel.FrameEntities.Add(3, frame3);

            byte[] expPidList = new byte[18] { 0x11, 0x12, 0x13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            // Act
            var actPidList = _slaveSenderViemodel.GetPids();

            // Assert
            Assert.That(actPidList, Is.EquivalentTo(expPidList));
        }

        [Test]
        public void GetPidsTest_ReturnAllPidsFromFrameEntityList_OnlyUniqValues()
        {
            // Arr
            var frame1 = new FrameSenderEntity
            {
                Frame = new Models.Lin.LinFrame
                {
                    DataQty = 8,
                    PID = 0,
                    Data = new byte?[8] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 },
                    State = Models.Lin.FrameState.OK,
                    CRC = 0
                }
            };

            var frame2 = new FrameSenderEntity
            {
                Frame = new Models.Lin.LinFrame
                {
                    DataQty = 8,
                    PID = 0,
                    Data = new byte?[8] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 },
                    State = Models.Lin.FrameState.OK,
                    CRC = 0
                }
            };
            var frame3 = new FrameSenderEntity
            {
                Frame = new Models.Lin.LinFrame
                {
                    DataQty = 8,
                    PID = 0,
                    Data = new byte?[8] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 },
                    State = Models.Lin.FrameState.OK,
                    CRC = 0
                }
            };

            frame1.Frame.PID = 0x11;
            frame2.Frame.PID = 0x12;
            frame3.Frame.PID = 0x11;

            _slaveSenderViemodel.FrameEntities.Add(1, frame1);
            _slaveSenderViemodel.FrameEntities.Add(2, frame2);
            _slaveSenderViemodel.FrameEntities.Add(3, frame3);

            byte[] expPidList = new byte[18] { 0x11, 0x12, 0x00, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            // Act
            var actPidList = _slaveSenderViemodel.GetPids();

            // Assert
            Assert.That(actPidList, Is.EquivalentTo(expPidList));
        }

        [Test]
        public void GetPidsTest_ReturnAllPidsFromFrameEntityList_AllZeroValuesInTail()
        {
            // Arr
            var frame1 = new FrameSenderEntity
            {
                Frame = new Models.Lin.LinFrame
                {
                    DataQty = 8,
                    PID = 0,
                    Data = new byte?[8] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 },
                    State = Models.Lin.FrameState.OK,
                    CRC = 0
                }
            };

            var frame2 = new FrameSenderEntity
            {
                Frame = new Models.Lin.LinFrame
                {
                    DataQty = 8,
                    PID = 0,
                    Data = new byte?[8] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 },
                    State = Models.Lin.FrameState.OK,
                    CRC = 0
                }
            };
            var frame3 = new FrameSenderEntity
            {
                Frame = new Models.Lin.LinFrame
                {
                    DataQty = 8,
                    PID = 0,
                    Data = new byte?[8] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 },
                    State = Models.Lin.FrameState.OK,
                    CRC = 0
                }
            };

            frame1.Frame.PID = 0x0;
            frame2.Frame.PID = 0x0;
            frame3.Frame.PID = 0x11;

            _slaveSenderViemodel.FrameEntities.Add(1, frame1);
            _slaveSenderViemodel.FrameEntities.Add(2, frame2);
            _slaveSenderViemodel.FrameEntities.Add(3, frame3);

            byte[] expPidList = new byte[18] { 0x11, 0x0, 0x0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            // Act
            var actPidList = _slaveSenderViemodel.GetPids();

            // Assert
            Assert.That(actPidList, Is.EquivalentTo(expPidList));
        }

    }
}
