﻿using LinGuiWP.BL.ConnectionManagment;
using LinGuiWP.Models;
using LinGuiWP.Services.Abstract;
using LinGuiWP.ViewModel;
using Moq;
using NUnit.Framework;
using System;

namespace LinGuiWP.Tests
{
    [TestFixture]
    public class MasterSenderViewModelTest
    {
        MasterSenderViewModel _masterSenderViemodel;
        FrameSenderEntity _slaveFrameEntity;

        [SetUp]
        public void SetUp()
        {
            // Arr
            var portMock = new Mock<ISerialPortManager>();
            var fileMock = new Mock<IFileService>();
            var messageMock = new Mock<IMessageService>();
            _masterSenderViemodel = new MasterSenderViewModel(portMock.Object, fileMock.Object, messageMock.Object);

            _slaveFrameEntity = new FrameSenderEntity
            {
                Frame = new Models.Lin.LinFrame
                {
                    DataQty = 8,
                    PID = 0,
                    Data = new byte?[8] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 },
                    State = Models.Lin.FrameState.OK,
                    CRC = 0
                },
                EntityMode = EntityMode.SingleShot,
                IsEnabled = true,
                IsStarted = false,
                IsMaster = false,
                IsUseIncrement = true,
                IncrementValues = new byte?[8] { 0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7 },
                MsgDelay = 500,
                RepetitionsNumber = 10
            };
        }


        [Test]
        public void RxFrameHandlerTest_ThrowValidException()
        {
            // Assert
            Assert.Throws<InvalidOperationException>(delegate { _masterSenderViemodel.RxFrameHandler(0); });
        }


    }
}
