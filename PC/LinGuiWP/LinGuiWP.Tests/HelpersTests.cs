﻿using System;
using LinGuiWP.Helpers;
using NUnit.Framework;

namespace LinGuiWP.Tests
{
    [TestFixture]
    public class HelpersTests
    {
        private byte[] data;

        [SetUp]
        public void SetUp()
        {
            // Array with data formated by WProtocol
            data = new byte[20] { 0x1A, 0x12, 0x13, 0x14, 0x15, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1A };
        }

        [Test]
        public void GetCrcCmdTest()
        {
            // Arr
            byte expCrc = 0x1A;

            // Act
            byte actCrc = data.GetCmdCrc();

            // Assert
            Assert.That(actCrc, Is.EqualTo(expCrc));
        }

        [Test]
        public void IsValidCrcTest()
        {
            // Arr
            bool expResult = true;

            // Act
            bool actResult = data.IsValidWpkgCrc();

            // Assert
            Assert.That(actResult, Is.EqualTo(expResult));
        }

        [Test]
        public void GetUint16Test()
        {
            // Arr
            UInt16 expU16 = 0x1213;

            // Act
            UInt16 actU16 = data.GetU16(1);


            // Assert
            Assert.That(actU16, Is.EqualTo(expU16));
        }

        [Test]
        public void PutU16Test()
        {
            // Arr
            UInt16 u16 = 0x3344;

            // Act
            data.PutU16(5, u16);

            // Assert
            Assert.That(data[5], Is.EqualTo(0x33));
            Assert.That(data[6], Is.EqualTo(0x44));
        }


    }
}