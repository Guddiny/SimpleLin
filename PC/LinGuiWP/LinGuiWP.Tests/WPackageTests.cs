﻿using LinGuiWP.Helpers;
using NUnit.Framework;

namespace LinGuiWP.Tests
{
    [TestFixture]
    public class WPackageTests
    {
        [Test]
        public void MakeWpkgTest_DataFormattedAsWpackage()
        {
            //Arr
            byte[] data = new byte[] {0x12, 0x13, 0x14, 0x15};

            //Act
            byte[] expData = new byte[20]{0x1A, 0x12, 0x13, 0x14, 0x15, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1A };
            byte[] actData = WPackage.MakeWpkg(0x1A, data);

            //Assert
            Assert.That(actData, Is.EqualTo(expData));
        }
    }
}