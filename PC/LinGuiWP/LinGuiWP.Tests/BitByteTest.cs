﻿using LinGuiWP.Models;
using NUnit.Framework;

namespace LinGuiWP.Tests
{
    [TestFixture]
    public class BitByteTest
    {
        private BitByte _bitByte;
        
        [SetUp]
        public void SetUp()
        {
            _bitByte = new BitByte();
        }

        [Test]
        public void SetBitByteTest_InpuByte_ValidBitValues()
        {
            _bitByte.SetBitByte(0xC0);
            
            Assert.That(_bitByte.Bit0, Is.EqualTo(0));
            Assert.That(_bitByte.Bit1, Is.EqualTo(0));
            Assert.That(_bitByte.Bit2, Is.EqualTo(0));
            Assert.That(_bitByte.Bit3, Is.EqualTo(0));
            Assert.That(_bitByte.Bit4, Is.EqualTo(0));
            Assert.That(_bitByte.Bit5, Is.EqualTo(0));
            Assert.That(_bitByte.Bit6, Is.EqualTo(1));
            Assert.That(_bitByte.Bit7, Is.EqualTo(1));
        }

    }
}