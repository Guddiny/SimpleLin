﻿using System.Linq;
using LinGuiWP.BL.ConnectionManagment;
using LinGuiWP.Services;
using LinGuiWP.Services.Abstract;
using LinGuiWP.ViewModel;
using NUnit.Framework;

namespace LinGuiWP.Tests
{
    [TestFixture]
    public class FrameSenderTests
    {
        private SlaveSenderViewModel _slaveSenderViewModel;
        private readonly ISerialPortManager _portManager  = new AsyncSerialPortManager();
        private readonly IFileService _fileService = new FileService();
        private readonly IMessageService _messageService = new MessageService();

        [SetUp]
        public void SetUp()
        {
            _slaveSenderViewModel = new SlaveSenderViewModel(_portManager, _fileService, _messageService);
        }

        [Test]
        public void AddOneTest_NewEneityInCollection()
        {
            // Act
            _slaveSenderViewModel.AddOneCmd.Execute(null);
            
            // Assert
            Assert.That(_slaveSenderViewModel.FrameEntities.Count, Is.EqualTo(1));
        }

        [Test]
        public void UniqueKeyInEeachEntityTets_QniquKey()
        {
            // Act
            _slaveSenderViewModel.AddOneCmd.Execute(null);
            _slaveSenderViewModel.AddOneCmd.Execute(null);
            _slaveSenderViewModel.AddOneCmd.Execute(null);
            
            
            // Assert
            Assert.That(_slaveSenderViewModel.FrameEntities[0].Key, Is.EqualTo(0));
            Assert.That(_slaveSenderViewModel.FrameEntities[1].Key, Is.EqualTo(1));
            Assert.That(_slaveSenderViewModel.FrameEntities[2].Key, Is.EqualTo(2));
        }

        [Test]
        public void DeleteOneTest_RemoveentityFromCollection()
        {
            // Arr
            _slaveSenderViewModel.AddOneCmd.Execute(null);
            _slaveSenderViewModel.AddOneCmd.Execute(null);
            _slaveSenderViewModel.AddOneCmd.Execute(null);
            
            // Act
            _slaveSenderViewModel.DeleteOneCmd.Execute(1);
            var expEntity = _slaveSenderViewModel.FrameEntities.FirstOrDefault(f => f.Key == 1);
            
            // Assert
            Assert.That(expEntity, Is.Null);
        }

        [Test]
        public void StartOneTest_EntityModeSingleShot_ChangeFrameStateToStarted()
        {
            // Arr
            _slaveSenderViewModel.AddOneCmd.Execute(null);
            _slaveSenderViewModel.AddOneCmd.Execute(null);
            _slaveSenderViewModel.AddOneCmd.Execute(null);
            
            // Act
            _slaveSenderViewModel.StartOneCmd.Execute(1);
            var expEntity = _slaveSenderViewModel.FrameEntitiesActiv.FirstOrDefault(f => f.Key == 1);
            
            //Assert
            Assert.That(expEntity.Value.IsStarted, Is.True);
        }
    }
}