﻿using System;
using LinGuiWP.Mappers;
using LinGuiWP.Models.Lin;
using NUnit.Framework;

namespace LinGuiWP.Tests
{
    [TestFixture]
    public class MappersTests
    {
        private byte[] _data;
        private byte[] _wpkg;

        [SetUp]
        public void SetUp()
        {
            // Array with data formated by WProtocol
            _data = new byte[20] { 0x1A, 0x12, 0x13, 0x14, 0x15, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1A };
            _wpkg = new byte[20] { 0x1D, 0x05, 0x55, 0xC1, 0x48, 0x02, 0xb5, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x73 };
        }

        [Test]
        public void BaudrateMapperTest()
        {
            // Arr
            UInt16 expBaud = 0x1213;

            // Act
            UInt16 actBaud = _data.MapToBaudrate();

            // Assert
            Assert.That(actBaud, Is.EqualTo(expBaud));
        }

        [Test]
        public void LinFrameMapperTest()
        {
            // Arr
            LinFrame expFrame = new LinFrame
            {
                DataQty = 0x02,
                PID = 0xC1,
                Data = new byte?[20]{ 0x48, 0x02, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null },
                CRC = 0xb5,
                State = FrameState.OK
            };

            // Act
            LinFrame actFrame = _wpkg.MapToLinFrame();

            // Assert
            Assert.That(actFrame.DataQty, Is.EqualTo(expFrame.DataQty));
            Assert.That(actFrame.PID, Is.EqualTo(expFrame.PID));
            Assert.That(actFrame.CRC, Is.EqualTo(expFrame.CRC));
            Assert.That(actFrame.Data, Is.EqualTo(expFrame.Data));
        }

        [Test]
        public void MapToObservableLinFrameTest()
        {
            // Arr
            LinFrame linFrame = new LinFrame
            {
                DataQty = 0x04,
                PID = 0x3C,
                Data = new byte?[8] { 0x14, 0x15, 0x11, 0xEE, null, null, null, null },
                CRC = 0xFF,
                State = FrameState.OK
            };

            ObservableLinFrame expObsFrame = new ObservableLinFrame
            {
                PID = 0x3C,
                CRC = 0xFF,
                DataQty = 0x04,
                Data = new ObservableItem[8] 
                {
                    new ObservableItem { Item = 0x14}, new ObservableItem {Item = 0x15}, new ObservableItem {Item = 0x11},
                    new ObservableItem { Item = 0xEE}, new ObservableItem { Item = null},new ObservableItem { Item = null},
                    new ObservableItem { Item = null},new ObservableItem { Item = null}
                }
            };

            // Act
            ObservableLinFrame actObsFrame = linFrame.MapToObservableLinFrame();

            // Assert
            Assert.That(actObsFrame.PID, Is.EqualTo(expObsFrame.PID));
            Assert.That(actObsFrame.CRC, Is.EqualTo(expObsFrame.CRC));
            Assert.That(actObsFrame.DataQty, Is.EqualTo(expObsFrame.DataQty));
            // TODO add assert for data field
            // Assert.That(actObsFrame.Data, Is.EqualTo(expObsFrame.Data));
        }

        [Test]
        public void StringMapperTest()
        {
            // Arr
            // Array with data formated by WProtocol
            byte[] wpkgWithString = new byte[20] {0x1a, 0x53, 0x69, 0x6D, 0x70, 0x6C, 0x65, 0x4C, 0x69, 0x6E, 0x0A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x75 };
            string expString = "SimpleLin";

            //Act
            string actString = wpkgWithString.MapToString();

            //Assert
            Assert.That(actString, Is.EqualTo(expString));
        }

        [Test]
        public void TmeoutMapperTest()
        {
            // Arr
            byte expTimeout = 0x12;

            // Act
            byte actTimeout = _data.MapToTimeout();

            // Assert
            Assert.That(actTimeout, Is.EqualTo(expTimeout));
        }

        [Test]
        public void VersionMapperTest()
        {
            // Arr 
            string expVersion = $"ver. {(int) 0x12}.{(int) 0x13}";

            // Act
            string actVersion = _data.MapToVersion();

            // Assert
            Assert.That(actVersion, Is.EqualTo(expVersion));
        }

        [Test]
        public void FrameToArrayMapperTest()
        {
            // Arr
            LinFrame linFrame = new LinFrame
            {
                DataQty = 0x04,
                PID = 0x3C,
                Data = new byte?[8] { 0x14, 0x15, 0x11, 0xEE, null, null, null, null },
                CRC = 0xFF,
                State = FrameState.OK,
                CrcType = CrcType.Enhanced
            };

            var expData = new byte[13] { 0x04, 0x3c, 0x14, 0x15, 0xFF, 0, 0, 0, 0, 0, 0, 0, 1};

            // Act
            var actData = linFrame.MapToByteArray();

            //Assert
            Assert.That(actData, Is.EqualTo(expData));
        }
    }
}