﻿using LinGuiWP.BL.ConnectionManagment;
using LinGuiWP.Models;
using LinGuiWP.Services.Abstract;
using LinGuiWP.ViewModel;
using Moq;
using NUnit.Framework;
using NUnit.Framework.Internal;
using System.Linq;

namespace LinGuiWP.Tests
{
    [TestFixture]
    public class SenderViewmodelBaseTests
    {
        SenderViemodelBase _senderViemodelBase;
        FrameSenderEntity frameEntity;

        [SetUp]
        public void SetUp()
        {
            // Arr
            var portMock = new Mock<ISerialPortManager>();
            var fileMock = new Mock<IFileService>();
            var messageMock = new Mock<IMessageService>();
            _senderViemodelBase = new SlaveSenderViewModel(portMock.Object, fileMock.Object, messageMock.Object);

            frameEntity = new FrameSenderEntity
            {
                Frame = new Models.Lin.LinFrame
                {
                    DataQty = 8,
                    PID = 0,
                    Data = new byte?[8] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 },
                    State = Models.Lin.FrameState.OK,
                    CRC = 0
                },
                EntityMode = EntityMode.SingleShot,
                IsEnabled = true,
                IsStarted = false,
                IsMaster = false,
                IsUseIncrement = true,
                IncrementValues = new byte?[8] { 0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7 },
                MsgDelay = 500,
                RepetitionsNumber = 10
            };
        }

        [Test]
        public void UpdateIncrement_CheckNewValueInFrameDataAfterIncrement()
        {
            frameEntity.IncrementValues[0] = 0;
            frameEntity.IncrementValues[1] = 1;
            frameEntity.IncrementValues[2] = 2;
            frameEntity.IncrementValues[3] = 3;
            frameEntity.IncrementValues[4] = 4;
            frameEntity.IncrementValues[5] = 5;
            frameEntity.IncrementValues[6] = 6;
            frameEntity.IncrementValues[7] = 7;

            // Act
            _senderViemodelBase.UpdateIncrement(frameEntity);

            //Assert
            Assert.That(frameEntity.Frame.Data[0], Is.EqualTo(0));
            Assert.That(frameEntity.Frame.Data[1], Is.EqualTo(1));
            Assert.That(frameEntity.Frame.Data[2], Is.EqualTo(2));
            Assert.That(frameEntity.Frame.Data[3], Is.EqualTo(3));
            Assert.That(frameEntity.Frame.Data[4], Is.EqualTo(4));
            Assert.That(frameEntity.Frame.Data[5], Is.EqualTo(5));
            //Assert.That(frameEntity.Frame.Data[6], Is.EqualTo(6));
            //Assert.That(frameEntity.Frame.Data[7], Is.EqualTo(7));
        }

        [Test]
        public void GetKeyTest_ReturnNextKeyForDictonary([Random(1, 8, 10)] int itemsInCollection)
        {
            // Arr
            for (int i = 0; i < itemsInCollection; i++) // Start from 0
            {
                _senderViemodelBase.FrameEntities.Add(i, frameEntity);
            }

            // Act
            var actKey = _senderViemodelBase.GetKey();

            // Assert
            Assert.That(actKey, Is.EqualTo(_senderViemodelBase.FrameEntities.Count)); // Beacouse starts from 0
        }

        [Test]
        public void DeleteOneCmdTest_CheckCountItemsInCollectionAfterDelete()
        {
            // Arr
            for (int i = 0; i < 10; i++) // Start from 0
            {
                _senderViemodelBase.FrameEntities.Add(i, frameEntity);
            }
            int expCout = 9;
            int expLastKey = 8;

            // Act 
            _senderViemodelBase.DeleteOne(9);
            int actCount = _senderViemodelBase.FrameEntities.Count;
            int actLastKey = _senderViemodelBase.FrameEntities.Last().Key;

            // Assert
            Assert.That(expCout == actCount);
            Assert.That(expLastKey == actLastKey);
        }

        public void StartOneTest_ValidStateForSenderEntiyuAndNewItemInActionList()
        {
            // Arr
            for (int i = 0; i < 10; i++) // Start from 0
            {
                _senderViemodelBase.FrameEntities.Add(i, frameEntity);
            }

            // Act
        }
    }
}
