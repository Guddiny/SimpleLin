﻿using System.Linq;
using LinGuiWP.Models;
using NUnit.Framework;
using LinGuiWP.Models.Lin;
using GalaSoft.MvvmLight.Threading;

namespace LinGuiWP.Tests
{
    [TestFixture]
    public class FrameBufferTests
    {
        private FramesBuffer _framesBuffer;
        private LinFrame _frame;
        private LinFrame _badFrame;

        [SetUp]
        public void SetUp()
        {
            _framesBuffer = new FramesBuffer();
            _frame = new LinFrame
            {
                DataQty = 0x04,
                PID = 0x3C,
                Data = new byte?[8] { 0x14, 0x15, 0x11, 0xEE, null, null, null, null },
                CRC = 0xFF,
                State = FrameState.OK
            };
            _badFrame = new LinFrame
            {
                DataQty = 0x04,
                PID = 0x3C,
                Data = new byte?[8] { 0x14, 0x15, 0x11, 0xEE, null, null, null, null },
                CRC = 0x1F, // Invalid CRC. True value is 0xFF
                State = FrameState.InvalidCRC
            };
            DispatcherHelper.Initialize();
        }

        [Test]
        public void InsertFrameTest_InsertAnyFrame()
        {
            // Act
            _framesBuffer.InsertFrame(_frame);

            // Assert
            Assert.That(_framesBuffer.ObservableLinFrames.Count, Is.EqualTo(1));
            Assert.That(_framesBuffer.ObservableLinFrames.Where(p => p.Key == 0x3C).Count, Is.EqualTo(1));

            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.DataQty, Is.EqualTo(0x04));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.CRC, Is.EqualTo(0xFF));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.PID, Is.EqualTo(0x3C));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.Data[0].Item, Is.EqualTo(_frame.Data[0]));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.Data[1].Item, Is.EqualTo(_frame.Data[1]));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.Data[2].Item, Is.EqualTo(_frame.Data[2]));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.Data[3].Item, Is.EqualTo(_frame.Data[3]));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.Data[4].Item, Is.EqualTo(_frame.Data[4]));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.Data[5].Item, Is.EqualTo(_frame.Data[5]));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.Data[6].Item, Is.EqualTo(_frame.Data[6]));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.Data[7].Item, Is.EqualTo(_frame.Data[7]));
        }

        [Test]
        public void InsertFrameTest_InsertFewSameFrames()
        {
            // Act
            _framesBuffer.InsertFrame(_frame);
            _framesBuffer.InsertFrame(_frame);

            // Assert
            Assert.That(_framesBuffer.ObservableLinFrames.Count, Is.EqualTo(1));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.Count, Is.EqualTo(2));

            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.DataQty, Is.EqualTo(0x04));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.CRC, Is.EqualTo(0xFF));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.PID, Is.EqualTo(0x3C));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.Data[0].Item, Is.EqualTo(_frame.Data[0]));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.Data[1].Item, Is.EqualTo(_frame.Data[1]));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.Data[2].Item, Is.EqualTo(_frame.Data[2]));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.Data[3].Item, Is.EqualTo(_frame.Data[3]));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.Data[4].Item, Is.EqualTo(_frame.Data[4]));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.Data[5].Item, Is.EqualTo(_frame.Data[5]));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.Data[6].Item, Is.EqualTo(_frame.Data[6]));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.Data[7].Item, Is.EqualTo(_frame.Data[7]));
        }

        [Test]
        public void InsertFrameTest_InsertFramesWhithOtherData()
        {
            // Act
            _framesBuffer.InsertFrame(_frame);
            _frame.Data[3] = 0x77;
            _framesBuffer.InsertFrame(_frame);

            // Assert
            Assert.That(_framesBuffer.ObservableLinFrames.Count, Is.EqualTo(1));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.Count, Is.EqualTo(2));

            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.DataQty, Is.EqualTo(0x04));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.CRC, Is.EqualTo(0xFF));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.PID, Is.EqualTo(0x3C));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.Data[0].Item, Is.EqualTo(_frame.Data[0]));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.Data[1].Item, Is.EqualTo(_frame.Data[1]));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.Data[2].Item, Is.EqualTo(_frame.Data[2]));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.Data[3].Item, Is.EqualTo(_frame.Data[3]));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.Data[4].Item, Is.EqualTo(_frame.Data[4]));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.Data[5].Item, Is.EqualTo(_frame.Data[5]));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.Data[6].Item, Is.EqualTo(_frame.Data[6]));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.Data[7].Item, Is.EqualTo(_frame.Data[7]));
        }

        [Test]
        public void InsertFrameTest_InsertFramesWhithOtherPid()
        {
            // Act
            _framesBuffer.InsertFrame(_frame);
            _frame.PID = 0xAA;
            _framesBuffer.InsertFrame(_frame);

            // Assert
            Assert.That(_framesBuffer.ObservableLinFrames.Count, Is.EqualTo(2));
            Assert.That(_framesBuffer.ObservableLinFrames.First(p => p.Key == 0x3C).Value.Count, Is.EqualTo(1));
            Assert.That(_framesBuffer.ObservableLinFrames.Where(p => p.Key == 0xAA).Count, Is.EqualTo(1));
        }

        [Test]
        public void InsertFrameTest_InsertFrameWithBadState()
        {
            // Act
            _framesBuffer.InsertFrame(_badFrame);

            // Assert
            Assert.That(_framesBuffer.LinFrames.Count, Is.EqualTo(1));
            Assert.That(_framesBuffer.ObservableLinFrames.Count, Is.EqualTo(0));
            Assert.That(_framesBuffer.Errors, Is.EqualTo(1));
            Assert.That(_framesBuffer.ErrorsPercent, Is.EqualTo(100));
        }
    }
}
