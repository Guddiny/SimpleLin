﻿using System;

namespace LinGuiWP.BL.ConnectionManagment
{
    public class DataStreamEventArgs : EventArgs
    {
        private byte[] _bytes;

        public DataStreamEventArgs(byte[] bytes)
        {
            _bytes = bytes;
        }

        public byte[] Response
        {
            get { return _bytes; }
        }
    }
}
