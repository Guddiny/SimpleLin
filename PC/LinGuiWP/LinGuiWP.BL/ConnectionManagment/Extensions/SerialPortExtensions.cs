﻿using System;
using System.IO.Ports;
using System.Threading.Tasks;

namespace LinGuiWP.BL.ConnectionManagment.Extensions
{
    public static class SerialPortExtensions
    {
        public static async Task ReadAsync(this SerialPort serialPort, byte[] buffer, int offset, int count)
        {
            int bytesToRead = count;
            var temp = new byte[count];

            while (bytesToRead > 0)
            {
                int readedBytesCount = await serialPort.BaseStream.ReadAsync(temp, 0, bytesToRead);
                Array.Copy(temp, 0, buffer, offset + count - bytesToRead, readedBytesCount);
                bytesToRead -= readedBytesCount;
            }
        }

        public static async Task<byte[]> ReadAsync(this SerialPort serialPort, int count)
        {
            var data = new byte[count];
            await serialPort.ReadAsync(data, 0, count);
            return data;
        }

        public static void ReadInside(this SerialPort serialPort, byte[] buffer, int offset, int count)
        {
            int bytesToRead = count;
            var temp = new byte[count];

            while (bytesToRead > 0)
            {
                int readedBytesCount = serialPort.Read(temp, 0, bytesToRead);
                Array.Copy(temp, 0, buffer, offset + count - bytesToRead, readedBytesCount);
                bytesToRead -= readedBytesCount;
            }
        }

        public static byte[] ReadInside(this SerialPort serialPort, int count)
        {
            var data = new byte[count];
            serialPort.ReadInside(data, 0, count);
            return data;
        }
    }
}