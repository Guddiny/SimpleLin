﻿using System;
using System.Collections.Generic;
using System.Management;
using System.Text.RegularExpressions;
using System.IO.Ports;
using System.Threading;
using Diagnostics = System.Diagnostics;
using System.Threading.Tasks;

namespace LinGuiWP.BL.ConnectionManagment
{
    public interface ISerialportManagerRR
    {

        void PortInit();
        bool PortState { get; }
        List<string> GetPortList();
        string SetPortName(string longPortName);
        string GetPortName();
        void OpnePort(string portName);
        void ClosePort();
        void WriteData(byte[] data);
        Task<int> ReadDataAsync(byte[] bytes, int offset, int count);
        event EventHandler<DataStreamEventArgs> OnReceiving;

    }

    public class SerialportManager : IDisposable
    {

        public bool PortState { get; set; }
        public event EventHandler<DataStreamEventArgs> OnReceiving;

        private const string DefaultPortName = "COM1";
        private const int DefaultPortSpeed = 56000;
        private double _packetsRate;
        private DateTime _lastReceive;
        private const int FreqCriticalLimit = 20;//The Critical Frequency of Communication to Avoid Any Lag
        private Thread _serThread;
        private SerialPort _sp1;
        private readonly Regex _regex = new Regex("COM");

        public byte[] IncomingBytes = new byte[20];

        public SerialportManager(string portName = DefaultPortName, int portSpeed = DefaultPortSpeed)
        {
            _sp1 = new SerialPort(portName, portSpeed, Parity.None, 8, StopBits.One);
            PortInit();
        }


        /// <summary>
        /// Initialisation events of port.
        /// </summary>
        public void PortInit()
        {
            _serThread = new Thread(SerialReceiving);
            _serThread.Priority = ThreadPriority.Normal;
            _serThread.Name = "SerialHandle" + _serThread.ManagedThreadId;
            _sp1.RtsEnable = true;
            _sp1.DtrEnable = true;
            _sp1.ReadTimeout = -1;
            _sp1.WriteTimeout = -1;
        }


        /// <summary>
        /// Return all aviable serial ports with PID info.
        /// </summary>
        /// <returns></returns>
        public List<string> GetPortList()
        {
            List<string> portList = new List<string>();
            string query = "SELECT * FROM Win32_PnPEntity WHERE ConfigManagerErrorCode = 0";

            var searcher = new ManagementObjectSearcher(query);
            foreach (ManagementObject service in searcher.Get())
            {
                try
                {
                    if (_regex.IsMatch(service["Name"].ToString()))
                    {
                        portList.Add(service["Name"].ToString());
                    }
                }
                catch (Exception e)
                {
                    Diagnostics.Debug.Write(e.Message);
                }
            }

            return portList;
        }


        /// <summary>
        /// Set port name. Incapsulate "PORT NAME" of line.
        /// </summary>
        /// <param name="longPortName"></param>
        /// <returns>Port nmae string with cendor info</returns>
        public string SetPortName(string longPortName)
        {
            string shortPortName = "";
            string pattern = @"COM\d+"; // Паттерн для выделения имени COM порта из строки
            try
            {
                MatchCollection matchs = Regex.Matches(longPortName, pattern); // Процесс выделения имени COM порта из строки
                foreach (Match match in matchs)
                    shortPortName = match.Value;
            }
            catch (Exception e)
            {
                Diagnostics.Debug.Write(e.Message);
            }

            return shortPortName;
        }


        /// <summary>
        /// Return curretn port name
        /// </summary>
        /// <returns></returns>
        public string GetPortName()
        {
            return _sp1.PortName;
        }


        /// <summary>
        /// Try open port
        /// </summary>
        /// <returns>Port state - boolean</returns>
        public void OpnePort(string portName)
        {
            if (!_sp1.IsOpen)
            {
                try
                {
                    _sp1.PortName = portName;
                    PortInit();
                    _sp1.Open();
                    PortState = true;
                    _serThread.Start(); /*Start The Communication Thread*/
                }
                catch (Exception ex)
                {
                    throw new SerialportManagerException(ex.Message, ex);
                }
            }
        }

        public bool IsNewFrame { get; set; }


        /// <summary>
        /// Try close port
        /// </summary>
        /// <returns>Port state - boolean</returns>
        public void ClosePort()
        {
            if (_sp1.IsOpen)
            {
                try
                {
                    _serThread.Abort();
                    while (_serThread.ThreadState != ThreadState.Aborted) { }
                    _sp1.Close();

                    PortState = false;
                }
                catch (Exception ex)
                {
                    throw new SerialportManagerException(ex.Message, ex);
                }
            }
        }


        /// <summary>
        /// Write data in port
        /// </summary>
        /// <param name="data"></param>
        public void WriteData(byte[] data)
        {
            if (_sp1.IsOpen)
            {
                _sp1.Write(data, 0, data.Length);
            }
        }


        /// <summary>
        /// Read data  and write them in an array
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public async Task<int> ReadDataAsync(byte[] bytes, int offset, int count)
        {
            int readBytes = 0;

            if (count > 0)
            {
                readBytes = await _sp1.BaseStream.ReadAsync(bytes, offset, count);
                //readBytes = _sp1.Read(bytes, offset, count);
            }

            return readBytes;
        }


        /// <summary>
        /// Receive incoming data
        /// </summary>
        private async void SerialReceiving()
        {
            if (_sp1.IsOpen)
            {
                while (true)
                {
                    int count = _sp1.BytesToRead;

                    /*Get Sleep Inteval*/
                    TimeSpan tmpInterval = (DateTime.Now - _lastReceive);

                    /*Form The Packet in The Buffer*/
                    byte[] buf = new byte[count];
                    int readBytes = await ReadDataAsync(buf, 0, count);

                    if (readBytes > 0)
                    {
                        OnSerialReceiving(buf);
                    }

#if DEBUG
                    #region Frequency Control
                    _packetsRate = ((_packetsRate + readBytes) / 2);

                    _lastReceive = DateTime.Now;

                    if ((double)(readBytes + _sp1.BytesToRead) / 2 <= _packetsRate)
                    {
                        if (tmpInterval.Milliseconds > 0)
                            Thread.Sleep(tmpInterval.Milliseconds > FreqCriticalLimit ? FreqCriticalLimit : tmpInterval.Milliseconds);

                        /*Testing Threading Model*/
                        Diagnostics.Debug.Write(tmpInterval.Milliseconds.ToString());
                        Diagnostics.Debug.Write(" - ");
                        Diagnostics.Debug.Write(readBytes.ToString());
                        Diagnostics.Debug.Write("\r\n");
                    }
                    #endregion
#endif
                }
            }

        }


        /// <summary>
        /// Data recived handler
        /// </summary>
        /// <param name="res"></param>
        private void OnSerialReceiving(byte[] res)
        {
            OnReceiving?.Invoke(this, new DataStreamEventArgs(res));
        }


        public void Dispose()
        {
            // ClosePort();

            if (_sp1 != null)
            {
                _sp1.Dispose();
                _sp1 = null;
            }
        }
    }
}
