﻿using System;
using System.Collections.Generic;

namespace LinGuiWP.BL.ConnectionManagment
{
    public interface ISerialPortManager
    {
        byte[] Bytes { get; }
        string CurrentPortName { get; }
        string CurrentVendorPortName { get; }
        bool PortState { get; }

        event EventHandler<byte[]> OnReceiving;

        void ClosePort();
        string ExtractPortName(string portName);
        List<string> GetPortList();
        void OpenPort();
        void OpenPort(string portName);
        void ReadDataAsync();
        void WriteData(byte[] buff);
    }
}