using System;
using System.Collections.Generic;
using System.IO.Ports;
using Diagnostics = System.Diagnostics;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using LinGuiWP.BL.ConnectionManagment.Extensions;
using System.Management;

namespace LinGuiWP.BL.ConnectionManagment
{
    public class AsyncSerialPortManager : ISerialPortManager
    {
        public const int DefaultBaudRate = 56000;
        public const string DefaultPortName = "COM1";
        public string CurrentPortName { get; private set; }
        public string CurrentVendorPortName { get; private set; }

        public bool PortState
        {
            get
            {
                return _sp != null && _sp.IsOpen;
            }
        }
        private readonly SerialPort _sp;
        private Task _serThread;
        private CancellationTokenSource _ts;
        private CancellationToken _ct;
        public byte[] Bytes { get; private set; }
        public event EventHandler<byte[]> OnReceiving;

        public AsyncSerialPortManager()
        {
            CurrentPortName = ExtractPortName(DefaultPortName);
            _sp = new SerialPort(CurrentPortName, DefaultBaudRate);
            PortInit();
        }

        private void PortInit()
        {
            _sp.Parity = Parity.None;
            _sp.StopBits = StopBits.One;
            _sp.DataBits = 8;
            _sp.DtrEnable = true;
            _sp.RtsEnable = true;
        }

        private void ThreadInit()
        {
             _ts = new CancellationTokenSource();
             _ct = _ts.Token;
            _serThread = new Task(ReadDataAsync, _ct);
        }

        /// <summary>
        /// Return all aviable serial ports with PID info.
        /// </summary>
        /// <returns></returns>
        public List<string> GetPortList()
        {
            var portList = new List<string>();
            var regex = new Regex("COM");
            const string query = "SELECT * FROM Win32_PnPEntity WHERE ConfigManagerErrorCode = 0";

            var searcher = new ManagementObjectSearcher(query);
            foreach (ManagementObject service in searcher.Get())
            {
                try
                {
                    if (service["Name"] != null && regex.IsMatch(service["Name"].ToString()))
                    {
                        portList.Add(service["Name"].ToString());
                    }
                }
                catch (Exception e)
                {
                    Diagnostics.Debug.Write(e.Message);
                }
            }

            return portList;
        }


        public string ExtractPortName(string portName)
        {
            CurrentVendorPortName = portName;
            var shortPortName = string.Empty;
            const string pattern = @"COM\d+"; // Паттерн для выделения имени COM порта из строки
            try
            {
                MatchCollection matchs = Regex.Matches(portName, pattern); // Процесс выделения имени COM порта из строки
                foreach (Match match in matchs)
                    shortPortName = match.Value;
            }
            catch (Exception e)
            {
                Diagnostics.Debug.Write(e.Message);
            }

            return shortPortName;
        }

        /// <summary>
        /// Open serial port
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        public void OpenPort()
        {
            try
            {
                _sp.Open();
                ThreadInit();
                _serThread.Start();
            }
            catch (Exception e)
            {
                throw new SerialportManagerException(e.Message, e);
            }
        }

        /// <summary>
        /// Open serial port
        /// </summary>
        /// <param name="portName"></param>
        public void OpenPort(string portName)
        {
            CurrentPortName = ExtractPortName(portName);
            try
            {
                _sp.PortName = CurrentPortName;
            }
            catch (Exception e)
            {
                throw new SerialportManagerException(e.Message, e);
            }

            OpenPort();
        }

        public void ReadDataAsync()
        {
            while (true)
            {
                Bytes = _sp.ReadInside(20);
                OnReceiving?.Invoke(this, Bytes);

                if (_ct.IsCancellationRequested)
                {
                    break;
                }
            }
        }

        public void WriteData(byte[] buff)
        {
            if (_sp.IsOpen)
            {
                try
                {
                    _sp.Write(buff, 0, buff.Length);
                }
                catch (Exception e)
                {
                    Diagnostics.Debug.Write(e.Message);
                }  
            }
        }

        public void ClosePort()
        {
            if (PortState)
            {
                try
                {
                    _ts.Cancel();
                    _sp.Close();
                    _sp.Dispose();
                }
                catch (Exception ex)
                {
                    throw new SerialportManagerException(ex.Message, ex);
                }
            }
        }

        ~AsyncSerialPortManager()
        {
            ClosePort();
        }
    }
}