﻿namespace LinGuiWP.BL.WProtocol
{
    public enum Codes : byte
    {
        CError = 0x01,
        CEcho = 0x02
    }
}