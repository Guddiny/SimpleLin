﻿namespace LinGuiWP.BL.WProtocol
{
    public enum Commands : byte
    {
        GetDeviceName = 0x1a,
        GetDeviceVersion = 0x2a,
        GetLinBaudrate = 0x1b,
        SetLinBaudrate = 0x2b,
        GetMasterTimeout = 0x1c,
        SetMasterTimeout = 0x2c,
        IncomingLinFrame = 0x1d,
        SetMasterFrame = 0x2d,
        SetSlaveFrame = 0x3d,
        SetMasterHeader = 0x4d,
        SetPidFilter = 0x3e,
        ResetPidFilter = 0x4e,
        Reboot = 0x4f
    }
}