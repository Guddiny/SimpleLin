﻿namespace LinGuiWP.BL.WProtocol
{
    public enum Definitions : int
    {
        TxRxArratySize = 20,
        CmdPtr = 0,
        DataPtr = 1,
        CrcPtr = 19
    }
}