#tool "nuget:?package=NUnit.ConsoleRunner"

var target = Argument("target", "Default");

Task("Default")
  .IsDependentOn("Run");

Task("Nuget")
  .Does(() =>{
      NuGetRestore("LinGuiWP.sln");
});

Task("Build")
  .IsDependentOn("Nuget")
  .Does(() =>{
  MSBuild("LinGuiWP.sln", new MSBuildSettings {
    Configuration = "Release"
    });
});

Task("Tests")
  .IsDependentOn("Build")
  .Does(()=>{
    NUnit3("./LinGuiWP.Tests/bin/Release/LinGuiWP.Tests.dll");
  });

Task("Run")
  .IsDependentOn("Tests")
  .Does(()=>{
    StartProcess("./LinGuiWP/bin/Release/LinGuiWP.exe");
    Information("Hello World!");
  });





RunTarget(target);