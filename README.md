### <dt>Simple LIN</dt> 
Based on cheap STM32F103 mini-devboard LIN bus analyser.
- - -
![lin_logo](https://upload.wikimedia.org/wikipedia/commons/7/79/Logo_lin_bus.svg)
- - -

### Project provide:
  - Microcontroller firmware ([info](https://gitlab.com/Guddiny/SimpleLin/tree/master/MC/Board/SimpleLin))
  - Pcb shield and seprate pcb  ([info](https://gitlab.com/Guddiny/SimpleLin/tree/master/MC/Pcb))
  - Soft for PC ([info](https://gitlab.com/Guddiny/SimpleLin/tree/master/PC/LinGuiWP))
  - User manual ([doc](PC/Tutorial/Simple LIN sniffer user manual.pdf))

### Directory structure
 - PC -- software for PC (include GUI program, test, nad emulator(very basic) )
 - MC -- firmware for microcomtroller, PCB, schecmatic

### Useful links:
* [Protocol](https://docs.google.com/document/d/1k_0_KEaK4MgFOL2ShaHvgRaSgq0yzDBagRkQHupUjd0/edit?usp=sharing) - Describes communication protocol with hardware and PC
