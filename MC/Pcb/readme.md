### Created by [DipTrace](http://diptrace.com/) cad system. 
- - -

### PCB- Shield
<img src="pcb_shield/PCB.jpg" alt="Mortality Bubble Chart" width="500" />

This board were created as shield for popular STM32F103C8T6 devboard <b>"Blue Pill"</b>
- - -

### PCB - V1
##### Top
![pcb_v1_top](lin_pcb_v1/lin_pcb_v1_top.png)

##### Bottom
![pcb_v1_top](lin_pcb_v1/lin_pcb_v1_bottom.png)



