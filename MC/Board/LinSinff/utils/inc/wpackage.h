#ifndef WPACKAGE_H
#define WPACKAGE_H

#include "main.h"
#include "stdbool.h"


/*===========================================================================*/
/* Difinitions                                                               */
/*===========================================================================*/


/*===========================================================================*/
/* Function prototypes.                                                      */
/*===========================================================================*/


uint8_t get_crc(uint8_t *buff);
bool check_crc(uint8_t *buff);
void send_wpkg(uint8_t cmd, uint8_t *buff, uint8_t data_len);

#endif
