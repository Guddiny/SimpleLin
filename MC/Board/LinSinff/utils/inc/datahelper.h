#ifndef DATAHELPER_H
#define DATAHELPER_H

#include "main.h"
#include <string.h>
/*===========================================================================*/
/* Difinitions                                                               */
/*===========================================================================*/

/*===========================================================================*/
/* Function prototypes.                                                      */
/*===========================================================================*/


uint16_t get_u16(uint8_t* buff, uint8_t start_ptr);
void put_u16(uint8_t* buff, uint8_t start_ptr, uint16_t value);
void clear_buff(uint8_t *array, uint8_t arrSize);
#endif
