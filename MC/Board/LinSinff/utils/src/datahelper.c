#include "datahelper.h"


/*===========================================================================*/
/* Private function implementation                                           */
/*===========================================================================*/

/*===========================================================================*/
/* Public function implementation                                            */
/*===========================================================================*/


//
// Get uint16_t value from array of bytes (chars)
//
uint16_t get_u16(uint8_t* buff, uint8_t start_ptr)
{
    uint16_t u16 = 0x0;
    u16 = buff[start_ptr] << 8 | buff[start_ptr + 1];

    return u16;
}

//
// Put uint16_t value to array of bytes
//
void put_u16(uint8_t* buff, uint8_t start_ptr, uint16_t value)
{
    buff[start_ptr] = (value & 0xFF00) >> 8;
    buff[start_ptr + 1] = value & 0xFF;
}


//
// Clear buffer
//
void clear_buff(uint8_t *array, uint8_t arrSize)
{
    memset(array, 0, arrSize);
}
