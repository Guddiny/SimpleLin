/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"
#include "stm32f1xx_ll_rcc.h"
#include "stm32f1xx_ll_bus.h"
#include "stm32f1xx_ll_system.h"
#include "stm32f1xx_ll_exti.h"
#include "stm32f1xx_ll_cortex.h"
#include "stm32f1xx_ll_utils.h"
#include "stm32f1xx_ll_pwr.h"
#include "stm32f1xx_ll_dma.h"
#include "stm32f1xx_ll_usart.h"
#include "stm32f1xx.h"
#include "stm32f1xx_ll_gpio.h"

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);


/* Private defines -----------------------------------------------------------*/
#define BUS_LED_Pin LL_GPIO_PIN_13
#define BUS_LED_GPIO_Port GPIOC
#define STATUS_LED_Pin LL_GPIO_PIN_14
#define STATUS_LED_GPIO_Port GPIOC
#define BOOT_BTN_Pin LL_GPIO_PIN_2
#define BOOT_BTN_GPIO_Port GPIOB
// SSR - solid state relay
#define SSR_CONTROL_Pin LL_GPIO_PIN_5
#define SSR_CONTROL_GPIO_Port GPIOB


#ifdef __cplusplus
}
#endif

#endif
