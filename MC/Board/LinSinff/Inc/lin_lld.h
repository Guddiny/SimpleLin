#ifndef LIN_LLD_H
#define LIN_LLD_H

#include "stdint.h"
#include "main.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "stdbool.h"

/* Definitions                                                               */

#define  LIN_SYNC_DATA               0x55  // SyncField (do not edit)
#define  LIN_MAX_DATA                   8  // Max 8
#define  LIN_TX_ARRAY_LENGHT           12  // Array for tx/rx LIN frame to/from PC

#define  LIN_POWERON_DELAY             10  // Pause after PowerOn     (ca. 10ms)
#define  LIN_AKTIV_DELAY                1  // Pause for Transceiver (ca. 50us)

#define  LIN_INTER_FRAME_DELAY         10  // Pause (Frame->Frame)   (ca. 10ms)
#define  LIN_FRAME_RESPONSE_DELAY       2  // Pause (Header->Data)   (ca.  2ms)
#define  LIN_BREAKFIELD_DELAY           4  // Pause (Break-field)     (ca.  4ms)
#define  LIN_DATA_BYTE_DELAY            1  // Pause (Data->Data)     (ca.  1ms)
#define  LIN_RX_TIMEOUT_CNT             5  // Timeout when receive (ca.  5ms)

#define HAL_LIN_UART_TIMEOUT          100  // Timeout

#define ID_MASK                      0x0f  // Get real ID from header of LIN message

#define SYNC_PTR                        1  // Sync data byte position in array

#define RESPONSE_ARR_SIZE              18  // Count of filtered ID filter array

//#defien DATALENGHT_MASK              (0x03 <<4))>>4 // get data length from header of LIN message



typedef struct LINDriver LINDriver;


/**
 * @brief   LIN bus error type
 */
typedef enum {
  LIN_OK  = 0,   // no error
  LIN_WRONG_LEN, // wrong error
  LIN_RX_EMPTY,  //no Frame received
  LIN_WRONG_CRC  // Wrong CRC
}linerr_t;

/**
 * @brief   LIN bus protocol version
 */
typedef enum
{
  V13 = 0,
  V20 = 1
} linversion_t;

/**
 * @brief   LIN CRC type
 */
typedef enum
{
  CLASSIC = 0,
  ENHANCED = 1
} lincrc_t;



/**
 * @brief   Represent RX state.
 */
typedef enum
{
  WAIT_BREAK = 0,
  WAIT_SYNC = 1,
  WAIT_ID,
  WAIT_DATA
} rxstate_t;

/**
 * @brief   Represent TX state.
 */
typedef enum
{
  SEND_BREAK = 0
} txstate_t;

/**
 * @brief   LIN frame.
 */
typedef struct
{
  uint8_t              ID:8;        /**< @brief Real ID                       */

  uint8_t               DCL;        /**< @brief Frame length                  */

  uint8_t          data8[8];        /**< @brief Frame data.                   */

  lincrc_t          crctype;        /**< @brief LIN CRC type data.            */

  linversion_t   busversion;        /**< @brief LIN bus (protocol) version.   */
} LINFrame;


/**
 * @brief   LIN driver.
 */
struct LINDriver
{
  rxstate_t         rxstate;
  txstate_t         txstate;
  struct{
    LINFrame*   resparr;
    uint8_t     lastitemptr;
  }resp;
  QueueHandle_t     rxqueue;
  QueueHandle_t     txqueue;
  USART_TypeDef*    usart;
};

/**
 * @brief   Driver for LIN on USART1
 */
extern LINDriver LIND1;

void HandleIrq(LINDriver* linp);
void bus_control_enable(void);
void bus_control_disable(void);
void chip_enable(void);
void chip_disable(void);
linerr_t lin_send_master_data(LINFrame* frame);
void lin_send_master_header(uint8_t id);
linerr_t lin_send_slave_response(LINFrame* frame);
void set_PID_filter(LINFrame* frame);
void reset_PID_filter(void);
int8_t contains(LINFrame* frame);

#endif
