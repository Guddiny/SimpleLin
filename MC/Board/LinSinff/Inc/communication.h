#ifndef COMMUNICATION_H_INCLUDED
#define COMMUNICATION_H_INCLUDED

#include "stdint.h"
#include "stdlib.h"
#include "string.h"


/*===========================================================================*/
/* Difinitions                                                               */
/*===========================================================================*/

//
// Protocol definitions.
//

/*
 See: https://docs.google.com/document/d/1k_0_KEaK4MgFOL2ShaHvgRaSgq0yzDBagRkQHupUjd0/edit#heading=h.fq71qivvo48c
 Data position in transmit-receive buffer
 */
#define WPKG_CMD_PTR        0
#define WPKG_DATA_PTR       1
#define WPKG_CRC_PTR        19

#define W_PKG_SIZE  20 // Full size of WPackage

//
// Lin frame data position
//
#define LIN_DATALEN_PTR     0
#define LIN_PID_PTR         2
#define REBOOT              0x4F

//
//Commands
//

/* System commands */
#define C_Err   0x01
#define C_Echo  0x02

/* Operation commands */
#define GET_DEVICE_NAME         0x1A
#define GET_DEVICE_VERSION      0x2A
#define GET_LIN_BAUDRATE        0x1B
#define SET_LIN_BAUDRATE        0x2B
#define GET_LIN_TIMEOUT         0x1C
#define SET_LIN_TIMEOUT         0x2C
#define GET_LIN_FRAME           0x1D
#define SET_LIN_MASTER_FRAME    0x2D
#define SET_LIN_SLAVE_FRAME     0x3D
#define SET_MASTER_HEADER       0x4D
#define SET_PID_FILTER          0x3E
#define RESET_PID_FILTER        0x4E


#endif /* COMMUNICATION_H_INCLUDED */
