#ifndef __usart_H
#define __usart_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

// Clock of APB2 bus: [MHz]
#define FCK 72000000


void MX_USART1_UART_Init(void);
void usart_set_baudrate(USART_TypeDef* usart, uint32_t baudrate);


#ifdef __cplusplus
}
#endif
#endif /*__ usart_H */
