#include "lin_lld.h"
#include <string.h>

LINFrame rxFrame;
BaseType_t xHigherPriorityTaskWoken = pdFALSE;

/**
 * @brief   Bus control enable (close SSR)
 */
void bus_control_enable(void)
{
  LL_GPIO_SetOutputPin(SSR_CONTROL_GPIO_Port, SSR_CONTROL_Pin);
}

/**
 * @brief   Bus control disable (open SSR)
 */
void bus_control_disable(void)
{
  LL_GPIO_ResetOutputPin(SSR_CONTROL_GPIO_Port, SSR_CONTROL_Pin);
}

/**
 * @brief   TJA1021 enable
 */
void chip_enable(void)
{
  // TODO not implemented
}

/**
 * @brief   TJA1021 disable
 */
void chip_disable(void)
{
 // TODO not implemented
}

/**
 * @brief  Check contains
 */
int8_t contains(LINFrame* frame)
{
  int8_t result = -1;

  for(uint8_t i = 0; i < RESPONSE_ARR_SIZE; i++)
  {
    if (LIND1.resp.resparr[i].ID == frame->ID)
    {
      result = i;
      break;
    }
  }

  return result;
}

/**
 * @brief   Calculate Checksum for LIN_Frame
 */
uint8_t lin_makeCassicChecksum(LINFrame *frame)
{
    uint8_t ret_wert = 0, n;
    uint16_t dummy;

    // Compute checksum
    dummy = 0;
    for (n = 0; n < frame->DCL; n++)
    {
        dummy += frame->data8[n];
        if (dummy > 0xFF)
        {
            dummy -= 0xFF;
        }
    }
    ret_wert = (uint8_t)(dummy);
    ret_wert ^= 0xFF;

    return (ret_wert);
}


/**
 * @brief   Calculate Checksum for LIN_Frame
 */
uint8_t lin_makeEnhancedChecksum(LINFrame *frame)
{
    uint8_t ret_wert = 0, n;
    uint16_t dummy;

    // Compute checksum
    dummy = 0;

    // Include  PID in to CRC
    dummy += frame->ID;

    for (n = 0; n < frame->DCL; n++)
    {
        dummy += frame->data8[n];
        if (dummy > 0xFF)
        {
            dummy -= 0xFF;
        }
    }
    ret_wert = (uint8_t)(dummy);
    ret_wert ^= 0xFF;

    return (ret_wert);
}


/**
 * @brief   Send data frame by LIN bus (USARTx)
 */
linerr_t lin_send_master_data(LINFrame* frame)
{
  uint8_t checksum, n;

  // Check the length
  if ((frame->DCL < 1) || (frame->DCL > LIN_MAX_DATA))
  {
    return (LIN_WRONG_LEN);
  }

  // Calculate checksum
  if (frame->crctype == CLASSIC)
  {
    checksum = lin_makeCassicChecksum(frame);
  }
  else
  {
    checksum = lin_makeEnhancedChecksum(frame);
    // TODO refactoring for ENHACED CRC calculation
  }

  // Send break
  LL_USART_RequestBreakSending(USART1);
  while (LL_USART_IsActiveFlag_TC(USART1) == RESET);

  // Small pause
  //delay_ms(LIN_BREAKFIELD_DELAY);

  // Sync-field
  USART1->DR = LIN_SYNC_DATA;
  while (LL_USART_IsActiveFlag_TC(USART1) == RESET);

  // Small pause
  vTaskDelay(LIN_DATA_BYTE_DELAY);

  // ID-Field
  USART1->DR = frame->ID;
  while (LL_USART_IsActiveFlag_TC(USART1) == RESET);

  // Small pause
  vTaskDelay(LIN_FRAME_RESPONSE_DELAY);

  // Data-field [1...n]
  for (n = 0; n < frame->DCL; n++)
  {
    USART1->DR = frame->data8[n];
    while (LL_USART_IsActiveFlag_TC(USART1) == RESET);

    // Small Pause
    //delay_ms(LIN_DATA_BYTE_DELAY);
  }

  //delay_ms(LIN_DATA_BYTE_DELAY);

  // CRC-field
  USART1->DR = checksum;
  while (LL_USART_IsActiveFlag_TC(USART1) == RESET);

  // Small pause
  // So that next Frame is not sent too fast
  //delay_ms(LIN_INTER_FRAME_DELAY);

  return (LIN_OK);
}

/**
 * @brief   Send only response to LIN network
 */
linerr_t lin_send_slave_response(LINFrame* frame)
{
  uint8_t checksum, n;

  // Check the length
  if ((frame->DCL < 1) || (frame->DCL > LIN_MAX_DATA))
  {
    return (LIN_WRONG_LEN);
  }


  // Calculate checksum
  if (frame->crctype == CLASSIC)
  {
    checksum = lin_makeCassicChecksum(frame);
  }
  else
  {
    checksum = lin_makeEnhancedChecksum(frame);
    // TODO refactoring for ENHACED CRC calculation
  }

  // Data-Field [1...n]
  for (n = 0; n < frame->DCL; n++)
  {
    USART1->DR = frame->data8[n];
    while (LL_USART_IsActiveFlag_TC(USART1) == RESET);

    // Small Pause
    //delay_ms(LIN_DATA_BYTE_DELAY);
  }

  // CRC-Field
  USART1->DR = checksum;
  while (LL_USART_IsActiveFlag_TC(USART1) == RESET);

  // Small pause
  // So that next Frame is not sent too fast
  //delay_ms(LIN_INTER_FRAME_DELAY);

  return (LIN_OK);
}

/**
 * @brief   Send only Master header to LIN network and wait response from slave
 */
void lin_send_master_header(uint8_t id)
{
  // Send break
  LL_USART_RequestBreakSending(USART1);
  while (LL_USART_IsActiveFlag_TC(USART1) == RESET);

  // Small pause
  //delay_ms(LIN_BREAKFIELD_DELAY);

  // Sync-field
  USART1->DR = LIN_SYNC_DATA;
  while (LL_USART_IsActiveFlag_TC(USART1) == RESET);

  // Small pause
  vTaskDelay(LIN_DATA_BYTE_DELAY);

  // ID-Field
  USART1->DR = id;
  while (LL_USART_IsActiveFlag_TC(USART1) == RESET);
}


/**
 * @brief  Set PID filters
 */
void set_PID_filter(LINFrame* frame)
{
  LIND1.resp.resparr[LIND1.resp.lastitemptr] = *frame;
  LIND1.resp.lastitemptr++;
}

/**
 * @brief  Reset all PID filters
 */
void reset_PID_filter(void)
{
  //memset(LIND1.resp.resparr, 0, sizeof(*LIND1.resp.resparr));

  // TODO need refactoring for bather performance
  for(uint8_t i = 0; i < RESPONSE_ARR_SIZE; i++)
  {
    LIND1.resp.resparr[i].ID = 0;
    LIND1.resp.resparr[i].DCL = 0;
    LIND1.resp.resparr[i].data8[0] = 0;
    LIND1.resp.resparr[i].data8[1] = 0;
    LIND1.resp.resparr[i].data8[2] = 0;
    LIND1.resp.resparr[i].data8[3] = 0;
    LIND1.resp.resparr[i].data8[4] = 0;
    LIND1.resp.resparr[i].data8[5] = 0;
    LIND1.resp.resparr[i].data8[6] = 0;
    LIND1.resp.resparr[i].data8[7] = 0;
  }
  LIND1.resp.lastitemptr = 0;
}


/**
 * @brief   LIN IRQ handler
 */
void HandleIrq(LINDriver* linp)
{
  if (LL_USART_IsActiveFlag_LBD(linp->usart))
  {
    LL_USART_ClearFlag_LBD(linp->usart);

    if(linp->rxstate == WAIT_DATA)
    {
      xQueueSendFromISR(linp->rxqueue, &rxFrame, &xHigherPriorityTaskWoken);
      linp->rxstate = WAIT_SYNC;
      rxFrame.DCL = 0;
    }
    else
    {
      linp->rxstate = WAIT_SYNC;
      rxFrame.DCL = 0;
    }
  }

  if (LL_USART_IsActiveFlag_RXNE(linp->usart))
  {
    uint8_t tmp = linp->usart->DR;
    LL_USART_ClearFlag_RXNE(linp->usart); // ??

    if(linp->rxstate == WAIT_SYNC && tmp == 0x55)
    {
      linp->rxstate = WAIT_ID;
    }
    else if(linp->rxstate == WAIT_ID)
    {
      rxFrame.ID = tmp;
      linp->rxstate = WAIT_DATA;
    }
    else if (linp->rxstate == WAIT_DATA)
    {
      rxFrame.data8[rxFrame.DCL] = tmp;
      rxFrame.DCL++;
    }
  }

  if (LL_USART_IsActiveFlag_IDLE(linp->usart))
  {
    LL_USART_ClearFlag_IDLE(linp->usart); // ??

    if (linp->rxstate == WAIT_DATA)
    {
      xQueueSendFromISR(linp->rxqueue, &rxFrame, &xHigherPriorityTaskWoken);
     if(rxFrame.DCL == 0)
     {
       linp->rxstate = WAIT_DATA;
     }
     else
     {
       linp->rxstate = WAIT_BREAK;
     }
    }
  }
}
