#include "usart.h"

LL_USART_InitTypeDef USART_InitStruct = {0};

/* USART1 init function */

void MX_USART1_UART_Init(void)
{
  LL_GPIO_InitTypeDef GPIO_InitStruct = {0};
  /* Peripheral clock enable */
  LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_USART1);

  LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOB);
  /**USART1 GPIO Configuration
  PB6   ------> USART1_TX
  PB7   ------> USART1_RX
  */
  GPIO_InitStruct.Pin = LL_GPIO_PIN_6;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  GPIO_InitStruct.Pin = LL_GPIO_PIN_7;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_FLOATING;
  LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  LL_GPIO_AF_EnableRemap_USART1();

  /* USART1 interrupt Init */


  NVIC_SetPriority(USART1_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),5, 0));

  LL_USART_EnableIT_RXNE(USART1);
  LL_USART_EnableIT_IDLE(USART1);
  LL_USART_EnableIT_LBD(USART1);

  NVIC_EnableIRQ(USART1_IRQn);

  USART_InitStruct.BaudRate = 9600;
  USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
  USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
  USART_InitStruct.Parity = LL_USART_PARITY_NONE;
  USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
  LL_USART_Init(USART1, &USART_InitStruct);
  LL_USART_SetLINBrkDetectionLen(USART1, LL_USART_LINBREAK_DETECT_10B);
  LL_USART_ConfigLINMode(USART1);
  LL_USART_Enable(USART1);

  __enable_irq();
}

/** \brief Set baud rate for USART module
 * \param USART Type (See stm32f10x.h).
 * \param baud rate - value like: 9600, 19200...
 */
void usart_set_baudrate(USART_TypeDef* usart, uint32_t baudrate)
{
    uint16_t calc_baudrate = 0;

    calc_baudrate = (FCK + (baudrate/2))/baudrate;
    usart->BRR = calc_baudrate;
}


