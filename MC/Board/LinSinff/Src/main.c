#include "main.h"
#include "usart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "usb_device.h"
#include "gpio.h"

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/


#define LOCATE_FUNC  __attribute__((__section__(".mysection")))

/**
 * @brief Address of user program in flash for bootloader
 */
uint32_t user_programm_address = 0x2800;


__IO uint8_t LOCATE_FUNC secret[16] =
{0x7D, 0x6A, 0x70, 0xE1, 0x95, 0xBA, 0x31, 0x05, 0xF5, 0x6E, 0x98, 0x4B, 0xF7, 0x25, 0x4C, 0x0E};

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void MX_FREERTOS_Init(void);

/* Private user code ---------------------------------------------------------*/

/**
 * @brief Reinitialize of USB device in system (Windows).
 * @details Emulate plug and unplug device to/from PC.
 */
void UpdateUSBBus(void)
{
    RCC->APB1ENR |= RCC_APB1ENR_USBEN;
    RCC->APB1ENR &= ~RCC_APB1ENR_USBEN;

    RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
    GPIOA->CRH |= GPIO_CRH_CNF12_0;
    GPIOA->CRH |= GPIO_CRH_MODE12_1;
    GPIOA->BSRR = GPIO_BSRR_BR12;

    HAL_Delay(100);
}

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
    /* Bootloader Configuration-------------------------------------------------*/

    //! Move interrupts vector table if use bootloader
#ifdef BOOTLOADER
    /* it's need for placing the "secret" variable in the flash memory by custom addr */
    secret[0];
    __disable_irq();
    SCB->VTOR = 0x08000000 | (user_programm_address & (uint32_t)0x1FFFFF80);
    __enable_irq();
#endif

    /* MCU Configuration--------------------------------------------------------*/

    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    HAL_Init();

    /* Reinitialize of USB device in system (Windows). */
    UpdateUSBBus();

    /* Configure the system clock */
    SystemClock_Config();

    /* Initialize all configured peripherals */
    MX_GPIO_Init();
    MX_USART1_UART_Init();

    /* Call init function for freertos objects (in freertos.c) */
    MX_FREERTOS_Init();

    /* Start scheduler */
    vTaskStartScheduler();

    /* We should never get here as control is now taken by the scheduler */

    /* Infinite loop */
    while (1) {}

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
    LL_FLASH_SetLatency(LL_FLASH_LATENCY_2);

    if(LL_FLASH_GetLatency() != LL_FLASH_LATENCY_2)
    {
        Error_Handler();
    }
    LL_RCC_HSE_Enable();

    /* Wait till HSE is ready */
    while(LL_RCC_HSE_IsReady() != 1)
    {

    }
    LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSE_DIV_1, LL_RCC_PLL_MUL_9);
    LL_RCC_PLL_Enable();

    /* Wait till PLL is ready */
    while(LL_RCC_PLL_IsReady() != 1)
    {

    }
    LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
    LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_2);
    LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_1);
    LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);

    /* Wait till System clock is ready */
    while(LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL)
    {

    }
    LL_Init1msTick(72000000);
    LL_SYSTICK_SetClkSource(LL_SYSTICK_CLKSOURCE_HCLK);
    LL_SetSystemCoreClock(72000000);
    LL_RCC_SetUSBClockSource(LL_RCC_USB_CLKSOURCE_PLL_DIV_1_5);
}


/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM1 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    if (htim->Instance == TIM1)
    {
        HAL_IncTick();
    }
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{

}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
}
#endif

