#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "queue.h"
#include "semphr.h"
#include "event_groups.h"
#include "main.h"
#include "lin_lld.h"
#include "wpackage.h"
#include "communication.h"
#include "usb_ring_handler.h"
#include "usbd_cdc_if.h"

/* Private includes ----------------------------------------------------------*/

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
LINDriver LIND1;
QueueHandle_t rxusbqueue;
uint8_t txtmp[12];
LINFrame slaverespone[RESPONSE_ARR_SIZE];

//osThreadId defaultTaskHandle;

/* Private function prototypes -----------------------------------------------*/
void StartDefaultTask(void * argument);
void USBRxQueueHandlerTask(void * argument);
void LINRxQueueHandlerTask(void * argument);

extern void MX_USB_DEVICE_Init(void);
void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void)
{
  rxusbqueue = xQueueCreate(20, 20);
  /* add mutexes, ... */

  /* add semaphores, ... */

  /* start timers, add new ones, ... */

  /* definition and creation of defaultTask */

  xTaskCreate(StartDefaultTask,"DefaultTask", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, NULL);
  xTaskCreate(USBRxQueueHandlerTask,"USBRxQueueHandlerTask", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, NULL);
  xTaskCreate(LINRxQueueHandlerTask,"LINRxQueueHandlerTask", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, NULL);

  /* add threads, ... */

  /* add queues, ... */
}


/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
void StartDefaultTask(void* argument)
{

  /* init code for USB_DEVICE */
  MX_USB_DEVICE_Init();

  /* LIN driver init */
  LIND1.usart = USART1;
  LIND1.rxqueue = xQueueCreate(30, sizeof(LINFrame));
  LIND1.resp.resparr = slaverespone;
  LIND1.resp.lastitemptr = 0;

  /* Infinite loop */
  for(;;)
  {
    LL_GPIO_TogglePin(BUS_LED_GPIO_Port,BUS_LED_Pin);
    vTaskDelay(1000);
  }
}

/**
  * @brief  Function implementing the LIN RX queue thread.
  * @param  argument: Not used
  * @retval None
  */
void LINRxQueueHandlerTask(void* argument)
{
  LINFrame rxfrm;
  /* Infinite loop */
  for(;;)
  {
    if (LIND1.rxqueue != 0)
    {
      if(xQueueReceive(LIND1.rxqueue, &rxfrm, portMAX_DELAY))
      {
        LL_GPIO_TogglePin(STATUS_LED_GPIO_Port,STATUS_LED_Pin);

        if (rxfrm.DCL == 0)
        {
          int8_t idx = contains(&rxfrm);

          if (idx >= 0)
          {
            lin_send_slave_response(&LIND1.resp.resparr[idx]);
            // TODO more refactoring here
          }
        }
        else
        {
          txtmp[0] = rxfrm.DCL + 2;
          txtmp[1] = 0x55;
          txtmp[2] = rxfrm.ID;

          for (uint8_t i = 0; i<rxfrm.DCL; i++)
            txtmp[i+3] = rxfrm.data8[i];

          send_wpkg(GET_LIN_FRAME, txtmp, sizeof(txtmp));
        }

      }
    }
    vTaskDelay(2);
  }
}

/**
  * @brief  Function implementing the USB RX queue thread.
  * @param  argument: Not used
  * @retval None
  */
void USBRxQueueHandlerTask(void* argument)
{
  uint8_t tmp[20];
  /* Infinite loop */
  for(;;)
  {
    if (rxusbqueue != 0)
    {
      if(xQueueReceive(rxusbqueue, tmp, portMAX_DELAY))
      {
        LL_GPIO_TogglePin(STATUS_LED_GPIO_Port,STATUS_LED_Pin);
        usb_ring_handler(tmp);
      }
    }
    vTaskDelay(5);
  }
}


/**
  * @brief  Function implementing the USART1 IRQ handler
  */
void USART1_IRQHandler(void)
{
  HandleIrq(&LIND1);
}

/* Private application code --------------------------------------------------*/
