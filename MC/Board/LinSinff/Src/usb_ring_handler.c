#include "usb_ring_handler.h"
#include "wpackage.h"
#include "datahelper.h"
#include "libc.h"
#include "devicecfg.h"
#include "communication.h"
#include "usart.h"
#include "lin_lld.h"

/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/
extern LL_USART_InitTypeDef USART_InitStruct;

/*===========================================================================*/
/* local definitions.                                                        */
/*===========================================================================*/


uint8_t tx_buff[10] = {0};
LINFrame tx_lin_frame;

/*===========================================================================*/
/* Public function implementation                                            */
/*===========================================================================*/


void usb_ring_handler(uint8_t *tmp)
{
  switch(tmp[WPKG_CMD_PTR])
  {
  case GET_DEVICE_NAME:
    send_wpkg(GET_DEVICE_NAME, device_nane, sizeof(device_nane));
    break;

  case GET_DEVICE_VERSION:
    clear_buff(tx_buff, sizeof(tx_buff));
    tx_buff[0] = h_version;
    tx_buff[1] = l_version;
    send_wpkg(GET_DEVICE_VERSION, tx_buff, sizeof(tx_buff));
    break;

  case GET_LIN_BAUDRATE:
    clear_buff(tx_buff, sizeof(tx_buff));
    put_u16(tx_buff, 0, USART_InitStruct.BaudRate);
    send_wpkg(GET_LIN_BAUDRATE, tx_buff, sizeof(tx_buff));
    break;

  case SET_LIN_BAUDRATE:
    enter_critical_section();
    uint16_t tmp_baudrate = get_u16(tmp, 1);

    USART_InitStruct.BaudRate = tmp_baudrate;
    usart_set_baudrate(USART1, tmp_baudrate);
    // TODO Maybe need reinitialize USART module
    exit_critical_section();
    break;

  case GET_LIN_TIMEOUT:
//    clear_buff(tx_buff, sizeof(tx_buff));
//    tx_buff[0] = tim3_struct.TIM_Period;
//    send_wpkg(GET_LIN_TIMEOUT, tx_buff, sizeof(tx_buff));
    break;

  case SET_LIN_TIMEOUT:
//    enter_critical_section();
//    tim3_clear_count();
//    tim3_struct.TIM_Period = (uint16_t)(tmp[1]);
//    tim3_set_period(tmp[1]);
//    exit_critical_section();
    break;

  case SET_LIN_MASTER_FRAME: // Map data from incoming buffer to lin frame struct.
    tx_lin_frame.DCL = tmp[1] - 2; // TODO Edit magic numbers.. Add defines for lin frame bytes in incoming buffer. See protocol.
    tx_lin_frame.ID = tmp[2];
    // tx_lin_frame.busversion = V13;
    tx_lin_frame.crctype = (lincrc_t)tmp[13];
    int j = 3;
    for (int i = 0; i < tx_lin_frame.DCL; i++)
    {
      tx_lin_frame.data8[i] = tmp[j];
      j++;
    }

    //bus_control_enable();

    lin_send_master_data(&tx_lin_frame);

    //bus_control_disable();
    break;

  case SET_MASTER_HEADER:
    //bus_control_enable();

    lin_send_master_header(tmp[1]);

    //bus_control_disable();
    break;

  case SET_LIN_SLAVE_FRAME:

    break;

  case SET_PID_FILTER:
    tx_lin_frame.DCL = tmp[1] - 2; // TODO Edit magic numbers.. Add defines for lin frame bytes in incoming buffer. See protocol.
    tx_lin_frame.ID = tmp[2];
    // tx_lin_frame.busversion = V13;
    tx_lin_frame.crctype = (lincrc_t)tmp[13];
    int a = 3;
    for (int i = 0; i < tx_lin_frame.DCL; i++)
    {
      tx_lin_frame.data8[i] = tmp[a];
      a++;
    }

    set_PID_filter(&tx_lin_frame);
    break;

  case RESET_PID_FILTER:
    reset_PID_filter();
    break;

  case REBOOT:
    /* set BKP register */
    PWR->CR |=  PWR_CR_DBP;
    BKP->DR1 = (uint8_t)2;
    PWR->CR &= ~PWR_CR_DBP;

    NVIC_SystemReset();
    break;
  }
}



