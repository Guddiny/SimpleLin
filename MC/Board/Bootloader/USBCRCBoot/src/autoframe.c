#include "autoframe.h"
#include "usbringhandler.h"

AFrameStates rxAState = WAIT_START;
uint16_t rxPtr = 0;
uint8_t rxCRC = 0;

AutoFrame txAFrame;
AutoFrame rxAFrame;

int rxABuffPtr = 0;

void HandleDataByte(uint8_t dataByte)
{
    switch (rxAState) {
    case WAIT_START:
        if (dataByte == START_CMD) {
            rxAState = WAIT_INCR;
            rxAFrame.DCL = 0;
            rxCRC = 0;
            rxCRC ^= dataByte;
        }
        break;

    case WAIT_INCR:
        rxAFrame.INCR = dataByte;
        rxAState = WAIT_DCL1;
        rxCRC ^= dataByte;
        break;

    case WAIT_DCL1:
        rxAFrame.DCL = rxAFrame.DCL | (dataByte << 8);;
        rxAState = WAIT_DCL2;
        rxCRC ^= dataByte;
        break;

    case WAIT_DCL2:
        rxAFrame.DCL = rxAFrame.DCL | dataByte;
        if (rxAFrame.DCL > MAX_DATA_LEN)
        {
            // Error
        }
        else
        {
            rxAState = WAIT_ADDR;
            rxCRC ^= dataByte;
        }
        break;

    case WAIT_ADDR:
        if (dataByte == ADDR_CMD)
        {
            rxAFrame.ADDR = dataByte;
            rxAState = WAIT_CMD;
            rxCRC ^= dataByte;
        }
        else
        {
            // Error
        }
        break;

    case WAIT_CMD:
        rxAFrame.CMD = dataByte;
        rxCRC ^= dataByte;
        if (rxAFrame.DCL == 1)
        {
            rxAState = WAIT_CRC;
        }
        else
        {
            rxAState = WAIT_DATA;
        }
        break;



    case WAIT_DATA:
        if (rxPtr < rxAFrame.DCL - 1)
        {
            rxAFrame.DATA[rxPtr] = dataByte;
            rxPtr++;
            rxCRC ^= dataByte;
            if (rxPtr == rxAFrame.DCL - 1)
            {
                rxPtr = 0;
                rxAState = WAIT_CRC;
            }
        }
        break;

    case WAIT_CRC:
        rxAState = WAIT_START;
        if(rxCRC == dataByte)
        {
            rxAFrame.ACRC = dataByte;
            usbRingHandler(&rxAFrame);
        }
        else
        {
            // Error CRC
        }
        break;

    default:
        break;
    }
}
