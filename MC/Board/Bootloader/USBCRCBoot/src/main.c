/*
**                           Main.c
**
**********************************************************************/
/*
   Last changed by:    $Author: AlexNL$
   Last changed date:  $Date:  25.02.2018$

**********************************************************************/

/* Core includes */
#include "stm32f10x_conf.h"
#include "string.h"

/* USB includes */
#include "usb_lib.h"
#include "usb_desc.h"
#include "hw_config.h"
#include "usb_pwr.h"
#include "usb_istr.h"
#include "bootcfg.h"

/* User includes */
#include "autoframe.h"
#include "delay.h"

/* User definitions */
//#define LOCATE_FUNC  __attribute__((__section__(".mysection")))

/*============================================================================*/
/* Local Types                                                                  */
/*============================================================================*/

typedef  void (*pFunction)(void);

/*============================================================================*/
/* Variables                                                                  */
/*============================================================================*/

//__IO uint8_t LOCATE_FUNC secret[16] =
//{0x7D, 0x6A, 0x70, 0xE1, 0x95, 0xBA, 0x31, 0x05, 0xF5, 0x6E, 0x98, 0x4B, 0xF7, 0x25, 0x4C, 0x0E};
uint8_t* getSecret = NULL;
uint8_t secretBuff[16] = {};
uint8_t isReboot = 0;
uint8_t codeFact = 0;
uint32_t JumpAddress;
AutoFrame af;

pFunction Jump_To_Application;

/*============================================================================*/
/* Private functions                                                          */
/*============================================================================*/

/*******************************************************************************
 * \brief Reinit USB device in system (Windows).
 * Emulate plug nad unpug device to/from PC.
 ******************************************************************************/
void UpdateUSBBus(void)
{
    // TODO Some time delete this comments

    //RCC_APB1PeriphResetCmd(RCC_APB1Periph_USB, ENABLE);
    //RCC_APB1PeriphResetCmd(RCC_APB1Periph_USB, DISABLE);
    RCC->APB1ENR |= RCC_APB1ENR_USBEN;
    RCC->APB1ENR &= ~RCC_APB1ENR_USBEN;

    // GPIO_InitTypeDef GPIO_InitStructure;
    // RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    // GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
    // GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;
    // GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    // GPIO_Init(GPIOA, &GPIO_InitStructure);
    // GPIO_WriteBit(GPIOA, GPIO_Pin_12, Bit_RESET);

    RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
    GPIOA->CRH |= GPIO_CRH_CNF12_0;
    GPIOA->CRH |= GPIO_CRH_MODE12_1;
    GPIOA->BSRR = GPIO_BSRR_BR12;


    delay_ms(1000);
}

/*******************************************************************************
 *\brief Calculate number of "1" bits in uint8_t array
 * \param pionter to uint8_t array
 * \return bits "1" number
 ******************************************************************************/
static uint8_t GetBitNumber(uint8_t *sourceArray)
{
    uint8_t mask = 0x01;
    uint8_t result = 0;
    for (uint8_t i = 0; i < 16; i++)
    {
        uint8_t sourceNumber = sourceArray[i];
        for (uint8_t iterator = 0; iterator < 8; iterator++)
        {
            if ((sourceNumber & (mask << iterator)) != 0)
            {
                result++;
            }
        }
    }
    return result;
}


/*******************************************************************************
 *\brief try to jumo to user app
 ******************************************************************************/
static void TryToJump(void)
{
    /* check secret code */
    if (codeFact == SECRET_CODE)
    {
        if (((*(__IO uint32_t*)JumpApplicationAddress) & 0x2FFE0000 ) == 0x20000000)//���������, ���� �� ���-������ �� ������ (��� ������ ������ �������� SP ��� ����������, ��� ������ ������)
        {
            JumpAddress = *(__IO uint32_t*) (JumpApplicationAddress + 4);           //����� �������� �� ������� Reset
            Jump_To_Application = (pFunction) JumpAddress;                      //��������� �� ������� ��������
            __set_MSP(*(__IO uint32_t*) JumpApplicationAddress);                    //������������� SP ����������
            GPIO_DeInit(GPIOA);
            //__disable_irq();                                                  // ��������� ����������
            //NVIC_SetVectorTable(NVIC_VectTab_FLASH, 0x10000);                 //����� ������� ������������ ������ Flash
            //__enable_irq();
            Jump_To_Application();                                              //��������� ����������
        }
    }
}


/*******************************************************************************
 *\brief Check boot mode conditions
 * \return true (1) or false(0)
 ******************************************************************************/
static bool CheckBootMode(void)
{
    bool result = 0;

    if (!GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_2))
    {
        result = 1;
    }
    else if (BKP->DR1 == 2)
    {
        PWR->CR |=  PWR_CR_DBP;
        BKP->DR1 = (uint8_t)0;
        PWR->CR &= ~PWR_CR_DBP;
        result = 1;
    }

    return result;
}

/*============================================================================*/
/* Main                                                                       */
/*============================================================================*/

int main(void)
{
    /* it's need for placing the "secret" variable in the flash memory by custom addr */
    //secret[0];

    /* get secret code from memory */
    getSecret = (uint8_t*)SECTER_CODE_ADDR;

    /* calculate bit "1" numbers */
    codeFact = GetBitNumber(getSecret);

    /* LED init */
    RCC->APB2ENR |= RCC_APB2ENR_IOPCEN;
    /* ������������� CRL ��������. */
    GPIOC->CRH	&= ~GPIO_CRH_CNF13;	    /* ����� 00 - Push-Pull */
    GPIOC->CRH 	|= GPIO_CRH_MODE13_0;	/* ����� MODE01 = Max Speed 10MHz */

    /* Button init */
    RCC->APB2ENR |= RCC_APB2ENR_IOPBEN;
    GPIOB->CRL |= GPIO_CRL_CNF2_1;      /* Input mode */
    GPIOB->CRL &= ~GPIO_CRL_CNF2_0;
    GPIOB->CRL &= ~GPIO_CRL_MODE2;
    GPIOB->ODR |= GPIO_ODR_ODR2;        /* Pull up */

    /* Init backup registr */
    RCC->APB1ENR |= RCC_APB1ENR_BKPEN | RCC_APB1ENR_PWREN;

    /* !!!!!!!!!!!!! */
    /* check the bootmode conditions and try to jump to a user app */
    if(CheckBootMode() == 0)
    {
        TryToJump();
    }

    /* Delay module init */
    delay_init();

    /* Reinit USB device in system (Windows).*/
    /* Emulate plug nad unpug device to/from PC */
    UpdateUSBBus();

    /* USB init*/
    Set_USBClock();
    USB_Interrupts_Config();
    USB_Init();

    /* Blink */
    GPIOC->BSRR = GPIO_BSRR_BS13;
    delay_ms(1000);
    GPIOC->BSRR = GPIO_BSRR_BR13;

    while(1)
    {
        if (isReboot == 1)
        {
            delay_ms(1000);
            NVIC_SystemReset();
        }
        if (!GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_2))
        {
            GPIOC->BSRR = GPIO_BSRR_BS13;
            delay_ms(1000);
            GPIOC->BSRR = GPIO_BSRR_BR13;
        }
    }
}


/*******************************************************************************
 * \brief USB interrupt handling
 ******************************************************************************/
void USB_LP_CAN1_RX0_IRQHandler(void)
{
    USB_Istr();
}
