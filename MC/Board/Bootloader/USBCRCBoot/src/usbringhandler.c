#include "usbringhandler.h"
#include "autoframe.h"
#include "stm32f10x_conf.h"
#include "delay.h"
#include "hw_config.h"
#include "devicecfg.h"
#include "bootcfg.h"

/*===========================================================================*/
/* Variables                                                                 */
/*===========================================================================*/


extern uint8_t isReboot;


uint32_t CurrentAddress = ApplicationAddress;
uint8_t idBuffer[12] = {};

//uint8_t __attribute__((section(".mysection"))) myVar = 0x12;

/** \brief Check flash for ready to use
 *
 * \return 1 or 0 (true of false)
 */
static uint8_t FlashReady(void)
{
    return !(FLASH->SR & FLASH_SR_BSY);
}


/** \brief Get Uint16_t from buffer
 *
 * \param target buffer (array)
 * \param index to read
 * \return Uint16_t values
 */
static uint16_t buffer_get_uint16(const uint8_t *buffer, uint32_t *index)
{
    uint16_t res = 	((uint16_t) buffer[*index]) |
                    ((uint16_t) buffer[*index + 1]) << 8;
    //*index += 2;
    return res;
}

/** \brief Put Uint32_t to buffer
 *
 * \param target buffer (array)
 * \param index to write
 * \return void
 */
void buffer_append_uint32(uint8_t* buffer, uint32_t number, uint32_t index)
{
	buffer[(index)++] = number;
	buffer[(index)++] = number >> 8;
	buffer[(index)++] = number >> 16;
	buffer[(index)++] = number >> 24;
}

/** \brief Frite data array to flash
 *
 * \param Flash page address
 * \param pointer to data array
 * \param data length
 * \return void
 */
inline static void FlashWriteBuffer(uint32_t pageAddr, uint8_t* data, uint16_t dataLen)
{
    uint32_t curretnAddr = pageAddr;
    for (uint32_t i = 0; i < dataLen - 1; i+=2)
    {
        uint16_t programWord = buffer_get_uint16(data, &i);
        FLASH_ProgramHalfWord(curretnAddr, programWord);
        while(!FlashReady()) {}
        curretnAddr += 2;
    }
}



//uint8_t v1[] = {0x1B, 0x01, 0x00, 0x15, 0x0E, 0x01, 0x00, 0x55, 0x53, 0x42, 0x32, 0x43, 0x41, 0x4E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x02, 0x03, 0x39};
//uint8_t v2[] = {0x1B, 0x02, 0x00, 0x0A, 0x0E, 0x02, 0x00, 0x55, 0x53, 0x42, 0x32, 0x43, 0x41, 0x4E, 0x00, 0x25 };
//uint8_t v3[] = {0x1B, 0x03, 0x00, 0x04, 0x0E, 0x03, 0x00, 0x01, 0x09, 0x19 };
/** \brief Handling incoming AFrame commands
 *
 * \param pointer to AFrame struct
 * \return void
 */
void usbRingHandler(AutoFrame *aFrame)
{
    switch (aFrame->CMD)
    {
        /* erase all page before writing new firmware */
    case ERASE_ALL:
        for(uint8_t i = 0; i < 60; i++)
        {
            FLASH_Unlock();
            FLASH_ErasePage(0x08002400 + (_EEPROM_FLASH_PAGE_SIZE * i));
            while(!FlashReady()) {}
            FLASH_Lock();
        }
        CurrentAddress = ApplicationAddress;
        USB_Send_Data('d');
        USB_Send_Data('\n');
        break;

        /* programm flash page */
    case PROGRAM_PAGE:
        FLASH_Unlock();
        FlashWriteBuffer(CurrentAddress, aFrame->DATA, aFrame->DCL);
        FLASH_Lock();

        CurrentAddress = CurrentAddress + 1024;

        USB_Send_Data('d');
        USB_Send_Data('\n');

        GPIOC->ODR ^= GPIO_Pin_13;
        break;

        /* jump to user code. Maybe will not use in bootloader */
    case 0xAA:
//        #ifdef DEBUG
//        if (((*(__IO uint32_t*)ApplicationAddress) & 0x2FFE0000 ) == 0x20000000)//���������, ���� �� ���-������ �� ������ (��� ������ ������ �������� SP ��� ����������, ��� ������ ������)
//        {
//            JumpAddress = *(__IO uint32_t*) (ApplicationAddress + 4);           //����� �������� �� ������� Reset
//            Jump_To_Application = (pFunction) JumpAddress;                      //��������� �� ������� ��������
//            __set_MSP(*(__IO uint32_t*) ApplicationAddress);                    //������������� SP ����������
//            GPIO_DeInit(GPIOA);
//            //__disable_irq();                                                  // ��������� ����������
//            //NVIC_SetVectorTable(NVIC_VectTab_FLASH, 0x10000);                 //����� ������� ������������ ������ Flash
//            //__enable_irq();
//            Jump_To_Application();                                              //��������� ����������
//        }
//        #endif
        break;

        /* software reboot for device */
    case REBOOT:
        isReboot = 1;
        break;

        /* get device name */
    case GET_NAME:
        USB_Send_Buff(device_nane, sizeof(device_nane));
        break;

        /* get device version*/
    case GET_VERSION:
        USB_Send_Data(h_version);

        //USB_Send_Buff(v1, sizeof(v1));
        break;

        /* get subversion */
    case GET_SUBVERSION:
        USB_Send_Data(l_version);

        //USB_Send_Buff(v2, sizeof(v2));
        break;

    case GET_DEVICEID:
        buffer_append_uint32(idBuffer, ID1, 0);
        buffer_append_uint32(idBuffer, ID2, 4);
        buffer_append_uint32(idBuffer, ID3, 8);
        USB_Send_Buff(idBuffer, sizeof(idBuffer));
        break;
    }


}
