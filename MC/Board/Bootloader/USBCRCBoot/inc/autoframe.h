#ifndef AUTOFRAME_H
#define AUTOFRAME_H

#define START_CMD   27 // B1
#define ERROR_CMD   238 // EE
#define ADDR_CMD    14 // 0E

#define MAX_DATA_LEN 1024+1
#define AFRAME_SIZE MAX_DATA_LEN+6

#include <stdint.h>



typedef enum
{
    BAD_DCL= 0,
    BAD_ADDR,
    BAD_CRC
}AErrorCodes;

typedef enum {
    WAIT_START = 0,
    WAIT_INCR,
    WAIT_DCL1,
    WAIT_DCL2,
    WAIT_ADDR,
    WAIT_CMD,
    WAIT_DATA,
    WAIT_CRC
}AFrameStates;

typedef struct {
    uint8_t INCR;
    uint16_t DCL;
    uint8_t ADDR;
    uint8_t CMD;
    uint8_t DATA[MAX_DATA_LEN];
    uint8_t ACRC;
}AutoFrame;

extern AutoFrame RxABuffer[30];
extern int rxABuffPtr;

void HandleDataByte(uint8_t dataByte);

#endif /* AUTOFRAME_H */
