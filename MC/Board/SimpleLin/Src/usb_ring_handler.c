#include "usb_ring_handler.h"
#include "ringmanager.h"
#include "communication.h"
#include "wpackage.h"
#include "devicecfg.h"
#include "datahelper.h"
#include "lin.h"
#include "usart.h"
#include "libc.h"


extern UART_HandleTypeDef huart1;
extern TIM_HandleTypeDef htim2;

uint8_t tx_buff[10] = {0};

LIN_FRAME_t tx_lin_frame;

void usb_ring_handler(base_ring *usb_ring)
{
    if (!is_empty_ring(usb_ring))
    {
        uint8_t *tmp;
        tmp = get_from_ring(usb_ring);

        switch(tmp[WPKG_CMD_PTR])
        {
        case GET_DEVICE_NAME:
            send_wpkg(GET_DEVICE_NAME, device_nane, sizeof(device_nane));
            break;

        case GET_DEVICE_VERSION:
            clear_buff(tx_buff, sizeof(tx_buff));
            tx_buff[0] = h_version;
            tx_buff[1] = l_version;
            send_wpkg(GET_DEVICE_VERSION, tx_buff, sizeof(tx_buff));
            break;

        case GET_LIN_BAUDRATE:
            clear_buff(tx_buff, sizeof(tx_buff));
            put_u16(tx_buff, 0, huart1.Init.BaudRate);
            send_wpkg(GET_LIN_BAUDRATE, tx_buff, sizeof(tx_buff));
            break;

        case SET_LIN_BAUDRATE:
            enter_critical_section();
            HAL_UART_MspDeInit(&huart1);
            huart1.Init.BaudRate = get_u16(tmp, 1);
            //USART1->BRR =
            HAL_UART_MspInit(&huart1);
            exit_critical_section();
            break;

        case GET_LIN_TIMEOUT:
            clear_buff(tx_buff, sizeof(tx_buff));
            tx_buff[0] = htim2.Init.Period/2;
            send_wpkg(GET_LIN_TIMEOUT, tx_buff, sizeof(tx_buff));
            break;

        case SET_LIN_TIMEOUT:
            enter_critical_section();
            htim2.Init.Period = tmp[1] * 2;
            TIM2->ARR = (uint32_t)tmp[1] * 2;
            exit_critical_section();
            break;

        case SET_LIN_FRAME: // Map data from incoming buffer to lin frame struct.
            tx_lin_frame.data_len = tmp[1] - 2; // TODO Edit magic numbers.. Add defines for lin frame bytes in incoming buffer. See protocol.
            tx_lin_frame.frame_id = tmp[2];
            int j = 3;
            for (int i = 0; i < tx_lin_frame.data_len; i++)
            {
                tx_lin_frame.data[i] = tmp[j];
                j++;
            }
            LIN_SendData(&tx_lin_frame);
            break;
        }
    }
}
