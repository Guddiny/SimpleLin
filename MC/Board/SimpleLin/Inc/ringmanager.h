#ifndef RINGMANAGER_H
#define RINGMANAGER_H

#include "ring.h"

void add_to_ring(base_ring *ring, uint8_t *buff);
uint8_t* get_from_ring(base_ring *ring);
void clean_ring(base_ring *ring);
int filling_ring(base_ring *ring);
int is_empty_ring(base_ring *ring);

#endif
