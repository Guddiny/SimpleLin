#ifndef DEVICECFG_H
#define DEVICECFG_H

#include "stdint.h"

// Device information
 uint8_t device_nane[] = {"SimpleLIN\n"};
 uint8_t h_version = 0x00;
 uint8_t l_version = 0x01;
 uint16_t lin_baudrate = 9600;
 uint8_t master_timeout = 20; // ms

// Memory map for configuration
// TODO add memory addresses (in flash or eeprom) for: lin baud rate, master timeout and etc.

#endif
