#ifndef USB_RING_HANDLER_H
#define USB_RING_HANDLER_H

#include "ring.h"

void usb_ring_handler(base_ring *usb_ring);

#endif
