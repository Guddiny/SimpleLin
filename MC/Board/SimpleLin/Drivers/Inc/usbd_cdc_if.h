#ifndef __USBD_CDC_IF_H
#define __USBD_CDC_IF_H

#ifdef __cplusplus
 extern "C" {
#endif
/* Includes ------------------------------------------------------------------*/
#include "usbd_cdc.h"
#include "ring.h"


/** @addtogroup STM32_USB_OTG_DEVICE_LIBRARY
  * @{
  */

/** @defgroup USBD_CDC_IF
  * @brief header
  * @{
  */

/** @defgroup USBD_CDC_IF_Exported_Defines
  * @{
  */


/**
  * @}
  */

/** @defgroup USBD_CDC_IF_Exported_Types
  * @{
  */


/**
  * @}
  */

/** @defgroup USBD_CDC_IF_Exported_Macros
  * @{
  */


/**
  * @}
  */

/** @defgroup USBD_AUDIO_IF_Exported_Variables
  * @{
  */
extern USBD_CDC_ItfTypeDef  USBD_Interface_fops_FS;
extern base_ring usb_rx_ring;
extern base_ring usb_tx_ring;

/**
  * @}
  */

/** @defgroup USBD_CDC_IF_Exported_FunctionsPrototype
  * @{
  */
uint8_t CDC_Transmit_FS(uint8_t* Buf, uint16_t Len);


#ifdef __cplusplus
}
#endif

#endif /* __USBD_CDC_IF_H */
