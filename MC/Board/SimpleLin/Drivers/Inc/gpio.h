#ifndef __gpio_H
#define __gpio_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"
#include "main.h"


void MX_GPIO_Init(void);

#define PIN_LED_STATUS			    C, 13, HIGH, MODE_OUTPUT_PUSH_PULL, SPEED_2MHZ
#define PIN_CHIP_ENABLE             A, 0, HIGH, MODE_OUTPUT_PUSH_PULL, SPEED_2MHZ
#define PIN_SSR                     B, 5, HIGH, MODE_OUTPUT_PUSH_PULL, SPEED_2MHZ

#ifdef __cplusplus
}
#endif
#endif /*__ pinoutConfig_H */
