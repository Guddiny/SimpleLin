#include "lin.h"
#include "usbd_cdc_if.h"
#include "macros.h"
#include "gpio.h"
#include "datahelper.h"
#include "wpackage.h"

#define TRUE 1
#define FALSE 0

//----------------------------------------------------------------------------
// Global variables
//----------------------------------------------------------------------------
LIN_MASTER_t LIN_MASTER;
LIN_FRAME_t LIN_FRAME;
LIN_FRAME_t lin_rx_frame;

uint8_t rxChar;
uint8_t linRxBff[1] = {0};

static uint8_t lin_TxRx_array[LIN_TX_ARRAY_LENGHT] = {0};
static uint8_t lin_txRx_array_ptr = LIN_DATA_PTR;
static uint8_t data_len = 0;
//static uint8_t wert = 0;

uint8_t rx_frame_mode = WAIT_BREAK;

LIN_FRAME_t frameBufer[50] = {0};
uint8_t farmeBuffPtr = 0;

uint8_t p_LIN_makeChecksum(LIN_FRAME_t *frame);
void UB_LIN_Master_Init(void);

//----------------------------------------------------------------------------
// Init all before start
//----------------------------------------------------------------------------
void UB_LIN_Master_Init(void)
{
    // init all struct
    LIN_MASTER.mode = WAIT_SYNC;
    LIN_MASTER.data_ptr = 0;
    LIN_MASTER.crc = 0;

    LIN_FRAME.frame_id = 0;
    LIN_FRAME.data_len = 0;
    LIN_FRAME.data[0] = 0;

    // Wait a short time until Transceiver is ready
    HAL_Delay(10);
}

//----------------------------------------------------------------------------
// Send data
//----------------------------------------------------------------------------
LIN_ERR_t LIN_SendData(LIN_FRAME_t *frame)
{
    uint8_t checksum, n;

    //check the length
    if ((frame->data_len < 1) || (frame->data_len > LIN_MAX_DATA))
    {
        return (LIN_WRONG_LEN);
    }

    // checksumme ausrechnen
    checksum = p_LIN_makeChecksum(frame);

    // wait until last Byte has been sent
    //while (USART_GetFlagStatus(LIN_UART, USART_FLAG_TXE) == RESET);

    //------------------------
    // Break-Field
    //------------------------
    HAL_LIN_SendBreak(&huart1);
    //wait until break field has been transmitted
    //while (USART_GetFlagStatus(LIN_UART, USART_FLAG_TC) == RESET);

    // Small pause
    HAL_Delay(LIN_BREAKFIELD_DELAY);

    //------------------------
    // Sync-Field
    //------------------------
    huart1.Instance->DR = LIN_SYNC_DATA;

    //wait until the sync field is sent
    //while (USART_GetFlagStatus(LIN_UART, USART_FLAG_TC) == RESET);

    // kleine Pause
    HAL_Delay(LIN_DATA_BYTE_DELAY);

    //------------------------
    // ID-Field
    //------------------------
    huart1.Instance->DR = frame->frame_id;

    // wait until the ID field has been sent
    //while (USART_GetFlagStatus(LIN_UART, USART_FLAG_TC) == RESET);

    // kleine Pause
    HAL_Delay(LIN_FRAME_RESPONSE_DELAY);

    //------------------------
    // Data-Field [1...n]
    //------------------------
    for (n = 0; n < frame->data_len; n++)
    {
        huart1.Instance->DR = frame->data[n];

        // wait until the DataField has been sent
        //while (USART_GetFlagStatus(LIN_UART, USART_FLAG_TC) == RESET);

        // Small Pause
        HAL_Delay(LIN_DATA_BYTE_DELAY);
    }

    HAL_Delay(LIN_DATA_BYTE_DELAY);

    //------------------------
    // CRC-Field
    //------------------------
    huart1.Instance->DR = checksum;

    // wait until CRC field was sent
    // while (USART_GetFlagStatus(LIN_UART, USART_FLAG_TC) == RESET);

    // small pause
    // so that next Frame is not sent too fast
    HAL_Delay(LIN_INTER_FRAME_DELAY);

    PIN_TOGGLE(PIN_LED_STATUS);

    return (LIN_OK);
}

//----------------------------------------------------------------------------
// LIN Transceiver (MAX13020)
// from the "Sleep Mode" in "Normal-Slope Mode" switch
//----------------------------------------------------------------------------
void p_LIN_aktivateTransceiver(void)
{
    // NSLP-Pin o Lo level
    PIN_ON(PIN_CHIP_ENABLE); // in "Sleep-Mode"
    // Short wait (min. 10us)
    HAL_Delay(LIN_AKTIV_DELAY);
    // NSLP-Pin to Hi-Level
    PIN_OFF(PIN_CHIP_ENABLE); // in "Normal-Slope-Mode"
    // Short wait (min. 10us)
    HAL_Delay(LIN_AKTIV_DELAY);
}

//----------------------------------------------------------------------------
// Calculate Checksum for LIN_Frame
//----------------------------------------------------------------------------
uint8_t p_LIN_makeChecksum(LIN_FRAME_t *frame)
{
    uint8_t ret_wert = 0, n;
    uint16_t dummy;

    // Compute checksums
    dummy = 0;
    for (n = 0; n < frame->data_len; n++)
    {
        dummy += frame->data[n];
        if (dummy > 0xFF)
        {
            dummy -= 0xFF;
        }
    }
    ret_wert = (uint8_t)(dummy);
    ret_wert ^= 0xFF;

    return (ret_wert);
}

//-----------------------------------------------------------------------------
// CLear any uint8_t array
//-----------------------------------------------------------------------------
void clear_buff(uint8_t *array, uint8_t arrSize)
{
    memset(array, 0, arrSize);
}

void Restart_timer(void)
{
    __HAL_TIM_CLEAR_IT(&htim2, TIM_IT_UPDATE);
    HAL_TIM_Base_Stop_IT(&htim2);
    htim2.Instance->CNT = 0;

    HAL_TIM_Base_Start_IT(&htim2);
}

void Stop_timer(void)
{
    __HAL_TIM_CLEAR_IT(&htim2, TIM_IT_UPDATE);
    HAL_TIM_Base_Stop_IT(&htim2);
    htim2.Instance->CNT = 0;
}



//-------------------------------------------------------------------------------
// New - re-factored function
//-------------------------------------------------------------------------------
void HAL_UART_RxCplt(UART_HandleTypeDef *huart)
{
    //Detect "BREAK" field
    if (__HAL_UART_GET_FLAG(&huart1, UART_FLAG_LBD))
    {
        if(rx_frame_mode == WAIT_BREAK)
        {
            rx_frame_mode = WAIT_DATA;
        }
        else if(rx_frame_mode == WAIT_DATA)
        {
            Transmit_To_Pc();
            PIN_TOGGLE(PIN_LED_STATUS); // Led blink
        }

        __HAL_UART_CLEAR_FLAG(&huart1, UART_FLAG_LBD);
    }

    // Detect other bytes
    if (__HAL_UART_GET_FLAG(&huart1, UART_FLAG_RXNE))
    {
        if(rx_frame_mode == WAIT_DATA)
        {
            lin_TxRx_array[lin_txRx_array_ptr] = huart1.Instance->DR;
            lin_txRx_array_ptr++;
            data_len++;
        }

        __HAL_UART_CLEAR_FLAG(&huart1, UART_FLAG_RXNE);
        Restart_timer();
    }

    // Recive new data
    HAL_UART_Receive_IT(&huart1, &rxChar, 1);
}

//------------------------------------------------------------------------------
// Period elapsed callback in non blocking mode - new re-factored function
//------------------------------------------------------------------------------
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    Transmit_To_Pc();
    rx_frame_mode = WAIT_BREAK;
}

//------------------------------------------------------------------------------
// Transmit complete message (received LIN frame + service bytes)
//------------------------------------------------------------------------------
void Transmit_To_Pc(void)
{
    Stop_timer();
    lin_TxRx_array[LIN_DATALEN_PTR] = data_len; // Temporary hack
    send_wpkg(GET_LIN_FRAME, lin_TxRx_array, LIN_TX_ARRAY_LENGHT); // Pack data to "wpkg" and send it to PC

    clear_buff(lin_TxRx_array, sizeof(lin_TxRx_array)); // Clear data array
    data_len = 0; // Reset data length
    lin_txRx_array_ptr = LIN_DATA_PTR; // Reset data pointer


  //  HAL_GPIO_TogglePin(LED_PIN_GPIO_Port, LED_PIN_Pin); // Led blink
}


//------------------------------------------------------------------------------
// Change solid state relay to "cut" original master from lin network.
//------------------------------------------------------------------------------
void origin_master_enable(void)
{
    PIN_OFF(PIN_SSR);
}


//------------------------------------------------------------------------------
// Change solid state relay to "connect" original master from lin network.
//------------------------------------------------------------------------------
void origin_master_disable(void)
{
    PIN_ON(PIN_SSR);
}
