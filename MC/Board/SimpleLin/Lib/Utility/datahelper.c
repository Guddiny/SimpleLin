#include "datahelper.h"

uint16_t get_u16(uint8_t* buff, uint8_t start_ptr)
{
    uint16_t u16 = 0x0;
    u16 = buff[start_ptr] << 8 | buff[start_ptr + 1];

    return u16;
}

void put_u16(uint8_t* buff, uint8_t start_ptr, uint16_t value)
{
    buff[start_ptr] = (value & 0xFF00) >> 8;
    buff[start_ptr + 1] = value & 0xFF;
}
