#include "wpackage.h"
#include "usbd_cdc_if.h"
#include "communication.h"


uint8_t get_crc(uint8_t *buff)
{
    uint8_t crc = 0x0;
    for(int i = 0; i < WPKG_CRC_PTR; i++)
    {
        crc = crc ^ buff[i];
    }

    return crc;
}


bool check_crc(uint8_t *buff)
{
    uint8_t act_crc = get_crc(buff);
    if (act_crc == buff[WPKG_CRC_PTR])
        return true;
    return false;
}


void send_wpkg(uint8_t cmd, uint8_t *buff, uint8_t data_len)
{
    uint8_t data_ptr = WPKG_DATA_PTR;
    uint8_t crc = 0x0;
    uint8_t tx_data[W_PKG_SIZE] = {0};
    // TODO add data length checking

    tx_data[WPKG_CMD_PTR] =  cmd;
    crc = crc ^ cmd;
    for(int i = 0; i<data_len; i++)
    {
        tx_data[data_ptr] = buff[i];
        crc = crc ^ buff[i];
        data_ptr++;
    }
    tx_data[WPKG_CRC_PTR] = crc;
    CDC_Transmit_FS(tx_data, W_PKG_SIZE); // Transmit data to PC
}
