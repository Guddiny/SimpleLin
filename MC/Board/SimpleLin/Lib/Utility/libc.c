#include "libc.h"

// ********************* IRQ helpers *********************
int critical_depth = 0;

void enter_critical_section()
{
    __disable_irq();
    // this is safe because interrupts are disabled
    critical_depth += 1;
}

void exit_critical_section()
{
    // this is safe because interrupts are disabled
    critical_depth -= 1;
    if (critical_depth == 0)
    {
        __enable_irq();
    }
}

// Compare memory
int Memcmp(const void * ptr1, const void * ptr2, unsigned int num)
{
    int i;
    for (i = 0; i < num; i++)
    {
        if ( ((uint8_t*)ptr1)[i] != ((uint8_t*)ptr2)[i] ) return -1;
    }
    return 0;
}
