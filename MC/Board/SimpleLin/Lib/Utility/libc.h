#ifndef __libc_H
#define __libc_H
// TODO  refactor this file

#include "stm32f1xx_hal.h"

//################################################################################################################
void enter_critical_section();
void exit_critical_section();
int Memcmp(const void * ptr1, const void * ptr2, unsigned int num);
//################################################################################################################

#endif
