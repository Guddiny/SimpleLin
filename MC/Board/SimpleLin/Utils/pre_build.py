from file_manager import inplace_change


print("////////////////////////////////////////// Pre-build started //////////////////////////////////////////////////////////////")
print("Move flash and sram for BOOTLOADER")

inplace_change("STM32F103C8_FLASH.ld", "0x8000000", "0x8002000")
inplace_change("STM32F103C8_FLASH.ld", "LENGTH = 64K", "LENGTH = 57K")
inplace_change("stm32f103c8_sram.ld", "0x08000000", "0x08002000")
inplace_change("stm32f103c8_sram.ld", "LENGTH = 64K", "LENGTH = 57K")

print("///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////")