from file_manager import inplace_change

print("////////////////////////////////////////// Post-build started //////////////////////////////////////////////////////////////")
print("Move flash and sram to default values")


inplace_change("STM32F103C8_FLASH.ld", "0x8002000", "0x8000000")
inplace_change("STM32F103C8_FLASH.ld", "LENGTH = 57K", "LENGTH = 64K")
inplace_change("stm32f103c8_sram.ld", "0x08002000", "0x08000000")
inplace_change("stm32f103c8_sram.ld", "LENGTH = 57K", "LENGTH = 64K")

print("///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////")