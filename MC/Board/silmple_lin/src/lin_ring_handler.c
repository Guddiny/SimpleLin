#include "lin_ring_handler.h"
#include "lin.h"
#include "ringmanager.h"
#include "wpackage.h"
#include "delay.h"

void lin_ring_handler(base_ring *lin_ring)
{
    if (!is_empty_ring(lin_ring))
    {
        delay_ms(3);
        uint8_t *tmp;
        tmp = get_from_ring(lin_ring);

         send_wpkg(GET_LIN_FRAME, tmp, LIN_TX_ARRAY_LENGHT); // Pack data to "wpkg" and send it to PC
    }
}
