/**********************************************************************
   Last committed:     V 0.1
   Last changed by:    AlexNL
   Last changed date:  Dec 12, 2017

**********************************************************************/

#include "stm32f10x_conf.h"

// USB lib includes
#include "usb_lib.h"
#include "usb_desc.h"
#include "hw_config.h"
#include "usb_pwr.h"
#include "usb_istr.h"

// Othre includes
#include "board.h"
#include "delay.h"
#include "usart.h"
#include "lin_lld.h"
#include "gpio.h"
#include "tim.h"
#include "libc.h"
#include "ringmanager.h"
#include "usb_ring_handler.h"
#include "lin_ring_handler.h"
#include "flash_controller.h"

#define LOCATE_FUNC  __attribute__((__section__(".mysection")))
/*===========================================================================*/
/* Global definitions.                                                       */
/*===========================================================================*/
__IO uint8_t LOCATE_FUNC secret[16] =
{0x7D, 0x6A, 0x70, 0xE1, 0x95, 0xBA, 0x31, 0x05, 0xF5, 0x6E, 0x98, 0x4B, 0xF7, 0x25, 0x4C, 0x0E};

flash_data_type app_cfg;
flash_data_type* eeprom;

base_ring usb_rx_ring;
base_ring usb_tx_ring;
base_ring lin_rx_ring;
base_ring lin_tx_ring;


LINDriver LIND1;
LINDriverConfig lin1cfg;

/*===========================================================================*/
/* local definitions.                                                        */
/*===========================================================================*/


/**
 * @brief Address of user program in flash for bootloader
 */
uint32_t user_programm_address = 0x2800;


void on_frame_rx(LINDriver *linp, LINFrame *rxFramep)
{

}

void on_err_rx(LINDriver *linp, uint32_t errorcode)
{

}


void on_id_rx(LINDriver *linp, uint8_t id)
{

}

/**
 * @brief Configure LIN1 drier (Uses USART1)
 */
void linDrv_config_init()
{
    lin1cfg.rxframe_cb = on_frame_rx;
    lin1cfg.rxerr_cb = on_err_rx;
    lin1cfg.rxid_cb = on_id_rx;

    LIND1.config = &lin1cfg;
    LIND1.usart = USART1;
    LIND1.rxBuffer = &lin_rx_ring;
    LIND1.txBuffer = &lin_tx_ring;
}

/**
 * @brief Nice function which indicate main firmware module was load
 */
void load_indicator()
{
    for(int i = 0; i < 7; i++)
    {
        delay_ms(40);
        gpio_pin_toggle(LED_BUS_PORT, LED_BUS_PIN);
    }

    for(int i = 0; i < 5; i++)
    {
        delay_ms(40);
        gpio_pin_toggle(LED_STATUS_PORT, LED_STATUS_PIN);
    }

    gpio_pin_reset(LED_STATUS_PORT, LED_STATUS_PIN);
    gpio_pin_set(LED_BUS_PORT, LED_BUS_PIN);
}


/**
 * @brief Reinitialize of USB device in system (Windows).
 * @details Emulate plug and unplug device to/from PC.
 */
void UpdateUSBBus(void)
{
    RCC->APB1ENR |= RCC_APB1ENR_USBEN;
    RCC->APB1ENR &= ~RCC_APB1ENR_USBEN;

    RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
    GPIOA->CRH |= GPIO_CRH_CNF12_0;
    GPIOA->CRH |= GPIO_CRH_MODE12_1;
    GPIOA->BSRR = GPIO_BSRR_BR12;

    delay_ms(1000);
}

/*===========================================================================*/
/* Application entry point                                                   */
/*===========================================================================*/


int main(void)
{
    //! Move interrupts vector table if use bootloader
#ifdef BOOTLOADER
    /* it's need for placing the "secret" variable in the flash memory by custom addr */
    secret[0];
    __disable_irq();
    SCB->VTOR = 0x08000000 | (user_programm_address & (uint32_t)0x1FFFFF80);
    __enable_irq();
#endif

    // Tools setup
    delay_init();

    /* Reinit USB device in system (Windows).*/
    /* Emulate plug nad unpug device to/from PC */
    UpdateUSBBus();

    // USB setup
    Set_USBClock();
    USB_Interrupts_Config();
    USB_Init();

    // Load configuration
    app_cfg = *(app_config_init());

    // Board setup
    board_init();

    // USART setup
    usart1_spl_init(9600);

    // lin driver and config init
    linDrv_config_init();

    // LIN setup
    ldStart(&LIND1);

    // Timers setup
    tim3_Int_Init(100);

    // Led blink - all peripherial configured!!
    load_indicator();

    //chip_enable();

    gpio_pin_set(BUS_CONTROL_PORT, BUS_CONTROL_PIN);
    gpio_pin_reset(BUS_CONTROL_PORT, BUS_CONTROL_PIN);

    while(1)
    {
        usb_ring_handler(&usb_rx_ring);
        usb_ring_handler(&usb_tx_ring);
        lin_ring_handler(&lin_rx_ring);
        lin_ring_handler(&lin_tx_ring);
    }

}


/*===========================================================================*/
/* IRQ Handling                                                              */
/*===========================================================================*/

/**
 * @brief Handling USB interrupts
 */
void USB_LP_CAN1_RX0_IRQHandler(void)
{
    USB_Istr();
}


/*===========================================================================*/
/* Application error handling                                                */
/*===========================================================================*/
