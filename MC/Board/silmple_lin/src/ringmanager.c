#include "ringmanager.h"
#include <stdlib.h>
#include <stdbool.h>


/*===========================================================================*/
/* local definitions.                                                        */
/*===========================================================================*/


uint8_t tmp_arr[W_PKG_SIZE] = {0};


/*===========================================================================*/
/* Public function implementation                                            */
/*===========================================================================*/

//
// Add new value to ring buffer
//
void add_to_ring(base_ring *ring, uint8_t *buff)
{
    memcpy(ring->elems[ring->w_ptr++], buff, 20);
    ring->w_ptr &= RING_MASK;
}

//
// Get value from ring buffer
//
uint8_t* get_from_ring(base_ring *ring)
{
    memcpy(tmp_arr, ring->elems[ring->r_ptr++], W_PKG_SIZE);
    ring->r_ptr &= RING_MASK;
    return tmp_arr;
}

//
// "Stop" and clean ring buffer
//
void clean_ring(base_ring *ring)
{
    ring->r_ptr = ring->w_ptr;
}

//
// Return number of data witch contains in buffer
//
int filling_ring(base_ring *ring)
{
    if (ring->w_ptr >= ring->r_ptr)
    {
        return ring->w_ptr - ring->r_ptr;
    }
    else
    {
        return (RING_SIZE - ring->r_ptr) + ring->w_ptr;
    }
}

//
// Check filling array
//
int is_empty_ring(base_ring *ring)
{
    if (ring->r_ptr == ring->w_ptr)
    {
        return true;
    }
    else
    {
        return false;
    }
}

