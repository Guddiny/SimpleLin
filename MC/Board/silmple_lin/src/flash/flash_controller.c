#include "flash_controller_private.h"
#include <string.h>


/*===========================================================================*/
/* Driver local definitions.                                                 */
/*===========================================================================*/


static eeprom_data_type* eeprom_data = {0};
uint16_t _tmp_cfg_arr[sizeof(eeprom_data_type)]= {0};

eeprom_data_type default_eeprom_cfg =
{
    IS_WRITTED_FLAG,
    {
        9600,
        20
    }
};


/*===========================================================================*/
/* Private function implementation                                           */
/*===========================================================================*/


//
//data - pointer to data struct
//address - address inflash
//count - data quantity, must be devisibl to 2
//
static void _flash_write(eeprom_data_type* cfg, uint32_t address, uint32_t count)
{
    uint32_t i;

    FLASH_Unlock();

    memcpy(_tmp_cfg_arr, cfg, sizeof(*cfg));

    while (FLASH->SR & FLASH_SR_BSY);
    if (FLASH->SR & FLASH_SR_EOP)
    {
        FLASH->SR = FLASH_SR_EOP;
    }

    uint16_t j = 0;
    for (i = 0; i < count; i += 2)
    {
        FLASH_ProgramHalfWord(FLASH_DATA_START_ADDRESS + i, _tmp_cfg_arr[j]);
        while (!(FLASH->SR & FLASH_SR_EOP));
        FLASH->SR = FLASH_SR_EOP;
        j++;
    }

    FLASH_Lock();
}


/*===========================================================================*/
/* Public function implementation                                            */
/*===========================================================================*/


//
// Init application config
// return - pointer to current application config
//
flash_data_type* app_config_init()
{
    eeprom_data = (eeprom_data_type*) (FLASH_DATA_START_ADDRESS);

    if(eeprom_data->mark != IS_WRITTED_FLAG)
    {
        FLASH_Unlock();
        FLASH_ErasePage(FLASH_DATA_START_ADDRESS);
        _flash_write(&default_eeprom_cfg, FLASH_DATA_START_ADDRESS, 32);
        FLASH_Lock();
    }

    return &eeprom_data->app_cfg_data;
}

flash_data_type* update_config(flash_data_type* new_app_cfg)
{
    default_eeprom_cfg.app_cfg_data = *new_app_cfg;

    FLASH_Unlock();

    FLASH_ErasePage(FLASH_DATA_START_ADDRESS);
    _flash_write(&default_eeprom_cfg, FLASH_DATA_START_ADDRESS, 32);

    FLASH_Lock();

    return &eeprom_data->app_cfg_data;
}






















//static void readAll()
//{
//    uint32_t *start_ptr = (uint32_t *)FLASH_DATA_START_ADDRESS;
//    flash_data_type = *((flash_data_type_private *)start_ptr);
//}

//static void _erase()
//{
//    uint32_t page_iterator = 0;
//    for(page_iterator = 0; page_iterator < REQUARED_PAGES_QTY; page_iterator++)
//    {
//        FLASH_ErasePage(CALC_PAGE_ADDR_BY_NUMBRE(page_iterator));
//    }
//}

//static void _write()
//{
//    uint32_t *start_ptr = (uint32_t *)FLASH_DATA_START_ADDRESS;
//    *((flash_data_type_private *)start_ptr) = flash_data;
//}

//data - pointer to data struct
//address - address inflash
//count - �data quantity, must be devisibl to 2
//void Internal_Flash_Write(uint8_t* data, unsigned int address, unsigned int count)
//{
//    unsigned int i;
//
//    FLASH_Unlock();
//
//    while (FLASH->SR & FLASH_SR_BSY);
//    if (FLASH->SR & FLASH_SR_EOP)
//    {
//        FLASH->SR = FLASH_SR_EOP;
//    }
//
//    FLASH->CR |= FLASH_CR_PG;
//
//    for (i = 0; i < count; i += 2)
//    {
//        *(__IO uint16_t*)(address + i) = (((uint16_t)data[i + 1]) << 8) + data[i];
//        while (!(FLASH->SR & FLASH_SR_EOP));
//        FLASH->SR = FLASH_SR_EOP;
//    }
//
//    FLASH->CR &= ~(FLASH_CR_PG);
//
//    FLASH_Lock();
//}
//


//static void writeAll()
//{
//    FLASH_Unlock();
//    _erase();
//    _write();
//    FLASH_Lock();
//}
//
//flash_data_type *flashData()
//{
//    readAll();
//    return &flash_data.flash_data;
//}
//
//void flashUpdate()
//{
//    flash_data.mark = DATA_NOT_WRITTEN_MARK;
//    writeAll();
//}
