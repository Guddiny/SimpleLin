#ifndef FLASH_CONTROLLER_H
#define FLASH_CONTROLLER_H

#include <stdint.h>
#include "stm32f10x_conf.h"


/*===========================================================================*/
/* Difinitions                                                               */
/*===========================================================================*/

#define COMMAND_QTY      20


/*===========================================================================*/
/* Driver data structures and types.                                         */
/*===========================================================================*/


typedef struct {
    uint8_t type;
    uint32_t action;
    uint32_t increment;
    //and etc.
} command_type;

typedef struct {
    uint16_t baudrate;
    uint8_t master_timeout;
    command_type commands[COMMAND_QTY];
    //and etc.
} flash_data_type;


/*===========================================================================*/
/* Function prototypes.                                                      */
/*===========================================================================*/


flash_data_type* app_config_init();
flash_data_type* update_config(flash_data_type* new_app_cfg);

#endif
