#ifndef FLASH_CONTROLLER_PRIVATE_H
#define FLASH_CONTROLLER_PRIVATE_H

#include "flash_controller.h"
#include "eepromConfig.h"


/*===========================================================================*/
/* Difinitions                                                               */
/*===========================================================================*/


#define FLASH_DATA_START_ADDRESS        _EEPROM_FLASH_PAGE_ADDRESS

#define IS_WRITTED_FLAG               0xED  /**< Show, that ths EEPROM emulation page is writed */


/*===========================================================================*/
/* Driver data structures and types.                                         */
/*===========================================================================*/


typedef struct {
    uint8_t mark;
    flash_data_type app_cfg_data;
} eeprom_data_type;


#endif
