#ifndef DEVICECFG_H_INCLUDED
#define DEVICECFG_H_INCLUDED

#include "stdint.h"


/*===========================================================================*/
/* Common devide definitions                                                 */
/*===========================================================================*/


uint8_t device_nane[] = {"SimpleLIN\n"};
uint8_t h_version = 0x01;
uint8_t l_version = 0x00;
uint16_t lin_baudrate = 9600;
uint8_t master_timeout = 20; // ms


/*===========================================================================*/
/* Memory map for configuration                                              */
/*===========================================================================*/

// TODO add memory addresses (in flash or eeprom) for: lin baud rate, master timeout and etc.


#endif /* DEVICECFG_H_INCLUDED */
