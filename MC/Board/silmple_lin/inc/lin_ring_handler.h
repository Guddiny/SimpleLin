#ifndef LIN_RING_HANDLER_H_INCLUDED
#define LIN_RING_HANDLER_H_INCLUDED
#include "ring.h"

void lin_ring_handler(base_ring *lin_ring);

#endif /* LIN_RING_HANDLER_H_INCLUDED */
