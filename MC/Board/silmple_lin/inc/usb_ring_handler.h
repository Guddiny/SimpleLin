#ifndef USB_RING_HANDLER_H_INCLUDED
#define USB_RING_HANDLER_H_INCLUDED

#include "ring.h"

/*===========================================================================*/
/* Dfinitions                                                                */
/*===========================================================================*/


/*===========================================================================*/
/* Function prototypes.                                                      */
/*===========================================================================*/


void usb_ring_handler(base_ring *usb_ring);


#endif /* USB_RING_HANDLER_H_INCLUDED */
