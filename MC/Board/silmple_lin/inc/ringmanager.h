#ifndef RINGMANAGER_H_INCLUDED
#define RINGMANAGER_H_INCLUDED

#include "ring.h"


/*===========================================================================*/
/* Difinitions                                                               */
/*===========================================================================*/


/*===========================================================================*/
/* Function prototypes.                                                      */
/*===========================================================================*/


void add_to_ring(base_ring *ring, uint8_t *buff);
uint8_t* get_from_ring(base_ring *ring);
void clean_ring(base_ring *ring);
int filling_ring(base_ring *ring);
int is_empty_ring(base_ring *ring);



#endif /* RINGMANAGER_H_INCLUDED */
