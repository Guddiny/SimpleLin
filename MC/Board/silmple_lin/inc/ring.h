#ifndef RING_H_INCLUDED
#define RING_H_INCLUDED

#include <stdio.h>
#include "communication.h"


/*===========================================================================*/
/* Difinitions                                                               */
/*===========================================================================*/

// TODO maybe  add RING_SIZE and RING_MASK inside to base_ring struct. for better flexibility.
#define RING_SIZE 0x20
#define RING_MASK RING_SIZE-1


/*===========================================================================*/
/* Driver data structures and types.                                         */
/*===========================================================================*/


typedef struct base_ring {
  uint8_t w_ptr;
  uint8_t r_ptr;
  uint8_t elems[RING_SIZE][W_PKG_SIZE];
} base_ring;



#endif /* RING_H_INCLUDED */
