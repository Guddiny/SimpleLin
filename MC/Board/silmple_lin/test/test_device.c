/**********************************************************************
   Last committed:     V 0.1
   Last changed by:    AlexNL
   Last changed date:  Dec 12, 2017

**********************************************************************/

#include "stm32f10x_conf.h"

// USB lib includes
#include "usb_lib.h"
#include "usb_desc.h"
#include "hw_config.h"
#include "usb_pwr.h"
#include "usb_istr.h"

// Othre includes
#include "board.h"
#include "delay.h"
#include "usart.h"
#include "lin.h"
#include "gpio.h"
#include "tim.h"
#include "libc.h"
#include "ringmanager.h"
#include "usb_ring_handler.h"
#include "flash_controller.h"


/*===========================================================================*/
/* Global definitions.                                                        */
/*===========================================================================*/


flash_data_type app_cfg;
flash_data_type* eeprom;

base_ring usb_rx_ring;
base_ring usb_tx_ring;
base_ring lin_rx_ring;


/*===========================================================================*/
/* local definitions.                                                        */
/*===========================================================================*/


//
// Address of user programm in flash for bootloader
//
uint32_t user_programm_address = 0x2000;

//
// Nice function which indicate, that main firmware module if loaded
//
void load_indicator()
{
    for(int i = 0; i < 8; i++)
    {
        delay_ms(50);
        gpio_pin_toggle(LED_BUS_PORT, LED_BUS_PIN);
    }
}



/*===========================================================================*/
/* Tests                                                                     */
/*===========================================================================*/

#define TEST_DELAY      4000    /**< Value in "ms" after each test */

//
// Test data definitions
//
linFrame_t test_frame1 =
{
    1,
    0xC1,
    4,
    {0xAA, 0xBB, 0xCC, 0xDD},
    0 /**< Calculate automaticaly in send function */
};

linFrame_t test_frame2 =
{
    2,
    0x42,
    8,
    {0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8},
    0 /**< Calculate automaticaly in send function */
};

//
// Test casies
//
void TEST_two_frame(uint16_t repetQtq, uint16_t delayMs)
{
    for (uint16_t i = 0; i < repetQtq; i++)
    {
        lin_send_master_data(&test_frame1);
        delay_ms(delayMs);
        lin_send_master_data(&test_frame2);
        delay_ms(delayMs);
    }
}

void TEST_onw_frame(uint16_t repetQtq, uint16_t delayMs)
{
    for (uint16_t i = 0; i < repetQtq; i++)
    {
        lin_send_master_data(&test_frame1);
        delay_ms(delayMs);
    }
}

void TEST_onw_frame_fast(uint16_t repetQtq)
{
    for (uint8_t i = 0; i < repetQtq; i++)
    {
        lin_send_master_data(&test_frame1);
        delay_ms(3);
    }
}


/*===========================================================================*/
/* Application entry point                                                   */
/*===========================================================================*/


int main(void)
{
    //! Move interrupts vector table if use bootloader
#ifdef BOOTLOADER
    __disable_irq();
    SCB->VTOR = 0x08000000 | (user_programm_address & (uint32_t)0x1FFFFF80);
    __enable_irq();
#endif

    // USB setup
    Set_USBClock();
    USB_Interrupts_Config();
    USB_Init();

    // Tools setup
    delay_init();

    // Load configuration
    app_cfg = *(app_config_init());

    // Board setup
    board_init();

    // USART setup
    usart1_spl_init(9600);

    // LIN setup
    usart_lin_init(USART1);

    // Timers setup
    tim3_Int_Init(100);

    // Led blink - all peripherial configured!!
    load_indicator();

    chip_enable();

    GPIOB->CRL |= GPIO_CRL_CNF7; // Disable echo rx
    //GPIOB->CRL |= GPIO_ODR_ODR6;


    while(1)
    {
        delay_ms(TEST_DELAY);

        TEST_two_frame(300, 20);

        delay_ms(TEST_DELAY);

        TEST_onw_frame(300, 5);

        delay_ms(TEST_DELAY);

        TEST_onw_frame_fast(200);

        delay_ms(TEST_DELAY);

        TEST_two_frame(300, 2000);

        return 0;
    }

}


/*===========================================================================*/
/* IRQ Handling                                                              */
/*===========================================================================*/


//
// Handling USB interrupts
//
void USB_LP_CAN1_RX0_IRQHandler(void)
{
    USB_Istr();
}


/*===========================================================================*/
/* Application error handling                                                */
/*===========================================================================*/
