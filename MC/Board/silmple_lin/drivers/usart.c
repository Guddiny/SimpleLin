#include "usart.h"

//Private functions declarations


// Public variables

USART_InitTypeDef usart1_struct;

// Private variables


/*===========================================================================*/
/* Public function implementation                                            */
/*===========================================================================*/


/** \brief Initialize USART1
 * \param baud rate - value like: 9600, 19200...
 */
void usart1_init(uint32_t baudrate)
{
    // Enable clock for USART1
    SET_BIT(RCC->APB2ENR, RCC_APB2ENR_USART1EN);

    /* Pins settings */
    // Enable clock for USART1 pins
    SET_BIT(RCC->APB2ENR, RCC_APB2ENR_IOPBEN);
    SET_BIT(RCC->APB2ENR, RCC_APB2ENR_AFIOEN);

    // TX pin settings (out Push-Pull) --> PB9
    RCC->APB2ENR |= RCC_APB2ENR_IOPAEN | RCC_APB2ENR_AFIOEN;     // GPIOA Clock ON. Alter function clock ON
    GPIOA->CRH  &= ~GPIO_CRH_CNF9;      // Clear CNF bit 9
    GPIOA->CRH  |= GPIO_CRH_CNF9_1;     // Set CNF bit 9 to 10 - AFIO Push-Pull
    GPIOA->CRH  |= GPIO_CRH_MODE9_0;    // Set MODE bit 9 to Mode 01 = 10MHz

    // RX pin settings (input) --> PB10
    CLEAR_BIT(GPIOB->CRL, GPIO_CRL_CNF7);
    SET_BIT(GPIOB->CRL, GPIO_CRL_CNF7_0);
    CLEAR_BIT(GPIOB->CRL, GPIO_CRL_MODE7);

    // Set baud rate for USART1
    usart_set_baudrate(USART1, baudrate);

    // Enable TX,RX, activate RXNE interrupt
    //USART1->CR1 |= USART_CR1_UE|USART_CR1_TE|USART_CR1_RE;

    // Activate RXNE interrupt
    USART1->CR1 |= USART_CR1_RXNEIE;

    // Activate LBD interrupt
    //USART1->CR1 |= USART_CR2_LBDIE;

    // Enable activated interrupts
    NVIC_EnableIRQ(USART1_IRQn);

    // Enable global interrupts
    __enable_irq();
}

//
// USART1 init by SPL library
//
void usart1_spl_init(uint32_t baudrate)
{
    /* Enable PORT B clock, Alternate functions and USART1 */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 | RCC_APB2Periph_AFIO |
                           RCC_APB2Periph_GPIOB, ENABLE);
    /* Remap USART1 pins to TB6->TX, PB7->RX */
    GPIO_PinRemapConfig(GPIO_Remap_USART1,ENABLE);

    /* Initialize GPIOB: 50MHz, PIN6, Push-pull Output */
    GPIO_InitTypeDef gpioa_init_struct =
    {
        GPIO_Pin_6,
        GPIO_Speed_50MHz,
        GPIO_Mode_AF_PP
    };
    GPIO_Init(GPIOB, &gpioa_init_struct);

    /* USART1 config */
    USART_Cmd(USART1, ENABLE);

    usart1_struct.USART_BaudRate = baudrate;
    usart1_struct.USART_WordLength = USART_WordLength_8b;
    usart1_struct.USART_StopBits = USART_StopBits_1;
    usart1_struct.USART_Parity = USART_Parity_No ;
    usart1_struct.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    usart1_struct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;

    USART_Init(USART1, &usart1_struct);

    /* Enable RXNE interrupt */
    USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);

    /* Enable LBD interrupt */
    USART_ITConfig(USART1, USART_IT_LBD, ENABLE);

    /* Enable IDLE interrupt */
    USART_ITConfig(USART1, USART_IT_IDLE, DISABLE);

     /* Disable TXE interrupt */
    USART_ITConfig(USART1, USART_IT_TXE, DISABLE);

    /* Enable USART1 global interrupt */
    NVIC_EnableIRQ(USART1_IRQn);
}


/** \brief Set baud rate for USART module
 * \param USART Type (See stm32f10x.h).
 * \param baud rate - value like: 9600, 19200...
 */
void usart_set_baudrate(USART_TypeDef* usart, uint32_t baudrate)
{
    uint16_t calc_baudrate = 0;

    calc_baudrate = (FCK + (baudrate/2))/baudrate;
    usart->BRR = calc_baudrate;
}


/** \brief Send data byte by USART
 * \param USART Type. See stm32f10x.h
 * \param data - any uint8_t value
 */
void usart_send_byte(USART_TypeDef* uasrt, uint8_t byte)
{
    uasrt->DR = byte;
}


/** \brief Send array of data byte by USART
 * \param USART Type (See stm32f10x.h).
 * \param data - any array of uint8_t values
 */
void usart_send_data(USART_TypeDef* uasrt, uint8_t* data, int data_qty)
{
    for (int i = 0; i < data_qty; data_qty++)
    {
        uasrt->DR = data[i];
    }
}
