#include "lin_lld.h"
#include "gpio.h"
#include "datahelper.h"
#include "wpackage.h"
#include "stm32f10x.h"
#include "delay.h"
#include "hw_config.h"
#include "libc.h"
#include "tim.h"
#include "board.h"
#include "ring.h"
#include "ringmanager.h"

/*===========================================================================*/
/* Global variables                                                         */
/*===========================================================================*/


/*===========================================================================*/
/* Driver local definitions.                                                 */
/*===========================================================================*/
LINFrame rxFrame;
LINFrame txFrame;

uint8_t data[4] = {0};

/*===========================================================================*/
/* Private function implementation                                           */
/*===========================================================================*/

static void Nill(void) {}

static void lin_lld_int()
{
    USART_LINBreakDetectLengthConfig(LIND1.usart, USART_LINBreakDetectLength_10b);
    USART_LINCmd(LIND1.usart, ENABLE);
}

static void lin_driver_init(LINDriver* linp)
{
    USART_LINBreakDetectLengthConfig(linp->usart, USART_LINBreakDetectLength_10b);
    USART_LINCmd(linp->usart, ENABLE);

    linp->state = WAIT_BREAK;
    linp->rxstate = WAIT_BREAK;
    linp->txstate = WAIT_DATA;

    rxFrame.id = 0;
    rxFrame.dataLen= 0;
    rxFrame.crc = 0;

    txFrame.id = 0;
    txFrame.dataLen = 0;
    txFrame.crc = 0;
}

/**
 * @brief   Calculate CRC.
 *
 * @param[in] frame     pointer to the @p linFrame_t object
 */
static uint8_t lin_makeChecksum(LINFrame *frame)
{
    uint8_t ret_wert = 0, n;
    uint16_t dummy;

    // Compute checksum
    dummy = 0;
    for (n = 0; n < frame->dataLen; n++)
    {
        dummy += frame->data[n];
        if (dummy > 0xFF)
        {
            dummy -= 0xFF;
        }
    }
    ret_wert = (uint8_t)(dummy);
    ret_wert ^= 0xFF;

    return (ret_wert);
}

/**
 * @brief   LIN common service routine.
 *
 * @param[in] linp     pointer to the @p LINDriver object
 */
static void serve_lin_irq(LINDriver* linp)
{
    uint32_t isr;
    USART_TypeDef* u = linp->usart;

    isr = u->SR;

    if (isr & (USART_SR_ORE | USART_SR_NE | USART_SR_FE | USART_SR_PE))
    {
        if (linp->config->rxerr_cb != NULL)
            linp->config->rxerr_cb(linp, 1);// Receive error callback

        /* Reset ISR */
        linp->usart->SR = ~ (USART_SR_ORE | USART_SR_NE | USART_SR_FE | USART_SR_PE);
    }

    //uint32_t cr1 = u->CR1;

    /* LIN Break detection */
    if (isr & USART_SR_LBD)
    {
        if(linp->rxstate == WAIT_BREAK)
            linp->rxstate = WAIT_SYNC;
        else
        {
            linp->rxstate = WAIT_BREAK;
            linp->rxCount = 0;
            add_to_ring(linp->rxBuffer, data);
        }

        /* Reset ISR */
        linp->usart->SR = ~USART_SR_LBD;
    }

    /* Data available (receive). */
    if (isr & USART_SR_RXNE)
    {
        uint8_t tmp = linp->usart->DR;

        if (linp->rxstate == WAIT_SYNC && tmp == 0x55)
        {
            linp->rxstate = WAIT_DATA;
            return;
        }
        else
        {
            linp->rxstate = WAIT_BREAK;
        }
        rxFrame.dataLen ++;
        rxFrame.data[linp->rxCount] = tmp;
        linp->rxCount++;
    }
}

/*===========================================================================*/
/* Public function implementation                                            */
/*===========================================================================*/

void ldStart(LINDriver* linp)
{
    lin_lld_int();
    lin_driver_init(linp);
}

void USART11_IRQHandler()
{
    serve_lin_irq(&LIND1);
}








