#ifndef LIN_H_INCLUDED
#define LIN_H_INCLUDED

#include "usart.h"
#include "communication.h"


/*===========================================================================*/
/* Difinitions                                                               */
/*===========================================================================*/


#define  LIN_SYNC_DATA               0x55  // SyncField (do not edit)
#define  LIN_MAX_DATA                   8  // Max 8 Datenytes
#define  LIN_TX_ARRAY_LENGHT           12  // Array for tx/rx lin frane to/from PC

#define  LIN_POWERON_DELAY             10  // Pause after PowerOn     (ca. 10ms)
#define  LIN_AKTIV_DELAY                1  // Pause for Transceiver (ca. 50us)

#define  LIN_INTER_FRAME_DELAY         10  // Pause (Frame->Frame)   (ca. 10ms)
#define  LIN_FRAME_RESPONSE_DELAY       2  // Pause (Header->Data)   (ca.  2ms)
#define  LIN_BREAKFIELD_DELAY           4  // Pause (Breakfield)     (ca.  4ms)
#define  LIN_DATA_BYTE_DELAY            1  // Pause (Data->Data)     (ca.  1ms)
#define  LIN_RX_TIMEOUT_CNT             5  // Timeout when recive (ca.  5ms)

#define HAL_LIN_UART_TIMEOUT          100  // Timeout for HAL_Uart_Transmit function

#define ID_MASK                      0x0f  // Get real ID from header of lIN message

#define SYNC_PTR                        1  // Sync data byte position in array

#define RESPONSE_ARR_SIZE              18  // Count of filted pid in PID filter array

//#defien DATALENGHT_MASK              (0x03 <<4))>>4 // get data lenght from header of lIN message


/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/


/*===========================================================================*/
/* Driver data structures and types.                                         */
/*===========================================================================*/
typedef void (*transition_callback)(void);


// LIN farme data structure
typedef struct {
  uint8_t frame_Header;
  uint8_t frame_id;              // ID-Nummer for Frame
  uint8_t data_len;              // Data lenght
  uint8_t data[LIN_MAX_DATA];    // data array
  uint8_t crc;
}linFrame_t;

//
// LIN bus status
//
typedef enum {
  RECEIVE_DATA =0,  // Receive data
  RECEIVE_CRC,      // CRC recive
  SEND_DATA         // Send data
}linMode_t;

//
//LIN recive mode
//
typedef enum {
    WAIT_BREAK = 0,
    WAIT_SYNC,
    WAIT_ID,
    WAIT_DATA,
    WAIT_CHECKSUM,
    LINRXMODE_COUNT
}linRxmode_t;

//
// LIN events
//
typedef enum{
    NONE_EVENT,
    UART_LBD_EVENT,
    UART_RXNE_EVENT,
    TIM_IRQ_EVENT,
    LINDRIVER_EVENT_COUNT
}linDriverEvent_t;

//
// LIN master frame type
typedef struct {
  linRxmode_t mode;   // currebt Mode
  uint8_t data_ptr;   // Datenpointer
  uint8_t crc;        // Checksumme
}linMaster_t;

//
// LIN bus error type
//
typedef enum {
  LIN_OK  = 0,   // no error
  LIN_WRONG_LEN, // wrong error
  LIN_RX_EMPTY,  //no Frame recived
  LIN_WRONG_CRC  // Wrong CRC
}linErr_t;


typedef struct {

}transition_t;

/*===========================================================================*/
/* Function prototypes.                                                      */
/*===========================================================================*/

void Nill(void);
void usart_lin_init(USART_TypeDef* usart);
void transmit_to_pc(void);
linErr_t lin_send_master_data(linFrame_t *frame);
void lin_send_master_header(uint8_t id);
linErr_t lin_send_slave_data(linFrame_t *frame);
void lin_master_enable(void);
void lin_master_disable(void);
void bus_control_enable(void);
void bus_control_disable(void);
void chip_enable(void);
void chip_disable(void);
void clear_buff(uint8_t *array, uint8_t arrSize);
void set_PID_filter(linFrame_t *frame);
void reset_PID_filter(void);

#endif /* LIN_H_INCLUDED */
