/*
 * General purpose timer 3 interrupt initialization
 * This clock is 2 times of that of APB1, while APB1 is 36M
 * arr: Automatic reload value.
 * psc: The frequency of the clock Prescaler
 * The calculation formula is as follows:
 * Tout= ((arr+1)*(psc+1))/Tclk,
 * Among them:
 * Tclk: The input clock frequency TIM3 (unit: Mhz).
 * Tout: TIM3 overflow time (unit: US).
 */

#include "tim.h"
#include "stm32f10x_conf.h"

/*===========================================================================*/
/* Private difinitions                                                       */
/*===========================================================================*/


#define DEFAULT_ARR (3600)
#define DEFAULT_PERIOD 20


/*===========================================================================*/
/* Public variables                                                          */
/*===========================================================================*/


TIM_TimeBaseInitTypeDef tim3_struct;


/*===========================================================================*/
/* Private function implementation                                           */
/*===========================================================================*/


//
// Interrupt priority set 3 NVIC
//
void TIM3_NVIC_Init(void)
{
    NVIC_InitTypeDef NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;                 /**< TIM3 interrupt */
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;       /*Preemptive priority level 0*/
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;              /*From the priority level 3*/
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;                 /*The IRQ channel is enabled*/
    NVIC_Init(&NVIC_InitStructure);
}


/*===========================================================================*/
/* Public function implementation                                            */
/*===========================================================================*/


//
// TIM3 timer initialization
//
void tim3_Int_Init(uint16_t period)
{
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);    // Clock enable

    tim3_struct.TIM_Period = period * 10;                   // Cycle value
    tim3_struct.TIM_Prescaler = DEFAULT_ARR;                 // Pre frequency value
    tim3_struct.TIM_ClockDivision = TIM_CKD_DIV1;           // Set the clock division: TDTS = Tck_tim
    tim3_struct.TIM_CounterMode = TIM_CounterMode_Up;       // TIM up counting mode
    TIM_TimeBaseInit(TIM3, &tim3_struct);                   // According to the specified parameter initialization of TIMx time base unit

    TIM3_NVIC_Init();

    TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);              //The specified TIM3 interrupt, interrupt is allowed to update
}

//
// CLear count register in TIM3
//
void tim3_clear_count(void)
{
    TIM3->CNT = 0;
}

// CLear TIM3 flags
void tim3_clear_flags(void)
{
    TIM3->SR = ~TIM_FLAG_Update;
}

//
// Restart TIM3
//
void tim3_restart(void)
{
    TIM_Cmd(TIM3, DISABLE);   /**< Disable timer */

    tim3_clear_count();
    tim3_clear_flags();

    TIM_Cmd(TIM3, ENABLE);   /**< Enable timer */
}

//
// Set TIM3 period
//
void tim3_set_period(uint32_t period)
{
    TIM3->ARR = period * 10;
}

//
// Start TIM3
//
void tim3_start(void)
{
    TIM_Cmd(TIM3, ENABLE);
}


//
// Stop TIM3
//
void tim3_stop(void)
{
    TIM_Cmd(TIM3, DISABLE);
    tim3_clear_count();
    tim3_clear_flags();
}


