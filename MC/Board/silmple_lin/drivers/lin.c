#include "lin.h"
#include "gpio.h"
#include "datahelper.h"
#include "wpackage.h"
#include "stm32f10x.h"
#include "delay.h"
#include "hw_config.h"
#include "libc.h"
#include "tim.h"
#include "board.h"
#include "ring.h"
#include "ringmanager.h"

/*===========================================================================*/
/* Global variables                                                         */
/*===========================================================================*/


/*===========================================================================*/
/* Driver local definitions.                                                 */
/*===========================================================================*/

static linMaster_t lin_master;
static linFrame_t lin_frame;

//static uint8_t PID_filter[RESPONSE_ARR_SIZE] = {0};

extern base_ring lin_rx_ring;

static uint8_t lin_TxRx_array[LIN_TX_ARRAY_LENGHT] = {0};
static uint8_t lin_TxRx_array_ptr = LIN_PID_PTR;
static uint8_t data_len = 0;
static uint8_t rcDataV2 = 0;

linRxmode_t rx_frame_mode = WAIT_BREAK;
linDriverEvent_t currentEvent = NONE_EVENT;

static uint8_t lin_makeChecksum(linFrame_t *frame);
static void lin_master_init(void);

uint8_t tmp_buff[2] = {0}; // Size 10 because.... is 10 :)))

static linFrame_t response_list[RESPONSE_ARR_SIZE];
uint8_t response_list_position = 0;

/*===========================================================================*/
/* Private function implementation                                           */
/*===========================================================================*/

void LBD_to_wait_data(void)
{
    rx_frame_mode = WAIT_DATA;
}

void rx_data(void)
{
    lin_TxRx_array[lin_TxRx_array_ptr] = USART1->DR;
    lin_TxRx_array_ptr++;
    data_len++;
    gpio_pin_toggle(LED_BUS_PORT, LED_BUS_PIN); // Led blink

    rx_frame_mode = WAIT_DATA;
}

void Nill(void) {}


int8_t Contains(linFrame_t *array, uint8_t arrSize, uint8_t item)
{
    for(uint8_t i = 0; i < arrSize; i++)
    {
        if (array[i].frame_id == item)
        {
            return i;
        }
    }

    return -1;
}

//transition transition_table[LINDRIVER_EVENT_COUNT][LINRXMODE_COUNT] =
//{
//    [UART_LBD_EVENT][WAIT_BREAK] = LBD_to_wait_data,
//    [UART_LBD_EVENT][WAIT_DATA] = transmit_to_pc,
//    [UART_RXNE_EVENT][WAIT_DATA] = rx_data,
//    [UART_RXNE_EVENT][WAIT_BREAK] = Nill
//};



//
//  Init LIN bus state machine
//
void lin_master_init(void)
{
    // Init all structure
    lin_master.mode = WAIT_BREAK;
    lin_master.data_ptr = 0;
    lin_master.crc = 0;

    lin_frame.frame_id = 0;
    lin_frame.data_len = 0;
    lin_frame.data[0] = 0;

    // Wait a short time until Transceiver is ready
    delay_ms(10);
}

// LIN Transceiver (MAX13020 or TJA1021)
// from the "Sleep Mode" in "Normal-Slope Mode" switch
//
void lin_aktivateTransceiver(void)
{
    // NSLP-Pin o Lo level
    gpio_pin_set(TRANSMITTER_PORT, TRANSMITTER_PIN); // in "Sleep-Mode"
    // Short wait (min. 10us)
    delay_ms(LIN_AKTIV_DELAY);
    // NSLP-Pin to Hi-Level
    gpio_pin_set(TRANSMITTER_PORT, TRANSMITTER_PIN); // in "Normal-Slope-Mode"
    // Short wait (min. 10us)
    delay_ms(LIN_AKTIV_DELAY);
}

//
// Calculate Checksum for LIN_Frame
//
uint8_t lin_makeChecksum(linFrame_t *frame)
{
    uint8_t ret_wert = 0, n;
    uint16_t dummy;

    // Compute checksum
    dummy = 0;
    for (n = 0; n < frame->data_len; n++)
    {
        dummy += frame->data[n];
        if (dummy > 0xFF)
        {
            dummy -= 0xFF;
        }
    }
    ret_wert = (uint8_t)(dummy);
    ret_wert ^= 0xFF;

    return (ret_wert);
}

//
// Restart timer
//
void restart_timer(void)
{
    // TODO - setup timer
}

//
// Stop timer
//
void stop_timer(void)
{
    // TODO setup timer
}


/*===========================================================================*/
/* Public function implementation                                            */
/*===========================================================================*/


//
// Init peripherial and lin bus state machin before start
//
void usart_lin_init(USART_TypeDef* usart)
{
    USART_LINBreakDetectLengthConfig(usart, USART_LINBreakDetectLength_10b);
    USART_LINCmd(usart, ENABLE);
    lin_master_init();
}


linErr_t lin_send_slave_response(linFrame_t* frame)
{
    uint8_t checksum, n;

    // Check the length
    if ((frame->data_len < 1) || (frame->data_len > LIN_MAX_DATA))
    {
        return (LIN_WRONG_LEN);
    }

    // Checksum ausrechnen
    checksum = lin_makeChecksum(frame);

    // Data-Field [1...n]
    for (n = 0; n < frame->data_len; n++)
    {
        USART1->DR = frame->data[n];
        while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);

        // Small Pause
        //delay_ms(LIN_DATA_BYTE_DELAY);
    }

    // CRC-Field
    USART1->DR = checksum;
    while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);

    // Small pause
    // So that next Frame is not sent too fast
    //delay_ms(LIN_INTER_FRAME_DELAY);


    gpio_pin_toggle(LED_STATUS_PORT, LED_STATUS_PIN);

    return (LIN_OK);
}

//
// Send data frame by lin bus (USARTx)
//
linErr_t lin_send_master_data(linFrame_t* frame)
{
    uint8_t checksum, n;

    // Check the length
    if ((frame->data_len < 1) || (frame->data_len > LIN_MAX_DATA))
    {
        return (LIN_WRONG_LEN);
    }

    // Checksumme ausrechnen
    checksum = lin_makeChecksum(frame);

    // Send breake
    USART_SendBreak(USART1);
    while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);

    // Small pause
    //delay_ms(LIN_BREAKFIELD_DELAY);

    // Sync-Field
    USART1->DR = LIN_SYNC_DATA;
    while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
    // kleine Pause
    delay_ms(LIN_DATA_BYTE_DELAY);

    // ID-Field
    USART1->DR = frame->frame_id;
    while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);

    // kleine Pause
    delay_ms(LIN_FRAME_RESPONSE_DELAY);

    // Data-Field [1...n]
    for (n = 0; n < frame->data_len; n++)
    {
        USART1->DR = frame->data[n];
        while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);

        // Small Pause
        //delay_ms(LIN_DATA_BYTE_DELAY);
    }

    //delay_ms(LIN_DATA_BYTE_DELAY);

    // CRC-Field
    USART1->DR = checksum;
    while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);

    // Small pause
    // So that next Frame is not sent too fast
    //delay_ms(LIN_INTER_FRAME_DELAY);


    gpio_pin_toggle(LED_STATUS_PORT, LED_STATUS_PIN);

    return (LIN_OK);
}


//
// Send only Master header to LIN network and wait response from slave
//
void lin_send_master_header(uint8_t id)
{
    // Send breake
    USART_SendBreak(USART1);
    while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);

    // Small pause
    //delay_ms(LIN_BREAKFIELD_DELAY);

    // Sync-Field
    USART1->DR = LIN_SYNC_DATA;
    while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
    // kleine Pause
    delay_ms(LIN_DATA_BYTE_DELAY);

    // ID-Field
    USART1->DR = id;
    while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
}

//
// Send dlsvr response by lin bus (USARTx)
//
linErr_t lin_send_slave_data(linFrame_t *frame)
{
    uint8_t checksum, n;

    // Check the length
    if ((frame->data_len < 1) || (frame->data_len > LIN_MAX_DATA))
    {
        return (LIN_WRONG_LEN);
    }

    // Checksumme ausrechnen
    checksum = lin_makeChecksum(frame);

    // Send breake
    //USART_SendBreak(USART1);
    //while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);

    // Small pause
    //delay_ms(LIN_BREAKFIELD_DELAY);

    // Sync-Field
    //USART1->DR = LIN_SYNC_DATA;
    //while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
    // kleine Pause
    //delay_ms(LIN_DATA_BYTE_DELAY);

    // ID-Field
    //USART1->DR = frame->frame_id;
    //while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);

    // kleine Pause
    delay_ms(LIN_FRAME_RESPONSE_DELAY);

    // Data-Field [1...n]
    for (n = 0; n < frame->data_len; n++)
    {
        USART1->DR = frame->data[n];
        while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);

        // Small Pause
        //delay_ms(LIN_DATA_BYTE_DELAY);
    }

    //delay_ms(LIN_DATA_BYTE_DELAY);

    // CRC-Field
    USART1->DR = checksum;
    while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);

    // Small pause
    // So that next Frame is not sent too fast
    //delay_ms(LIN_INTER_FRAME_DELAY);


    gpio_pin_toggle(LED_STATUS_PORT, LED_STATUS_PIN);

    bus_control_disable();

    return (LIN_OK);
}

//
// Transmit complete message (received LIN frame + service bytes)
//
//void transmit_to_pc(void)
//{
//    stop_timer();
//    // lin_TxRx_array[LIN_DATALEN_PTR] = data_len_rx; // Temporary hack
//    send_wpkg(GET_LIN_FRAME, lin_TxRx_array, LIN_TX_ARRAY_LENGHT); // Pack data to "wpkg" and send it to PC
//
//    clear_buff(lin_TxRx_array, sizeof(lin_TxRx_array)); // Clear data array
//    data_len_rx = 0; // Reset data length
//    lin_TxRx_array_ptr = LIN_DATA_PTR; // Reset data pointer
//
//    rx_frame_mode = WAIT_DATA;
//}

//
// Change solid state relay to "cut" original master from lin network.
//
void lin_master_enable(void)
{
    gpio_pin_reset(MASTER_ENABLE_PORT, MASTER_ENABLE_PIN);
}

//
// Change solid state relay to "connect" original master from lin network.
//
void lin_master_disable(void)
{
    gpio_pin_set(MASTER_ENABLE_PORT, MASTER_ENABLE_PIN);
}

//
// Enable bus control - solid state relay is activated, cut off origin LIN
// network
//
void bus_control_enable(void)
{
    gpio_pin_reset(BUS_CONTROL_PORT, BUS_CONTROL_PIN);
}


//
// Disable bus control - solid state relay - disabled and comeback origin LIN
// network connection
//
void bus_control_disable(void)
{
    gpio_pin_set(BUS_CONTROL_PORT, BUS_CONTROL_PIN);
}

//
// Enable transceiver
//
void chip_enable(void)
{
    gpio_pin_set(TRANSMITTER_PORT, TRANSMITTER_PIN);
}

//
// Disable transceiver
//
void chip_disable(void)
{
    gpio_pin_reset(TRANSMITTER_PORT, TRANSMITTER_PIN);
}

//
// CLear any uint8_t array
//
void clear_buff(uint8_t *array, uint8_t arrSize)
{
    memset(array, 0, arrSize);
}

//
// Set array of filtred PID
//
void set_PID_filter(linFrame_t *frame)
{
    if (response_list_position < RESPONSE_ARR_SIZE)
    {
        response_list[response_list_position] = *frame;
        response_list_position ++;
    }
}

//
// Reset array of filtred PID
//
void reset_PID_filter(void)
{
    memset(response_list, 0, RESPONSE_ARR_SIZE);
    response_list_position = 0;
}

/*===========================================================================*/
/* IRQ Handling                                                              */
/*===========================================================================*/


void USART1_IRQHandler()
{
    if (USART_GetITStatus(USART1, USART_IT_LBD) != RESET)
    {
        USART_ClearITPendingBit(USART1, USART_IT_LBD);

        rcDataV2 = USART1->DR;

        if (lin_master.mode == WAIT_BREAK)
        {
            lin_master.mode = WAIT_SYNC;
            return;
        }

        return;
    }

    if (USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
    {
        rcDataV2 = USART1->DR;
        if (lin_master.mode == WAIT_SYNC && rcDataV2 == LIN_SYNC_DATA)
        {
            lin_TxRx_array[LIN_DATALEN_PTR]++;
            lin_master.mode = WAIT_DATA;
            lin_TxRx_array[SYNC_PTR] = rcDataV2;
            return;
        }

        if (lin_master.mode == WAIT_DATA)
        {
            USART_ITConfig(USART1, USART_IT_IDLE, ENABLE);
            // TODO Chech - is PID contains in PID FILtER LIST!
            if (lin_TxRx_array_ptr == LIN_PID_PTR) // First data byte is PID
            {
                lin_TxRx_array[LIN_DATALEN_PTR]++;
                lin_TxRx_array[lin_TxRx_array_ptr] = rcDataV2;
                lin_TxRx_array_ptr++;

                int8_t frameIndex = Contains(response_list, RESPONSE_ARR_SIZE ,rcDataV2);
                if (frameIndex >= 0)
                {
                    //bus_control_enable();
                    //tmp_buff[0] = rcDataV2;
                    //send_wpkg(RESET_PID_FILTER, tmp_buff, sizeof(tmp_buff));
                    // TODO Add some actions

                    lin_send_slave_response(&response_list[frameIndex]);
                    return;
                }
            }
            else
            {
                lin_TxRx_array[LIN_DATALEN_PTR]++;
                lin_TxRx_array[lin_TxRx_array_ptr] = rcDataV2;
                lin_TxRx_array_ptr++;
            }
        }
    }

    if (USART_GetITStatus(USART1, USART_IT_IDLE) != RESET)
    {
        USART_ClearITPendingBit(USART1, USART_IT_IDLE);

        lin_master.mode = WAIT_BREAK;
        add_to_ring(&lin_rx_ring,lin_TxRx_array);
        clear_buff(lin_TxRx_array, sizeof(lin_TxRx_array)); // Clear data array
        lin_TxRx_array_ptr = LIN_PID_PTR;

        USART_ITConfig(USART1, USART_IT_IDLE, DISABLE);

        return;
    }
}


//void USART1_IRQHandler()
//{
//    if (USART1->SR & USART_SR_RXNE)
//    {
//        currentEvent = UART_RXNE_EVENT;
//        USART1->SR = ~USART_SR_RXNE;
//        transition_table[currentEvent][rx_frame_mode]();
//    }
//    if (USART1->SR & USART_SR_LBD)
//    {
//        currentEvent = UART_LBD_EVENT;
//        USART1->SR = ~USART_SR_LBD;
//        transition_table[currentEvent][rx_frame_mode]();
//    }
//}


//
// Period elapsed callback in non blocking mode - new re-factored function
//
// TODO setup timer IRQ
void TIM3_IRQHandler(void)
{
    if(TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET ) /*Check the TIM3 update interrupt occurs or not*/
    {
        TIM_ClearITPendingBit(TIM3, TIM_IT_Update); /*Remove TIMx update interrupt flag */
    }
}

