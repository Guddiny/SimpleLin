#ifndef USART_H_INCLUDED
#define USART_H_INCLUDED

#include "stm32f10x.h"


/*===========================================================================*/
/* Difinitions                                                               */
/*===========================================================================*/


// Clock of APB2 bus: [MHz]
#define FCK 72000000


/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/


extern USART_InitTypeDef usart1_struct;


/*===========================================================================*/
/* Function prototypes.                                                      */
/*===========================================================================*/


void usart1_init(uint32_t baudrate);
void usart1_spl_init(uint32_t baudrate);
void usart_set_baudrate(USART_TypeDef* usart, uint32_t baudrate);
void usart_send_byte(USART_TypeDef* uasrt, uint8_t byte);
void usart_send_data(USART_TypeDef* uasrt, uint8_t* data, int data_qty);



#endif
