#include "gpio.h"


void gpio_pin_set(GPIO_TypeDef* gpio, uint16_t pin)
{
    gpio->BSRR = pin;
}

void gpio_pin_reset(GPIO_TypeDef* gpio, uint16_t pin)
{
    gpio->BRR = pin;
}

void gpio_pin_toggle(GPIO_TypeDef* gpio, uint16_t pin)
{
    gpio->ODR ^= pin;
}

