#ifndef GPIO_H_INCLUDED
#define GPIO_H_INCLUDED

#include "stm32f10x.h"

void gpio_pin_set(GPIO_TypeDef* gpio, uint16_t pin);
void gpio_pin_reset(GPIO_TypeDef* gpio, uint16_t pin);
void gpio_pin_toggle(GPIO_TypeDef* gpio, uint16_t pin);


#endif /* GPIO_H_INCLUDED */
