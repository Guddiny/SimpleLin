#ifndef LIN_LLD_H
#define LIN_LLD_H

#include "usart.h"
#include "communication.h"
#include "ring.h"
#include "ringmanager.h"

/*===========================================================================*/
/* Definitions                                                               */
/*===========================================================================*/

#define  LIN_SYNC_DATA               0x55  // SyncField (do not edit)
#define  LIN_MAX_DATA                   8  // Max 8 bytes
#define  LIN_TX_ARRAY_LENGHT           12  // Array for tx/rx lin frame
                                           // to/from PC

#define  LIN_POWERON_DELAY             10  // Pause after PowerOn     (ca. 10ms)
#define  LIN_AKTIV_DELAY                1  // Pause for Transceiver (ca. 50us)

#define  LIN_INTER_FRAME_DELAY         10  // Pause (Frame->Frame)   (ca. 10ms)
#define  LIN_FRAME_RESPONSE_DELAY       2  // Pause (Header->Data)   (ca.  2ms)
#define  LIN_BREAKFIELD_DELAY           4  // Pause (Breakfield)     (ca.  4ms)
#define  LIN_DATA_BYTE_DELAY            1  // Pause (Data->Data)     (ca.  1ms)
#define  LIN_RX_TIMEOUT_CNT             5  // Timeout when receive (ca.  5ms)

#define HAL_LIN_UART_TIMEOUT          100  // Timeout for HAL_Uart_Transmit
                                           // function

#define ID_MASK                      0x0f  // Get real ID from header of
                                           //LIN message

#define SYNC_PTR                        1  // Sync data byte position in array

#define RESPONSE_ARR_SIZE              18  // Count of filtered PID in PID
                                           // filter array

/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/


/*===========================================================================*/
/* Driver data structures and types.                                         */
/*===========================================================================*/

/**
 * @brief   Structure representing an LIN driver.
 */
typedef struct LINDriver LINDriver;

/**
 * @brief   Structure representing an LIN driver states.
 */
typedef enum
{
    LIN_UNINIT = 0,                   /**< Not initialized.                   */
    LIN_STOP = 1,                     /**< Stopped.                           */
    LIN_READY = 2                     /**< Ready.                             */
} linstate_t;


/**
 * @brief   Structure representing an LIN driver RX states.
 */
typedef enum
{
    IDLE = 0,
    WAIT_BREAK,
    WAIT_SYNC,
    WAIT_ID,
    WAIT_DATA,
    WAIT_END // ??
} linrxstate_t;


/**
 * @brief   Structure representing an LIN driver TX states.
 */
typedef enum
{
    SEND_BREAK,
    SEND_SYNC,
    SEND_DATA,
    SEND_CRC
} lintxstate_t;


/**
 * @brief   Enum representing an LIN errors.
 */
typedef enum
{
    LIN_OK = 0,    // no error
    LIN_WRONG_LEN, // wrong error
    LIN_RX_EMPTY,  //no Frame received
    LIN_WRONG_CRC  // Wrong CRC
} linerrors_t;


/**
 * @brief   Structure representing an LIN frame.
 */
typedef struct
{
    /**
    * @brief LIN frame Id
    */
    uint8_t id;
    /**
    * @brief LIN frame data lenfth.
    */
    uint8_t dataLen;
    /**
    * @brief LIN frame data.
    */
    uint8_t data[LIN_MAX_DATA];
    /**
    * @brief LIN frame crc
    */
    uint8_t crc;
} LINFrame;


/**
 * @brief   Character received LIN notification callback type.
 *
 * @param[in] iuartp     pointer to the @p LINDriver object
 * @param[in] c          received character
 */
typedef void (*linccb_t)(LINDriver *linp, uint8_t c);


/**
 * @brief   Character received LIN notification callback type.
 *
 * @param[in] iuartp     pointer to the @p LINDriver object
 * @param[in] id         received Id
 */
typedef void (*linidcb_t)(LINDriver *linp, uint8_t id);


/**
 * @brief   Character received LIN notification callback type.
 *
 * @param[in] iuartp     pointer to the @p LINDriver object
 * @param[in] rxFrame    received LINframe
 */
typedef void (*linframecb_t)(LINDriver *linp, LINFrame *rxFrame);


/**
 * @brief   Character received LIN notification callback type.
 *
 * @param[in] iuartp     pointer to the @p LINDriver object
 * @param[in] error      error code
 */
typedef void (*linerrcb_t)(LINDriver *linp, uint32_t error);


/**
 * @brief   Structure representing an LIN driver config.
 */
typedef struct{
    /**
    * @brief Receive frame Id callback.
    */
    linidcb_t rxid_cb;
    /**
    * @brief Receive full LIN frame callback.
    */
    linframecb_t rxframe_cb;
    /**
    * @brief Receive full LIN frame callback.
    */
    linerrcb_t rxerr_cb;

} LINDriverConfig;


/**
 * @brief   Structure representing an LIN driver.
 */
struct LINDriver
{
    /**
    * @brief Driver state.
    */
    linstate_t state;
    /**
    * @brief Receiver state.
    */
    linrxstate_t rxstate;
    /**
    * @brief Transmitter state.
    */
    lintxstate_t txstate;
    /**
    * @brief Pointer to the USART registers block.
    */
    USART_TypeDef* usart;
    /**
    * @brief Current configuration data.
    */
    LINDriverConfig* config;

    uint16_t txCount;          // Number of bytes in transmit buffer
    base_ring  *txBuffer;          // Pointer to current transmit buffer
    uint16_t rxCount;          // Number of bytes for 'active' receive
    base_ring *rxBuffer; // Pointer to receive buffer if block receive active
};


/*===========================================================================*/
/* Function prototypes.                                                      */
/*===========================================================================*/
void ldObjectInit(LINDriver* linp);
void ldStart(LINDriver* linpm);


/*===========================================================================*/
/* Driver macros.                                                            */
/*===========================================================================*/

/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

extern LINDriver LIND1;

#endif
