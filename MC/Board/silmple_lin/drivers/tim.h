#ifndef TIM_H_INCLUDED
#define TIM_H_INCLUDED

#include "stm32f10x_conf.h"


/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/


extern TIM_TimeBaseInitTypeDef tim3_struct;


/*===========================================================================*/
/* Difinitions                                                               */
/*===========================================================================*/


/*===========================================================================*/
/* Function prototypes.                                                      */
/*===========================================================================*/


void tim3_Int_Init(uint16_t period);
void tim3_clear_count(void);
void tim3_restart(void);
void tim3_start(void);
void tim3_stop(void);
void tim3_set_period(uint32_t period);


#endif /* TIM_H_INCLUDED */
