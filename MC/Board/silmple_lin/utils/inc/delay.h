#ifndef DELAY_H_INCLUDED
#define DELAY_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f10x.h"


/*===========================================================================*/
/* Difinitions                                                               */
/*===========================================================================*/


/*===========================================================================*/
/* Function prototypes.                                                      */
/*===========================================================================*/


void delay_init(void);
void delay_us(uint32_t us);
void delay_ms(uint32_t ms);


#ifdef __cplusplus
}
#endif

#endif /* DELAY_H_INCLUDED */
