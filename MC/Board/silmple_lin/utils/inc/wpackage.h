#ifndef WPACKAGE_H
#define WPACKAGE_H

#include "stm32f10x_conf.h"
#include "hw_config.h"


/*===========================================================================*/
/* Difinitions                                                               */
/*===========================================================================*/


/*===========================================================================*/
/* Function prototypes.                                                      */
/*===========================================================================*/


uint8_t get_crc(uint8_t *buff);
bool check_crc(uint8_t *buff);
void send_wpkg(uint8_t cmd, uint8_t *buff, uint8_t data_len);

#endif
