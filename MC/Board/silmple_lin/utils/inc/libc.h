#ifndef __libc_H
#define __libc_H

#include "stm32f10x_conf.h"

/*===========================================================================*/
/* ocal definitions.                                                         */
/*===========================================================================*/


/*===========================================================================*/
/* Function prototypes.                                                      */
/*===========================================================================*/


void enter_critical_section();
void exit_critical_section();
int memcmp(const void * ptr1, const void * ptr2, unsigned int num);


#endif
