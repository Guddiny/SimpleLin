#include "delay.h"


/*===========================================================================*/
/* Gloabal variables                                                         */
/*===========================================================================*/


// For store tick counts in us
static __IO uint32_t usTicks;


/*===========================================================================*/
/* Private function implementation                                           */
/*===========================================================================*/


// SysTick_Handler function will be called every 1 us
void SysTick_Handler()
{
    if (usTicks != 0)
    {
        usTicks--;
    }
}


/*===========================================================================*/
/* Public function implementation                                            */
/*===========================================================================*/


//
// Initialize delay function
//
void delay_init()
{
    // Update SystemCoreClock value
    SystemCoreClockUpdate();
    // Configure the SysTick timer to overflow every 1 us
    SysTick_Config(SystemCoreClock / 1000000);
}

//
// Delay in microseconds (1s = 1 000 000 ms)
//
void delay_us(uint32_t us)
{
    // Reload us value
    usTicks = us;
    // Wait until usTick reach zero
    while (usTicks);
}

//
// Delay in milliseconds ( 1s = 1000 ms)
//
void delay_ms(uint32_t ms)
{
    // Wait until ms reach zero
    while (ms--)
    {
        // Delay 1ms
        delay_us(1000);
    }
}

