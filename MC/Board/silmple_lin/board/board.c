#include "board.h"
#include "gpio.h"


/*===========================================================================*/
/* Private function implementation                                           */
/*===========================================================================*/


void led_bus_pin_init()
{
    RCC->APB2ENR |= RCC_APB2ENR_IOPCEN;
    GPIO_InitTypeDef led_bus_pin_struct =
    {
        LED_BUS_PIN,
        GPIO_Speed_2MHz,
        GPIO_Mode_Out_PP
    };
    GPIO_Init(LED_BUS_PORT, &led_bus_pin_struct);
}

void led_status_pin_init()
{
    RCC->APB2ENR |= RCC_APB2ENR_IOPCEN;
    GPIO_InitTypeDef led_status_pin_struct =
    {
        LED_STATUS_PIN,
        GPIO_Speed_2MHz,
        GPIO_Mode_Out_PP
    };
    GPIO_Init(LED_STATUS_PORT, &led_status_pin_struct);
}

void button_boot_pin_init()
{
    RCC->APB2ENR |= RCC_APB2ENR_IOPBEN;
    GPIO_InitTypeDef butn_boot_pin_init_struct =
    {
        BUTTON_BOOT_PIN,
        GPIO_Speed_2MHz,
        GPIO_Mode_IPU
    };
    GPIO_Init(BUTTON_BOOT_PORT, &butn_boot_pin_init_struct);
}

void master_enable_pin_init()
{
    RCC->APB2ENR |= RCC_APB2ENR_IOPBEN | RCC_APB2ENR_AFIOEN;
    GPIO_PinRemapConfig (GPIO_Remap_SWJ_JTAGDisable, ENABLE);
    GPIO_PinRemapConfig (GPIO_Remap_SWJ_NoJTRST, ENABLE);
    GPIO_InitTypeDef master_enable_pin_struct =
    {
        GPIO_Pin_4,
        GPIO_Speed_2MHz,
        GPIO_Mode_Out_PP
    };
    gpio_pin_set(BUS_CONTROL_PORT, BUS_CONTROL_PIN);
    GPIO_Init(GPIOB, &master_enable_pin_struct);
}

void bus_control_pin_init()
{
    RCC->APB2ENR |= RCC_APB2ENR_IOPBEN;
    GPIO_InitTypeDef bus_ctr_pin_struct =
    {
        BUS_CONTROL_PIN,
        GPIO_Speed_50MHz,
        GPIO_Mode_Out_PP
    };
    gpio_pin_set(BUS_CONTROL_PORT, BUS_CONTROL_PIN);
    GPIO_Init(BUS_CONTROL_PORT, &bus_ctr_pin_struct);
}

void transmitter_pin_init()
{
    RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
    GPIO_InitTypeDef trans_en_pin_struct =
    {
        TRANSMITTER_PIN,
        GPIO_Speed_10MHz,
        GPIO_Mode_Out_PP
    };
    GPIO_Init(TRANSMITTER_PORT, &trans_en_pin_struct);
}


/*===========================================================================*/
/* Public function implementation                                            */
/*===========================================================================*/


void board_init()
{
    led_bus_pin_init();
    led_status_pin_init();
    button_boot_pin_init();
    master_enable_pin_init();
    bus_control_pin_init();
    transmitter_pin_init();
}
