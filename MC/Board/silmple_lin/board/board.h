/**********************************************************************

File contain definition and initialization for separate board pins? like
a buttons, leds and etc.

**********************************************************************/

#ifndef BOARD_H
#define BOARD_H

#include "stm32f10x_conf.h"

/*===========================================================================*/
/* Difinitions                                                               */
/*===========================================================================*/


#define LED_STATUS_PIN              GPIO_Pin_14
#define LED_STATUS_PORT             GPIOC

#define LED_BUS_PIN                 GPIO_Pin_13
#define LED_BUS_PORT                GPIOC

#define BUTTON_BOOT_PIN             GPIO_Pin_2
#define BUTTON_BOOT_PORT            GPIOB

#define MASTER_ENABLE_PIN           GPIO_Pin_4
#define MASTER_ENABLE_PORT          GPIOB

#define BUS_CONTROL_PIN             GPIO_Pin_5
#define BUS_CONTROL_PORT            GPIOB

#define TRANSMITTER_PIN             GPIO_Pin_0
#define TRANSMITTER_PORT            GPIOA


/*===========================================================================*/
/* Function prototypes.                                                      */
/*===========================================================================*/


void board_init(void);


#endif

