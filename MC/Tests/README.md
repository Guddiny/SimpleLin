## To use this tests you must do some steps:
1. Install Python 3.xon your PC.
2. Install pyserial library: `pip install pyserial`.
3. Specify serial port number of connected device. You can do this in main.py file:

    ```python
    port_name = "COM5"  # Port name of your device
    
or setup it in device manager in windows.

 4. Open termnal and run: `py main.py`