import serial
import serial.tools.list_ports
import wpackage
import unittest
import datahelper

# Test hardware request - response

class DeviceCommunicationTest(unittest.TestCase):

    port_name = "COM5"  # Port name of your device
    temp_data = [0x12, 0x13, 0x14, 0x15]

    # Connect to device
    sp1 = serial.Serial(port_name, 115200, timeout=2)

    # Helper for package sending
    wp = wpackage.WPacket()

    # Definitions from protocol.
    # See: https://docs.google.com/document/d/1k_0_KEaK4MgFOL2ShaHvgRaSgq0yzDBagRkQHupUjd0/edit#
    GET_DEVICE_NAME = 0x1a
    GET_DEVICE_VERSION = 0x2a
    GET_LIN_BAUDRATE = 0x1b
    SET_LIN_BAUDRATE = 0x2b
    GET_LIN_MASTER_TIMEOUT = 0x1C
    SET_LIN_MASTER_TIMEOUT = 0x2C
    GET_LIN_FRAME = 0x1D  # Need 2 devices or emulator
    SET_LIN_FRAME = 0x2D  # Need 2 devices or emulator

    W_PKG_LENGTH = 20

    # Tests

    def test_get_device_name(self):
        serial.time.sleep(1);
        print('Test device name getting')
        self.wp.send_wpkg(self.sp1, self.GET_DEVICE_NAME, self.temp_data)
        data = self.sp1.read(self.W_PKG_LENGTH)
        print('[ {0} : {1} ]'.format(data[0], self.GET_DEVICE_NAME))

        self.assertEqual(self.wp.check_crc(data), True)
        self.assertEqual(data[0], self.GET_DEVICE_NAME)

    def test_get_device_version(self):
        serial.time.sleep(1);
        print('Test device version getting')
        self.wp.send_wpkg(self.sp1, self.GET_DEVICE_VERSION, self.temp_data)
        data = self.sp1.read(self.W_PKG_LENGTH)
        print('[ {0} : {1} ]'.format(data[0], self.GET_DEVICE_VERSION))

        self.assertTrue(self.wp.check_crc(data))
        self.assertEqual(data[0], self.GET_DEVICE_VERSION)

    def test_get_lin_baudrate(self):
        serial.time.sleep(1);
        print('Test get LIN baudrate:')
        exp_baudrate = 0x2580  # 9600 kb/s

        self.wp.send_wpkg(self.sp1, self.GET_LIN_BAUDRATE, self.temp_data)
        data = self.sp1.read(self.W_PKG_LENGTH)
        act_baudrate = datahelper.get_u16(data, 1)
        print('[ {0} : {1} ]'.format(act_baudrate, exp_baudrate))

        self.assertEqual(self.wp.check_crc(data), True)
        self.assertEqual(act_baudrate, exp_baudrate)

    def test_set_lin_baudrate(self):
        serial.time.sleep(1);
        print('Test: set LIN baudrate')
        new_baudrate = 0x2EE0  # 12000 kb/s
        new_baudrate_data = [0x2E, 0xE0, 0, 0]  # 0x2EE0 - 12000 kb/s

        self.wp.send_wpkg(self.sp1, self.SET_LIN_BAUDRATE, new_baudrate_data)
        self.wp.send_wpkg(self.sp1, self.GET_LIN_BAUDRATE, self.temp_data)
        data = self.sp1.read(self.W_PKG_LENGTH)
        act_baudrate = datahelper.get_u16(data, 1)
        print('[ {0} : {1} ]'.format(act_baudrate, new_baudrate))

        self.assertEqual(self.wp.check_crc(data), True)
        self.assertEqual(act_baudrate, new_baudrate)

        self.wp.send_wpkg(self.sp1, self.SET_LIN_BAUDRATE, [0x25, 0x80, 0, 0])  # reset baudrate value

    def test_get_lin_master_timeout(self):
        serial.time.sleep(1);
        print('Test get LIN master timeout:')
        exp_timeout = 0xC8  # 200 ms

        self.wp.send_wpkg(self.sp1, self.GET_LIN_MASTER_TIMEOUT, self.temp_data)
        data = self.sp1.read(self.W_PKG_LENGTH)
        act_timeout = data[1]
        print('[ {0} : {1} ]'.format(act_timeout, exp_timeout))

        self.assertEqual(self.wp.check_crc(data), True)
        self.assertEqual(act_timeout, exp_timeout)

    def test_set_lin_master_timeout(self):
        serial.time.sleep(1);
        print('Test set LIN master timeout:')
        new_timeout = 0x64  # 100 ms
        new_timeout_data = [0x64] # 100 ms

        self.wp.send_wpkg(self.sp1, self.SET_LIN_MASTER_TIMEOUT, new_timeout_data)
        self.wp.send_wpkg(self.sp1, self.GET_LIN_MASTER_TIMEOUT, self.temp_data)

        data = self.sp1.read(self.W_PKG_LENGTH)
        act_timeout = data[1]
        print('[ {0} : {1} ]'.format(new_timeout, act_timeout))

        self.assertEqual(self.wp.check_crc(data), True)
        self.assertEqual(act_timeout, new_timeout)

        self.wp.send_wpkg(self.sp1, self.SET_LIN_MASTER_TIMEOUT, [0xC8,0])  # Reset timeout value

if __name__ == "__main__":
    unittest.main()